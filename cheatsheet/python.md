<!--
Copyright (C) 2019-2022 - dServ Solutions SàRL - [Pascal Rapaz](mailto:pascal.rapaz@dserv.ch)

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
Free Documentation License".

A copy of the license is agreement is available at [GNU Free Documentation License](https://www.gnu.org/licenses/fdl-1.3.txt)".
-->

# TODO 
- [ ] Match (switch case)
- [ ] enumerate

# Formation Python 3.x <!-- omit in toc -->

## Table des matières <!-- omit in toc -->

- [TODO](#todo)
- [Visual Studio](#visual-studio)
  - [Configuration](#configuration)
  - [Exécution des applications Python](#exécution-des-applications-python)
- [Python](#python)
  - [Conventions de nommage](#conventions-de-nommage)
  - [Commentaires](#commentaires)
  - [Variables](#variables)
  - [Conversions](#conversions)
  - [Opérateurs](#opérateurs)
    - [Arithmétique](#arithmétique)
    - [Comparaison](#comparaison)
    - [Assignement](#assignement)
    - [Logique](#logique)
  - [Tests conditionnels](#tests-conditionnels)
  - [Boucles](#boucles)
    - [Break, Continue et Pass](#break-continue-et-pass)
  - [Structures de données](#structures-de-données)
    - [Listes](#listes)
    - [Tuples](#tuples)
    - [Dictionnaires](#dictionnaires)
    - [Set et Frozenset](#set-et-frozenset)
      - [Set](#set)
      - [Frozenset](#frozenset)
  - [Slicing](#slicing)
  - [Print](#print)
  - [Formater une string](#formater-une-string)
    - [f-Strings](#f-strings)
      - [Expressions arbitraires](#expressions-arbitraires)
      - [Lignes multiples](#lignes-multiples)
      - [Types spécifiques](#types-spécifiques)
      - [Justification](#justification)
    - [Format](#format)
      - [Types spécifiques](#types-spécifiques-1)
      - [Justification](#justification-1)
    - [Opérateur `%`](#opérateur-)
  - [Logs](#logs)
    - [Utilisation](#utilisation)
    - [Enregistrer les logs dans un fichier](#enregistrer-les-logs-dans-un-fichier)
    - [Gérer plusieurs sorties](#gérer-plusieurs-sorties)
  - [Exceptions](#exceptions)
    - [Gérer une exception](#gérer-une-exception)
    - [Lever une exception](#lever-une-exception)
  - [Fichiers](#fichiers)
    - [Lire un fichier](#lire-un-fichier)
    - [Écrire dans un fichier](#écrire-dans-un-fichier)
    - [Le mot clef `with`](#le-mot-clef-with)
    - [Format YAML et JSON](#format-yaml-et-json)
  - [Fonctions](#fonctions)
    - [Syntaxe](#syntaxe)
    - [Fonctions à plusieurs paramètres](#fonctions-à-plusieurs-paramètres)
    - [Valeurs par défaut](#valeurs-par-défaut)
    - [Variables locales et globales](#variables-locales-et-globales)
    - [Valeurs de retour](#valeurs-de-retour)
    - [Fonction main](#fonction-main)
  - [Classes](#classes)
    - [Syntaxe](#syntaxe-1)
    - [Objets](#objets)
    - [Méthodes](#méthodes)
    - [Attributs de classe](#attributs-de-classe)
    - [Getter et setter](#getter-et-setter)
      - [Décorateurs](#décorateurs)
      - [Propriétés](#propriétés)
    - [Héritage](#héritage)
    - [Polymorphisme](#polymorphisme)
    - [Divers](#divers)
      - [Fonction \_\_repr\_\_ et \_\_str\_\_](#fonction-__repr__-et-__str__)
      - [Fonction `dir` et attribut `__dict__`](#fonction-dir-et-attribut-__dict__)
  - [Modules et packages](#modules-et-packages)
    - [Création d'un package](#création-dun-package)
    - [Import d'un package](#import-dun-package)
  - [Tests unitaires](#tests-unitaires)
    - [unittest](#unittest)
      - [Exécution](#exécution)
    - [doctest](#doctest)
  - [Divers](#divers-1)
    - [Arguments d'exécution](#arguments-dexécution)
- [Sources](#sources)

# Visual Studio

## Configuration

- Sélectionnez l'interpréteur Python utilisé dans la barre des tâches

- Adaptez la taille des espaces du tabulateur (*en général j'indente avec 2 espaces*)
  - Pour une configuration spécifique pour python, ajoutez le bloc suivant dans le fichier `settings.json` de vscode

    ```json
    "[python]": {
      "editor.insertSpaces": true,
      "editor.tabSize": 2
    }
    ```

## Exécution des applications Python

- Sans le debugger
  - Menu contextuel "Run Python File in Terminal" ou signe "play" à droite des onglets des fichiers ouverts

- Avec le debugger
  - F9 ou clic dans la marge pour mettre un point d'arrêt
  - F5 pour initialiser le debugger

<!-- 
Exercices : 
  01 - Introduction
    01 - Hello World
-->

# Python

## Conventions de nommage

*(Selon norme [PEP 8](https://www.python.org/dev/peps/pep-0008/))*

<table>
<tr>
  <th>Type</th>
  <th>Règle</th>
  <th>Exemple</th>
</tr>
<tr>
  <td>variables</td>
  <td>
  
  Tout en minuscules séparé avec un underscore (`_`)</td>
  <td>

  ```python
  number_places = 25
  total_amount = 157.45
  cat_name = "Cookie"
  ```

  </td>
</tr>
<tr>
  <td>boolean</td>
  <td>
  
  Commencent par `is_`, `has_` ou `are_`

  > :information_source: **_INFO_**
  >
  > la plupart du temps, vous utilisez `is_`

  </td>
  <td>

  ```python
  is_married = False
  is_open = True
  ```

  </td>
</tr>
<tr>
  <td>constantes</td>
  <td>
  
  Tout en majuscules séparé par un underscore (`_`)</td>
  <td>

  ```python
  JUNE_NUMBER_DAY = 30
  ```

  </td>
</tr>
<tr>
  <td>privée</td>
  <td>
  
  Précédée par 2 underscore (`__`)</td>
  <td>

  ```python
  __number_places = 25
  ```

  </td>
</tr>
<tr>
  <td>protégée</td>
  <td>
  
  Précédée par 1 underscore (`_`)</td>
  <td>

  ```python
  _number_places = 25
  ```

  </td>
</tr>
<tr>
  <td>fonctions</td>
  <td>
  
  Tout en minuscules séparé avec un underscore (`_`)</td>
  <td>

  ```python
  my_function()
  ```

  </td>
</tr>
<tr>
  <td>arguments de fonctions</td>
  <td>
  
  Tout en minuscules séparé avec un underscore (`_`)</td>
  <td>

  ```python
  my_function(number_places=25)
  ```

  </td>
</tr>
<tr>
  <td>modules</td>
  <td>
  
  Nom court, tout en minuscules, underscore (`_`) si nécessaire</td>
  <td>

  ```python
  account_module
  ```

  </td>
</tr>
<tr>
  <td>modules</td>
  <td>
  
  Nom court, tout en minuscules, underscore (`_`) déconseillé</td>
  <td>

  ```python
  account
  ```

  </td>
</tr>
<tr>
  <td>classes</td>
  <td>
  
  `PascalCase` : Première lettre des mots en majuscule y compris la 1re lettre</td>
  <td>

  ```python
  AccountClass
  ```

  </td>
</tr>
<tr>
  <td>méthodes</td>
  <td>
  
  Tout en minuscules séparé avec un underscore (`_`) avec `self` comme 1re paramètre</td>
  <td>

  ```python
  add_one_place(self)
  ```

  </td>
</tr>
<tr>
  <td>exceptions</td>
  <td>
  
  `PascalCase` avec `Error` à la fin</td>
  <td>

  ```python
  AddNewPlaceError
  ```

  </td>
</tr>
</table>

## Commentaires

- Le caractère `#` permet de commenter 1 ou plusieurs lignes
  
  ```python
  # première ligne de commentaire
  # seconde ligne de commentaire
  ```

- Documentation des méthodes
  (Selon norme [PEP 257](https://www.python.org/dev/peps/pep-0257/))
  
  ```python
  def complex(real=0.0, imag=0.0):
    """Form a complex number.

    Keyword arguments:
    real -- the real part (default 0.0)
    imag -- the imaginary part (default 0.0)
    """
    if imag == 0.0 and real == 0.0:
        return complex_zero
    ...
  ```

## Variables

- Déclaration

  ```python
  # [nom] = [valeur]
  language = "Python"
  ```

- Connaître le type d'une variable : `type()`

  ```python
  language = "Python"
  type(language)
  ```

<!-- 
Exercices : 
  02 - Variables
    01 - Hello World
-->

## Conversions

- Nombre vers une string : `str()`

  ```python
  a = str(12.5)
  ```

- String vers un int : `int()`

  ```python
  a = int("12")
  ```

- Char vers un int : `ord()`

  ```python
  a = ord('b')
  ```

- Un int vers un char : `chr()`

  ``` python
  a = chr(98)
  ```

## Opérateurs

### Arithmétique

Dans les exemples suivants, `a=10` et `b=20`.

| Opérateur | Description                           | Exemple                                                             |
| :-------: | ------------------------------------- | ------------------------------------------------------------------- |
|    `+`    | addition                              | `a + b = 30`                                                        |
|    `-`    | soustraction                          | `a - b = -10`                                                       |
|    `*`    | multiplication                        | `a * b = 200`                                                       |
|    `/`    | division                              | `b / a = 2`                                                         |
|    `%`    | modulo (reste de la division entière) | `b % a = 0`                                                         |
|   `**`    | puissance (ou exposant)               | `a**b = 30`                                                         |
|   `//`    | division euclidienne                  | `9//2 = 4` \| `9//2.0 = 4.0` \| `-11//3 = -4` \| `-11.0//3 = -4.0` |

### Comparaison

Dans les exemples suivants, `a=10` et `b=20`.

|  Opérateur   | Description        | Exemple                                |
| :----------: | ------------------ | -------------------------------------- |
|     `==`     | égalité            | `(a == b) => false`                    |
| `!=` ou `<>` | différent          | `(a != b) => true` \| `(a<>b) => true` |
|     `>`      | plus grand         | `(a > b) => false`                     |
|     `<`      | plus petit         | `(a < b) => true`                      |
|     `>=`     | plus grand ou égal | `(a >= b) => false`                    |
|     `<=`     | plus petit ou égal | `(a <= b) => true`                     |

### Assignement

Les opérateurs arithmétiques peuvent être suivis du symbole `=` pour effectuer simultanément une opération et une affectation. Ainsi, dans les exemples suivants,  `a=10` et `b=20`.

| Opérateur | Exemple                           |
| :-------: | --------------------------------- |
|    `=`    | `c = a + b => c=30`               |
|   `+=`    | `c += a` équivaux à `c = c + a`   |
|   `-=`    | `c -= a` équivaux à `c = c - a`   |
|   `*=`    | `c *= a` équivaux à `c = c * a`   |
|   `/=`    | `c /= a` équivaux à `c = c / a`   |
|   `%=`    | `c %= a` équivaux à `c = c % a`   |
|   `**=`   | `c **= a` équivaux à `c = c ** a` |
|   `//=`   | `c //= a` équivaux à `c = c // a` |

### Logique

| Opérateur | Exemple                                                 |
| :-------: | ------------------------------------------------------- |
|   `and`   | `2 < 3 and 3 < 4 => True` \| `2 < 3 and 3 > 4 => False` |
|   `or`    | `2 < 3 or 3 < 4 => True` \| `2 < 3 or 3 > 4 => True`    |

## Tests conditionnels

- Instruction `if`

  ```python
  a = 10 
  if a > 0:
    print("'a' est positif")

  if a < 0:
    print("'a' est négatif")

  => 'a' est positif
  ```

- Instruction `if ... else`

  ```python
  a = -10 
  if a > 0:
    print("'a' est positif")
  else:
    print("'a' est négatif")

  => 'a' est négatif
  ```

- Instruction `if ... elif ... else`

  ```python
  a = 10
  if a > 30:
    print("'a' est plus grand que 30")
  elif a > 15 and a < 20:
    print("'a' est entre 15 et 20")
  elif a > 5 and a < 15:
    print("'a' est entre 5 et 15")
  else:
    print("'a' ne réponds pas aux critères")

  => 'a' est entre 5 et 15
  ```

- Opérateur ternaire

  ```python
  res = resTrue if test_condition else resFalse
  ```

## Boucles

- Boucle `for`
  
  ```python
  for i in [0, 1, 2, 3, 4]:
    print("i vaut", str(i))
  ```

  ```python
  for i in range(4):
    print("i vaut", str(i))
  ```

  ```python
  a = ["Formation", "Python", "présentée", "par", "dServ", "Solutions", "SàRL"]
  for i in range(len(a)):
    print(a[i])
  ```

  ```python
  a = ["Formation", "Python", "présentée", "par", "dServ", "Solutions", "SàRL"]
  for i in a:
    print(i)
  ```

- Boucle `while`

  ```python
  x = 1
  while x < 10:
    print("x a pour valeur", str(x))
    x *= 2
  ```

### Break, Continue et Pass

L'instruction `break` termine la boucle en cours et reprend l'exécution à l'instruction suivante. L'instruction `break` peut être utilisée dans les boucles `while` et `for`.

```python
for l in 'Python':
   if l == 'h':
      break
   print('Lettre courante :', l)

=> Lettre courante : P
   Lettre courante : y
   Lettre courante : t
```

L'instruction `continue` rejette toutes les instructions restantes de l'itération en cours et ramène le contrôle en haut de la boucle. L'instruction `continue` peut être utilisée dans les boucles `while` et `for`.

```python
for l in 'Python':
   if l == 'h':
      continue
   print('Lettre courante :', l)

=> Lettre courante : P
   Lettre courante : y
   Lettre courante : t
   Lettre courante : o
   Lettre courante : n
```

L'instruction `pass` ne fait rien. Elle est utilisée lorsqu'une instruction est requise syntaxiquement mais que le programme ne nécessite aucune action..

```python
for l in 'Python': 
   if l == 'h':
      pass # TODO : implementer la gestion du 'h'
   print('Lettre courante :', l)

=> Lettre courante : P
   Lettre courante : y
   Lettre courante : t
   Lettre courante : h
   Lettre courante : o
   Lettre courante : n
```

<!-- 
Exercices : 
  03 - Tests conditionnels et boucles
    01 - Christmas tree
    02 - Bazinga
    03 - Factorial
-->

## Structures de données

### Listes

Une `liste` ou `tableau` (`list`/`array`) est un objet qui peut en contenir d'autres.

- Déclaration

  ```python
  l = ["physique", "chimie", 1975, 2019]
  ```

- Accès aux données

  L'accès aux données s'effectue en utilisant des crochets `[x]` où `x` est l'indice (ou index) de l'élément à récupérer. La notation des indices commence à `0`.

  ```python
  print(l[1])

  => chimie
  ```

- Modifier un élément

  ```python
  l[2] = "informatique"
  print(l)

  => ['physique', 'chimie', 'informatique', 2019]  
  ```

- Ajouter un élément

  ```python
  l.append("Python")
  print(l)

  => ['physique', 'chimie', 'informatique', 2019, 'Python']
  ```

- Supprimer un élément

  ```python
  del l[3]
  print(l)

  => ['physique', 'chimie', 'informatique', 'Python']
  ```

- Longueur d'une liste

  ```python
  print(len(l))

  => 4
  ```

### Tuples

Un `tuple` est une `liste` immuable. Le `tuple` consomme moins de mémoire et l'accès aux données qu'il contient est plus rapide.

- Déclaration

  ```python
  l = ("physique", "chimie", 1975, 2019)
  ```

- Longueur d'une liste

  ```python
  print(len(l))

  => 4
  ```

<!-- 
Exercices : 
  04 - Structures de données
    01 - Calendar
-->

### Dictionnaires

Les dictionnaires sont des listes dont chaque élément a une paire clef/valeur dont la clef est immuable.

- Créer un dictionnaire

  ```python
  a = {"Nom": "Rapaz", "Prenom": "Pascal"}

  print(a)

  => {'Nom': 'Rapaz', 'Prenom': 'Pascal'}
  ```

- Ajouter une donnée

  ```python
  a["Ville"] = "Monthey"

  print(a)

  => {'Nom': 'Rapaz', 'Prenom': 'Pascal', 'Ville': 'Monthey'}
  ```

- Modifier une donnée

  ```python
  a["Ville"] = "Lausanne"

  print(a)

  => {'Nom': 'Rapaz', 'Prenom': 'Pascal', 'Ville': 'Lausanne'}
  ```

- Récupérer une valeur

  ```python
  print a.get("Prenom")

  => Pascal
  ```

- Supprimer une entrée

  ```python
  del a.["Nom"]

  print(a)

  => {'Prenom': 'Pascal', 'Ville': 'Monthey'}
  ```

- Rechercher une clef

  ```python
  print("Adresse" in a)

  => False
  ```

- Boucler sur le contenu d'un dictionnaire

  ```python
  # Récupérer les clefs
  for key in a.keys():
    print(key)

  => Nom
     Prenom
     Ville

  # Récupérer les valeurs
  for value in a.values():
    print(value)

  => Rapaz
     Pascal
     Lausanne
  
  # Récupérer les clefs et les valeurs
  for key, value in a.items():
    print(key, value)

  => Nom Rapaz
     Prenom Pascal
     Ville Lausanne
  ```

- Copier un dictionnaire

  Vous ne pouvez pas copier un dictionnaire en faisant `dic1 = dic2`, vous **devez** utiliser la méthode `copy`

  ```python
  b = a.copy()

  print(b)

  => {'Nom': 'Rapaz', 'Prenom': 'Pascal', 'Ville': 'Lausanne'}
  ```

- Fusionner 2 dictionnaires

  ```python
  a = {'Nom': 'Rapaz'}
  b = {'Prenom': 'Pascal'}
  a.update(b)
  
  print(a)

  => {'Nom': 'Rapaz', 'Prenom': 'Pascal'}
  ```

<!--
Exercices : 
  04 - Structures de données
    02 - Network
-->

### Set et Frozenset

Les `set` et `frozenset` sont des types de données standard qui stockent des valeurs à l'instar des `list` et des `tuples`.

La différence réside dans le fait que les `set` et `frozenset` n'autorisent pas les doublons.

#### Set

- Déclaration

  ```python
  s = set() # cree un set vide

  s1 = {"Linux", "since", 1991}
  # ou
  s2 = set(["Linux", "since", 1991])

  print(s1)

  => {'since', 'Linux', 1991}
  ```

- Ajouter une donnée

  ```python
  s1.add("Linus")

  print(s1)

  => {'since', 'Linus', 'Linux', 1991}
  ```

- Copier un `set`

  Vous ne pouvez pas copier un `set` en faisant `set1 = set2`, vous **devez** utiliser la méthode `copy`

  ```python
  s2 = s1.copy()

  print(s2)

  => {'since', 'Linus', 'Linux', 1991}
  ```

- Fusionner 2 `set`

  ```python
  s2 = {"Marc", "Willy"}
  s1.update(s2)
  
  print(s1)

  => {'Marc', 1991, 'since', 'Linux', 'Willy'}
  ```

- Récupérer un élément

  Vous ne pouvez pas accéder aux éléments d'un `set` en faisant référence à un index, car les éléments contenus dans les `set` n'ont pas d'index.

  Toutefois, vous pouvez utiliser les boucles pour accéder aux données ou spécifier une valeur afin de savoir si elle est présente ou non dans le `set`.

  ```python
  s = {"pomme", "poire", "abricot"}

  # Boucle sur tous les elements
  for i in s:
    print(i)

  => poire
     pomme
     abricot

  # Rechercher la presence d'un element
  print("pomme" in s)

  => True
  ```

- Supprimer une entrée

  ```python
  # Supprime un element et leve une exception s'il n'existe pas
  s1.remove(1991)

  # Supprime un element sans lever d'exception
  s1.discard(1991)

  print(s1)

  => {'since', 'Linus', 'Linux'}

  # Vide le contenu du set
  s1.clear()

  => set()
  ```

- Opération mathématiques
  
  - Différence entre 2 set

    ```python
    s1 = {"Kevin", "Kilian", "Morgane", "Gustave"}
    s2 = {"Kevin", "Gustave"}
    s3 = s1 - s2
    print(s3)

    => {'Kilian', 'Morgane'}
    ```

  - Union de 2 sets

    ```python
    s1 = {"Kevin", "Kilian", "Morgane", "Gustave"}
    s2 = {"Kevin", "Gustave"}
    s3 = s1 | s2
    print(s3)

    => {'Gustave', 'Kilian', 'Kevin', 'Morgane'}
    ```

  - Intersection de 2 sets

    ```python
    s1 = {"Kevin", "Kilian", "Morgane", "Gustave"}
    s2 = {"Kevin", "Gustave"}
    s3 = s1 & s2
    print(s3)

    => {'Gustave', 'Kevin'}
    ```

#### Frozenset

Les `set` sont des objets modifiables donc il n’est pas possible d’utiliser un `set` comme clef de dictionnaire. Il n’est pas possible non plus de placer un `set` dans un autre `set`. Dans ce cas, le `frozenset` peut être utilisé car il est immuable.

- Déclaration

  ```python
  f = frozenset(["Linux", "since", 1991])

  print(f)

  => frozenset(['Linux', 'since', 1991])

  # Exemple d'ajout d'un frozenset dans un set
  s = {"Marc", f, "Willy"}

  print(s)

  => {'Marc', 'Willy', frozenset({'since', 'Linux', 1991})}
  ```

- Copier un `frozenset`
  
  Comme les `set`, vous ne pouvez pas copier un `frozenset` en faisant `frozenset1 = frozenset2`, vous **devez** utiliser la méthode `copy`

<!--
Exercices : 
  04 - Structures de données
    03 - MinMax
-->

## Slicing

Le `slicing` permet de "découper" des structures de données tels que les chaînes de caractères ou les listes.

Exemple:

```python
a="Hello World"
print(a[2:5])

=> llo
```

Notation :

```python
a[start:stop:step]
```

`start` indice du 1<sup>er</sup> élément à inclure

`stop` indice du 1<sup>er</sup> élément qui ne **doit** pas être inclus

`step` le "pas" à utiliser (option facultative, défaut = 1)

```python
a[start:stop]  # elements entre start et stop-1
a[start:]      # elements depuis start jusqu'a la fin
a[:stop]       # elements depuis le debut jusqu'a stop-1
a[:]           # copie de tous les elements

# le "step" peut etre utilise avec toutes les formules ci-dessus
```

<!--
Exercices : 
  05 - Slicing
    01 - Pizzas
    02 - World
-->

## Print

Pour afficher dans un terminal, on utilise la fonction `print()`

```python
print("Hello World !")

=> Hello World !
```

Il est possible de réaliser plusieurs affichages à la suite. Pour cela, on sépare les éléments par des virgules.

```python
print("Hello", "World", "!")

=> Hello World !
```

## Formater une string

### f-Strings

> :information_source: **_INFO_**
>
> Cette manière de formater une chaîne de caractères a été introduite avec Python 3.6. Pour les versions antérieures, référez-vous aux méthodes [Format](#Format) et/ou [Opérateur %](#Opérateur-%).

`f-Strings` est une méthode de formatage qui permet plusieurs substitutions de valeurs dans une chaîne de caractères. Cette méthode permet de concaténer des éléments dans une chaîne via un formatage positionnel.

Les formateurs remplacent un ou plusieurs champs représentés par des paires d'accolades `{}` dans une chaîne de caractères.

Pour indiquer à Python que vous souhaitez formater une chaîne de caractères, vous devez la précéder du caractère `f` ou `F`.

```python
nom = "Doe"
prenom = "John"

print(f"Hello {prenom} {nom} !")
print(F"Hello {prenom} {nom} !")

=> Hello John Doe !
   Hello John Doe !
```

#### Expressions arbitraires

Le contenu entre accolades étant évalué à l'exécution, il est possible d'y mettre une expression valide, un appel de fonction, etc.

```python
formation = 'Python'
contact = {'nom': 'Doe', 'prenom': 'John', 'age': 37}

print(f"{2 * 37}")
print(f"{formation.upper()}")
print(f"M. {contact['nom']} a {contact['age']} ans.")

=> 74
   PYTHON
   M. Doe a 37 ans.
```

Si un objet est passé en paramètre, `f-strings` appelera les méthodes `__str__()` ou `__repr__()` conformément aux règles décrites dans le chapitre [Fonction \_\_repr\_\_ et \_\_str\_\_](#Fonction-\_\_repr\_\_-et-\_\_str\_\_).

```python
import datetime
today = datetime.datetime.now()

print(f"__str__  : {today}")
print(f"__repr__ : {repr(today)}")

=> __str__  : 2019-11-27 22:27:42.719296
   __repr__ : datetime.datetime(2019, 11, 27, 22, 27, 42, 719296)
```

#### Lignes multiples

Il est possible d'avoir des chaînes de caractères sur plusieurs lignes, dans ce cas, chaque chaîne devra être précédée par le caractère `f` ou `F`

```python
print(f"Bonjour {prenom}, "
      f"Comment allez-vous ?")

=> Bonjour John, Comment allez-vous ?
```

#### Types spécifiques

Il est possible de typer les arguments utilisés pour le remplacement des accolades : `{champ:type}`

|  Type   | Description                                                         |
| :-----: | ------------------------------------------------------------------- |
|   `s`   | chaîne de caractère (`string`)                                      |
|   `d`   | nombres entier (`integer`)                                          |
|   `f`   | nombres à virgule (`float`)                                         |
| `.<x>f` | nombres à virgule avec un nombre fixe de chiffres à droite du point |
|   `c`   | caractère (`char`)                                                  |
|   `b`   | représentation notation binaire d'un nombre entier (`binary`)       |
|   `o`   | représentation notation octale d'un nombre entier (`octal`)         |
|  `x/X`  | représentation hexadécimal d'un nombre entier (minuscule/majuscule) |
|   `e`   | notation exponentielle                                              |

```python
a = 42
print(f"int: {a:d}; hex: {a:x}; oct: {a:o}; bin: {a:b}; exp: {a:e}")

=> int: 42; hex: 2a; oct: 52; bin: 101010; exp: 4.200000e+01
```

#### Justification

Par défaut, le texte est aligné à gauche et les nombres à droite. Il est possible de modifier cet alignement en plaçant un code juste après les deux points `:` dans le champ à remplacer.

| Type  | Description         |
| :---: | ------------------- |
|  `<`  | alignement à gauche |
|  `^`  | centré              |
|  `>`  | alignement à droite |

> :information_source: **_INFO_**
>
> L'alignement peut également contenir un nombre qui représentera la taille de la zone ainsi qu'un caractère de remplissage : `f"{'Python':*^12s}" => ***Python***`

``` python
python = "Python"
an = 1991

print(f"{python} a été créé en {an:10}!")
=> Python a été créé en       1991!

print(f"{python:^10} a été créé en {an:<10}!")
=> Python   a été créé en 1991      !

print(f"{python:*^12s}")
=> ***Python***
```

### Format

Tout comme [f-Strings](#f-Strings), `format` est une méthode de formatage qui substitue le contenu des champs représentés par des paires d'accolades `{}` dans une chaîne de caractères.

```python
print("{} est un excellent langage de programmation !".format("Python"))

=> Python est un excellent langage de programmation !

s = "Ce cours est écrit en {}, l extension des fichiers doit être {}."
print(s.format("Markdown", "'.md'"))

=> Ce cours est écrit en Markdown, l extension des fichiers doit être '.md'.
```

Dans certains cas, il peut être utile d'identifier les arguments, notamment si l'on rajoute des blocs de remplacement qui n'étaient pas prévu initialement dans la chaîne de caractères.

```python
s = "Ce cours est écrit en {1}, l extension des fichiers doit être {0}."
print(s.format("'.md'", "Markdown"))

=> Ce cours est écrit en Markdown, l extension des fichiers doit être '.md'.

s = "Ce cours est écrit en {lang}, l extension des fichiers doit être {0}."
print(s.format("'.md'", lang="Markdown"))

=> Ce cours est écrit en Markdown, l extension des fichiers doit être '.md'.
```

#### Types spécifiques

Il est possible de typer les arguments utilisés pour le remplacement des accolades, les règles sont identiques aux [Types spécifiques](#Types-spécifiques) de [f-Strings](#f-Strings).

```python
print("int: {0:d}; hex: {0:x}; oct: {0:o}; bin: {0:b}; exp: {0:e}".format(42))

=> int: 42; hex: 2a; oct: 52; bin: 101010; exp: 4.200000e+01
```

#### Justification

Il est possible de modifier l'alignement du texte et des nombres selon les mêmes règles de [Justification](#Justification) que [f-Strings](#f-Strings).

```python
print("{0} a été créé en {1:10}!".format("Python", 1991))
=> Python a été créé en       1991!

print("{0:^10} a été créé en {1:<10}!".format("Python", 1991))
=>   Python   a été créé en 1991      !

print("{:*^12s}".format("Python"))
=> ***Python***`
```

### Opérateur `%`

> :information_source: **_INFO_**
>
> Ancienne méthode pour formatter les chaînes de caractères  

Il est possible de formater la sortie avec l'opérateur `%` suivi d'un `tuple` contenant les valeurs.

```python
print("Hello %s !" % "World")

=> Hello World !
```

Dans ce cas, on utilise des arguments pour formater la chaîne de caractères :

| Argument | Description                                                         |
| :------: | ------------------------------------------------------------------- |
|   `%s`   | Converti l'objet en `string` avec la fonction `str()`               |
|   `%r`   | Converti l'objet en `string` avec la fonction `repr()`              |
|   `%d`   | nombres entier (`integer`)                                          |
|   `%f`   | nombres à virgule (`float`)                                         |
| `%.<x>f` | nombres à virgule avec un nombre fixe de chiffres à droite du point |
| `%x/%X`  | représentation hexadécimal d'un nombre entier (minuscule/majuscule) |

```python
name = "John"
age = 23
print("%s a %d ans" % (name, age))

=> John a 23 ans

l = [1, 2, 3]
print("Contenu de l: %s" % l)

=> Contenu de l: [1, 2, 3]
```

<!--
Exercices : 
  06 - Formater une string
    01 - Bank
-->

## Logs

Afin de déterminer quand utiliser les différents outils permettant de gérer les logs, vous pouvez vous référer au tableau suivant :

| Tâche à réaliser                                                                  | Méthode                                                                                                                                                                                                    |
| --------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Afficher dans le terminal pour une utilisation en ligne de commande               | `print()`                                                                                                                                                                                                  |
| Signaler un événement qui survient durant le fonctionnement normal d'un programme | `logging.info()` (ou `logging.debug()` pour une sortie plus détaillée)                                                                                                                                     |
| Émettre un avertissement pour un événement particulier                            | `warnings.warn()` si le problème est évitable et que l'application doit être adaptée pour l'éliminer<br /><br />`logging.warning()` si le problème n'est pas évitable mais que l'événement doit être signalé |
| Signaler une erreur concernant un événement particulier                           | Levez une exception                                                                                                                                                                                        |
| Signaler une erreur sans déclencher une exception                                 | `logging.error()`,<br />`logging.exception()` ou<br /> `logging.critical()` selon le cas spécifique d'erreur et d'application                                                                                |

Le niveau du log sera choisi en fonction de la gravité des événements qui surviennent :

| Niveau            | Utilisation                                                                                                                                    |
| ----------------- | ---------------------------------------------------------------------------------------------------------------------------------------------- |
| `DEBUG`           | Informations détaillées afin de pouvoir diagnostiquer un problème                                                                              |
| `INFO`            | Confirmation que les choses fonctionnent comme prévu                                                                                           |
| *)&nbsp;`WARNING` | Indique qu’un événement imprévu est survenu ou indique un problème dans un avenir proche néanmoins le logiciel fonctionne toujours comme prévu |
| `ERROR`           | En raison d'un problème plus grave, le logiciel n'a pas été en mesure d'exécuter certaines fonctions                                           |
| `CRITICAL`        | Une erreur grave est survenue, indiquant que le programme lui-même pourrait ne pas pouvoir continuer à s'exécuter                              |

*\*) niveau par défaut*

### Utilisation

```python
import logging

logging.warning('Espace disque faible') # affiche le message dans la console
logging.info('Connexion établie')       # n'affiche pas le message par défaut

logging.getLogger().setLevel(logging.INFO) # change le niveau d'affichage
logging.info('Fichier créé') # affiche le message dans la console
```

```python
import warnings

warnings.warn('Obsolète depuis version 1.0')
```

### Enregistrer les logs dans un fichier

```python
import logging

# filemode 'w' cree un nouveau fichier à chaque exécution
#          'a' ajoute au fichier existant (defaut)
logging.basicConfig(filename='example.log', filemode='w', level=logging.INFO)

logging.debug('Ce message ne sera pas dans le fichier de log')
logging.info('Ce message sera dans le fichier de log')
logging.warning('... ainsi que celui-ci')
```

### Gérer plusieurs sorties

Il est possible d'avoir plusieurs sorties pour les logs avec, pour chaque sortie, un niveau de détail différent. Dans ce cas, on affectera un `handler` qui se chargera de gérer la nouvelle sortie.

```python
# Collecte tous les logs dans un fichier
logging.basicConfig(filename='execution.log', level=logging.DEBUG)

### 
# Nouvel handler pour afficher egalement dans la console les 
# messages WARNING, ERROR et CRITICAL
logger = logging.getLogger()

# (Optionnel) creation d'un format personnalise pour la sortie dans la console
# (correspond au format par defaut)
fmt = logging.Formatter("%(levelname)s:%(name)s:%(message)s")

logConsole = logging.StreamHandler()
logConsole.setFormatter(fmt)
logConsole.setLevel(logging.WARNING)
logger.addHandler(logConsole)
###

logging.info('Détenteur %s' % data[0])
logging.debug('Solde actuel du compte %d' % data[1])
```

<!--
Exercices : 
  07 - Logs
    01 - Bank2
-->

## Exceptions

### Gérer une exception

Le bloc `try` permet de tester un bloc de code pour rechercher des erreurs

Le bloc `except` permet de gérer l'erreur

Le bloc `finally` permet d'exécuter du code, quel que soit le résultat des blocs `try` et `except`

```python
try:
  print('Hello World')
except ValueError:
   pass # TODO : gérer l'exception ValueError
except (TypeError, ZeroDivisionError):
   pass # TODO : gérer les exceptions TypeError et ZeroDivisionError
except:
  print('Quelque chose a mal fonctionné')
else:
  print('Tout a bien fonctionné')
finally:
  print("Instruction 'try / except' est terminée")

=> Hello World
   Tout a bien fonctionné
   Instruction 'try / except' est terminée
```

### Lever une exception

L'instruction `raise` permet de déclencher une exception spécifique.

```python
a = -5
try:
  if a <= 0:
    raise ValueError("'a' est négatif: %d" % a)
except ValueError as ve:
  print(ve)

=> 'a' est négatif: -5
```

## Fichiers

Pour éditer un fichier en python on utilise la fonction `open`. Une fois terminé, il ne faut pas oublier de fermer le fichier avec la fonction `close()`.

La fonction `open` prend en premier paramètre le chemin du fichier (relatif ou absolu) et en second le type d'ouverture :

| Type  | Description                                                                                                                           |
| :---: | ------------------------------------------------------------------------------------------------------------------------------------- |
|  `r`  | ouvre le fichier en lecture (`READ`)                                                                                                  |
|  `w`  | ouvre le fichier en écriture (`WRITE`), à chaque ouverture le contenu du fichier est écrasé. Si le fichier n'existe pas, il sera créé |
|  `a`  | ouvre le fichier en mode ajout à la fin du fichier (`APPEND`). Si le fichier n'existe pas, il sera créé                               |
|  `b`  | ouvre le fichier en mode binaire                                                                                                      |
|  `t`  | ouvre le fichier en mode texte                                                                                                        |
|  `x`  | crée un nouveau fichier et l'ouvre pour écriture                                                                                      |

### Lire un fichier

Pour afficher le contenu d'un fichier, on utilise la méthode `read`

```python
try:
  f = open("data.txt", "r")
  print(f.read())
finally:
  f.close()
```

### Écrire dans un fichier

Pour écrire dans un fichier, on utilise la méthode `write`

``` python
try:
  f = open("data.txt")
  f.write("Lorum Ipsum")
finally:
  f.close()
```

### Le mot clef `with`

Il existe une autre syntaxe qui permet de s'émanciper du problème de fermeture du fichier grâce au mot clef `with`

```python
with open("data.txt", "r") as f:
  print(f.read())
```

<!--
Exercices : 
  08 - Exceptions et fichiers
    01 - DisplayLog
    02 - FilterLog
-->

### Format YAML et JSON

Les formats de fichiers `YAML` et `JSON` utilisent des associations clefs/valeurs pour organiser les données. Ils sont donc très proche de la structure de données "dictionnaire" en Python ce qui simplifie l'importation et l'exportation des données.

- Import/Export YAML

  > :information_source: **_INFO_**
  >
  > Nécessite l'installation du module `pyyaml` sous Windows : `pip install pyyaml`

  ```python
  import yaml

  datas = {'Nom': 'Doe', 'Prenom': 'John', 'Ville': 'Martigny'}

  # save YAML
  with open("yamldatas.yaml", "w") as f:
    doc = yaml.dump(datas, f)

  # open YAML
  with open("yamldatas.yaml", "r") as f:
    doc = yaml.full_load(f)

  print("datas:", datas)
  print("yaml :", doc)

  => datas: {'Nom': 'Doe', 'Prenom': 'John', 'Ville': 'Martigny'}
     yaml : {'Nom': 'Doe', 'Prenom': 'John', 'Ville': 'Martigny'}
  ```

- Import/Export JSON
  
  ```python
  import json

  datas = {'Nom': 'Doe', 'Prenom': 'John', 'Ville': 'Martigny'}

  # save JSON
  with open("jsondata.json", "w") as f:
    doc = json.dump(datas, f)

  # open JSON
  with open("jsondata.json", "r") as f:
    doc = json.load(f)

  print("datas:", datas)
  print("json :", doc)

  => datas: {'Nom': 'Doe', 'Prenom': 'John', 'Ville': 'Martigny'}
     json : {'Nom': 'Doe', 'Prenom': 'John', 'Ville': 'Martigny'}
  ```

## Fonctions

Lorsqu'une tâche doit être réalisée plusieurs fois par un programme avec seulement des paramètres différents, on peut l'isoler au sein d’une fonction.

### Syntaxe

Une fonction se déclare avec le mot clef `def`. Vous pouvez choisir n'importe quel nom pour la fonction que vous créez, à l'exception des mots-clefs réservés au langage, et à la condition de n'utiliser aucun caractère spécial ou accentué (le caractère souligné `'_'` est permis).

De plus, il ne peut pas y avoir 2 fonctions portant le même nom dans un fichier ou une classe.

```python
# Fonction sans parametre
def hello():
  print("Hello World !")

# Fonction avec parametre
def hello_world(text):
  print(text)

# Appel de fonctions
hello()
hello_world("Hello World !")
```

### Fonctions à plusieurs paramètres

Pour définir une fonction avec plusieurs paramètres, il suffit d'inclure ceux-ci entre les parenthèses qui suivent le nom de la fonction, en les séparant à l'aide de virgules.

Lors de l'appel de la fonction, les arguments utilisés doivent être fournis dans le même ordre que celui des paramètres correspondants (en les séparant eux aussi à l’aide de virgules).

```python
def table_multiplication(base, debut, fin):
  print(f'Fragment table de multiplication par {base:d} :')
  n = debut
  while n <= fin :
    print(n, 'x', base, '=', n * base)
    n += 1

table_multiplication(8, 13, 15)

=> Fragment table de multiplication par 8 :
   13 x 8 = 104
   14 x 8 = 112
   15 x 8 = 120
```

### Valeurs par défaut

Il est possible de donner des valeurs par défaut aux arguments d'une fonction.

```python
def log_file(file_name, folder="/tmp/"):
  print(f"Le fichier de log {file_name} sera stocké dans le dossier {folder}\n"
        f"Le chemin complet est : {folder}{file_name}")

log_file("execution.log")
log_file("execution.log", "C:\\temp\\")

=> Le fichier de log execution.log sera stocké dans le dossier /tmp/
   Le chemin complet est : /tmp/execution.log
   Le fichier de log execution.log sera stocké dans le dossier C:\temp\
   Le chemin complet est : C:\temp\execution.log
```

### Variables locales et globales

Chaque fois que nous définissons des variables à l'intérieur d'une fonction, ces variables ne sont accessibles qu'à la fonction elle-même. On dit que ces variables sont des variables `locales`.

Les variables définies à l'extérieur d'une fonction sont des variables `globales`. Leur contenu est "visible" de l'intérieur d'une fonction, mais la fonction ne peut pas le modifier.

```python
a = 2
b = 7

def sample():
  b = 5
  print(a, b)

sample()
print(a, b)

=> 2 5
   2 7
```

Pour modifier une variable globale à l'intérieur d'une fonction, vous devez indiquer explicitement que la variable en question est globale à l'aide de l'instruction `global`

```python
a = 2
b = 7

def sample():
  global b 
  b = 5
  print(a, b)

sample()
print(a, b)

=> 2 5
   2 5
```

### Valeurs de retour

Une fonction ne doit pas forcément retourner une valeur (dans ce cas, elle retourne `None`). Toutefois, la plupart des fonctions doivent retourner des valeurs afin que le résultat puisse être utilisé pour une nouvelle opération.

L'instruction `return` va terminer l'exécution d'une fonction et retourner une ou plusieurs valeurs (données composites). Cela signifie qu'on placera généralement cette instruction en fin de fonction puisque le code suivant une instruction `return` ne sera jamais exécuté.

```python
def order(a, b):
  if a < b:
    return a, b
  else:
    return b, a

res = order(10, 3)
print(res)

=> (3, 10)
```

### Fonction main

Contrairement à d'autres langages, Python n'a pas de fonction ou méthode `main()`. Quand on lance un script, tout le script est exécuté et aucune fonction n'est appelée automatiquement.

Cela pose un problème quand on a un script qui contient du code que l'on souhaite exécuter quand on lance le script directement, mais pas quand on l'importe dans un autre script. Dans ce cas, on va tester le contenu de la variable `__name__` qui est une variable automatiquement créée et toujours disponible.

Cette variable contient le nom du script courant si on importe le script. Mais si le script est le script principal, alors `__name__` ne contient pas le nom du script mais la chaîne de caractères `__main__`.

Il suffit donc de vérifier le contenu de la variable `__name__` pour simuler une méthode `main()`.

```python
def hello_world():
  print("Hello World !")

if __name__ == '__main__':
  hello_world()
```

<!--
Exercices : 
  09 - Fonctions
    01 - Unique
    02 - Palindrome
-->

## Classes

Une classe regroupe des fonctions et des attributs qui définissent un objet. On appelle les fonctions d'une classe des "méthodes".

### Syntaxe

Une classe se déclare avec le mot clef `class`. Vous pouvez choisir n'importe quel nom pour une classe, mais par convention, elle sera en `PascalCase` (la première lettre des mots en majuscule y compris le premier mot) et ne contiendra pas d'accents ni de caractères spéciaux.

La fonction `__init__` est appelée à la création de l'objet.

Quant à l'attribut `self`, il représente l'instance en cours et permet de stocker une information dans une classe.

```python
class FormationPython:
  def __init__(self):
    self.version = 3.8
```

### Objets

Un objet est une instance de classe. On peut créer autant d'objets que l'on désire avec une classe.

```python
ma_formation = FormationPython()
```

### Méthodes

Les méthodes sont des fonctions définies dans une classe.

```python
class FormationPython:
  def __init__(self):
    self.version = 3.8
  
  def version_langage(self):
    return self.version

ma_formation = FormationPython()
print(ma_formation.version_langage())

=> 3.8
```

### Attributs de classe

Les attributs de classe permettent de stocker des informations au niveau de la classe. Ils sont similaires aux variables.

Vous pouvez à tout moment créer un attribut pour votre objet.

```python
ma_formation.annee = 2020 # nouvel attribut 'annee'

print(f"Formation python version {ma_formation.version:.1f} année {ma_formation.annee} !")

=> Formation python version 3.8 année 2020 !
```

### Getter et setter

En orienté objets, il est rare que l'on modifie les attributs d'un objet sans passer par des `getter` et des `setter`.

> :information_source: **_INFO_**
>
> Par convention, les méthodes et attributs privés (`private`) commencent par 2 underscores `'__'` en Python

```python
class FormationPython:
  def get_version(self):
    print("Accède avec le getter")
    return self.__version

  def set_version(self, version):
    print("Modifie avec le setter")
    self.__version = version

ma_formation = FormationPython()
ma_formation.set_version("3.8.0")
print(ma_formation.get_version())

=> Modifie avec le setter
   Accède avec le getter
   3.8.0
```

#### Décorateurs

Le décorateur `@property` permet d'accéder à la valeur d'un attribut sans utiliser de méthode `getter`.

Le décorateur `@<attribut>.setter` permet, quant à lui, de modifier la valeur d'un attribut sans utiliser de méthode `setter`.

Cette syntaxe rend le code plus lisible.

```python
class FormationPython:
  @property
  def version(self):
    print("Accède avec le getter")
    return self.__version

  @version.setter
  def version(self, v):
    print("Modifie avec le setter")
    self.__version = v

ma_formation = FormationPython()
ma_formation.version = "3.8.0"
print(ma_formation.version)

=> Modifie avec le setter
   Accède avec le getter
   3.8.0
```

#### Propriétés

L'usage des propriétés permet de rendre privé les `getter` et les `setter` des attributs d'un objet.

```python
class FormationPython:
  def __get_version(self):
    print("Accède avec le getter")
    return self.__version

  def __set_version(self, v):
    print("Modifie avec le setter")
    self.__version = v

  version = property(__get_version, __set_version)

ma_formation = FormationPython()
ma_formation.version = "3.8.0"
print(ma_formation.version)

=> Modifie avec le setter
   Accède avec le getter
   3.8.0
```

### Héritage

L'héritage permet de construire une classe à partir d'une autre. La classe enfant hérite de toutes les méthodes et attributs de la classe parent.

Une fois la classe parent définie, il faut l'initialiser. Pour cela, il suffit d'appeler le constructeur de la classe parent dans le constructeur de la classe fille. (`Personne.__init__(...)` dans l'exemple ci-après).

> :information_source: **_INFO_**
>
> Il est possible de faire de l'héritage multiple en Python, ce qui veut dire qu'une classe enfant peut hériter de plusieurs classes parents. Les classes parents seront séparées par des virgules lors de la déclaration de la classe enfant.
>
> Dans le cas de l'utilisation de l'héritage multiple, il faudra faire attention qu'il n'y ait pas de nom de méthodes ou d'attributs communs à plusieurs classes dans l'arbre d'héritage. En effet, au mieux Python vous indiquera un conflit, au pire il en choisira un au hasard parmi les disponibles.

```python
# Classe parent
class Personne:
  def __init__(self, nom, prenom):
    self.__nom = nom
    self.__prenom = prenom

  @property
  def nom(self):
    return self.__nom

  @property
  def prenom(self):
    return self.__prenom

  @nom.setter
  def nom(self, n):
    self.__nom = n

  @prenom.setter
  def prenom(self, n):
    self.__prenom = n

# Classe enfant
class Contact(Personne):
  def __init__(self, nom, prenom, telephone):
    # Appel du constructeur de la classe parent
    Personne.__init__(self, nom, prenom)
    self.__telephone = telephone

  @property
  def telephone(self):
    return self.__telephone

  @telephone.setter
  def telephone(self, v):
    self.__telephone = v

c = Contact("Doe", "John", "+41 (0)78 342 11 23")
print(f"{c.prenom} {c.nom} - {c.telephone}")

=> John Doe - +41 (0)78 342 11 23
```

### Polymorphisme

Une classe enfant peut redéfinir une méthode de la classe parent. On parle alors de surcharge de méthode ou de polymorphisme.

```python
class Pingouin:
  def voler(self):
    return "Un pingouin sait voler"

class Manchot(Pingouin):
  def voler(self):
    return "Un manchot ne sait pas voler"

pingouin = Pingouin()
manchot = Manchot()

print(pingouin.voler())
print(manchot.voler())

=> Un pingouin sait voler
   Un manchot ne sait pas voler
```

### Divers

#### Fonction \_\_repr\_\_ et \_\_str\_\_

`__repr__` et `__str__` sont des méthodes spéciales, comme `__init__`, qui sont censées renvoyer une représentation textuelle d'un objet. Si la méthode `__str__` n'existe pas, la fonction `print()` cherchera la méthode `__repr__`, sinon elle affichera l'adresse mémoire de l'objet.

- `__repr__`, appelée par la fonction `repr()`, retourne la représentation "officielle" d'un objet. La méthode `__repr__` doit être utilisable comme argument de la méthode `eval()` dont la valeur de retour doit être une chaîne de caractères valide. Cette méthode est utilisée par les développeurs dans les phases de debug.
- `__str__`, appelée par la fonction `str()`, retourne la représentation "informelle" d'un objet. Elle est utilisée par les utilisateurs finaux.

> :information_source: **_INFO_**
>  
> Toutes les classes devraient avoir une implémentation de la méthode `__repr__` et seules les classes qui ont besoin de plus de lisibilité auront la méthode `__str__`.

```python
import datetime
today = datetime.datetime.now()

print("__str__  :", str(today))
print("__repr__ :", repr(today))

=> __str__  : 2019-11-24 14:25:09.741616
   __repr__ : datetime.datetime(2019, 11, 24, 14, 25, 9, 741616)
```

- Exemple d'implémentation
  
  ```python
  class Personne:
    nom = ""
    prenom = ""

    def __init__(self, nom, prenom):
      self.nom = nom
      self.prenom = prenom

    def __repr__(self):
      return f"Personne(nom='{self.nom}', prenom='{self.prenom}')"

    def __str__(self):
      return f'{self.nom} {self.prenom}'

  p = Personne("Doe", "John")

  print("Utilise implicitement str()          : ", p)
  print("Appel explicite de la méthode str()  : ", str(p))
  print("Appel explicite de la méthode repr() : ", repr(p))
  print("Evalue le retour de repr() qui crée un objet " 
        "puis utilise implicitement str() :", eval(repr(p)))

  => Utilise implicitement str()          :  Doe John
     Appel explicite de la méthode str()  :  Doe John
     Appel explicite de la méthode repr() :  Personne(nom='Doe', prenom='John')
     Evalue le retour de repr() qui crée un objet puis utilise implicitement str() : Doe John
  ```

#### Fonction `dir` et attribut `__dict__`

Parfois il est intéressant de pouvoir décortiquer un objet pour résoudre un bug ou pour comprendre un script.

La fonction `dir` et l'attribut `__dict__` donne respectivement les méthodes et attributs d'un objet.

```python
contact = Contact("Doe", "John", "+41 (0)78 342 11 23")

print("dir()   :", dir(contact))
print("__dict__:", contact.__dict__)

=> dir()   : ['_Contact__telephone', '_Personne__nom', '_Personne__prenom',
              '__class__', '__delattr__', '__dict__', '__dir__', '__doc__',
              '__eq__', '__format__', '__ge__', '__getattribute__', '__gt__',
              '__hash__', '__init__', '__init_subclass__', '__le__', '__lt__', 
              '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__',
              '__repr__', '__setattr__', '__sizeof__', '__str__', 
              '__subclasshook__', '__weakref__', 'nom', 'prenom', 'telephone']

   __dict__ : {'_Personne__nom': 'Doe', '_Personne__prenom': 'John', 
               '_Contact__telephone': '+41 (0)78 342 11 23'}
```

<!--
Exercices : 
  10 - Classes
    01 - Animaux
    02 - Lévrier
-->

## Modules et packages

Un `module` est un fichier portant l'extention `.py` qui regroupe des attributs, des fonctions et/ou des classes. Un module peut être utilisé (importé) depuis un autre fichier à l'aide du mot-clef `import`.

Un `package` est un dossier qui va regrouper un ensemble de `modules`.

### Création d'un package

> :information_source: **_INFO_**
>
> Avant Python 3.5, pour être un package, un répertoire devait contenir un fichier `__init__.py`. Ce n'est plus obligatoire aujourd'hui, mais c'est toujours utile si l'on désire avoir une configuration plus fine.

```bash
project
├── __init__.py
├── config
│   ├── __init__.py
│   └── constants.py
├── core
│   ├── __init__.py
│   └── common.py
└── utils
    ├── __init__.py
    └── helper.py
```

### Import d'un package

Les modules ne peuvent être importés que s'ils appartiennent à un répertoire de recherche référencé. Ces répertoires sont par exemple les dossiers d’installation de Python (comprenant la bibliothèque standard), les dossiers d’installation des bibliothèques tierces, et le répertoire courant. Ainsi, un module dans un autre répertoire que ceux cités précédemment ne sera pas accessible.

Plusieurs syntaxes différentes existent pour importer un module.

- On importe de l'entier du module et on utilise ce dont on a besoin

  ```python
  import config.constants

  config.constants.fonction()
  ```

- On importe le strict nécessaire depuis le module

  ```python
  from config import constants
  
  constants.fonction()
  ```

- On importe tout le contenu du module avec le caractère `'*'`

  ```python
  from config.constants import *

  fonction()
  ```

  > :information_source: **_INFO_**
  >
  > Cette dernière méthode est à éviter car cela encombre inutilement l'espace de noms.

<!--
Exercices : 
  11 - Modules et packages
    01 - Géométrie
-->

## Tests unitaires

Python propose de base deux méthodes pour écrire des tests unitaires. La première, `unittest`, consiste à écrire un ou plusieurs modules dédiés aux tests. Le seconde, `doctest`, se base sur le contenu de la documentation des fonctions pour effectuer les tests.

### unittest

Le module `unittest` est le module standard inclus dans les librairies Python pour les tests unitaires.

Pour créer un cas test, vous devez créer une classe nommée `test_[nom].py` qui hérite de la classe `unittest.TestCase`.

> :information_source: **_INFO_**
>
> En général, les tests unitaires sont enregistrés dans un dossier contenant tous les tests.

Dans le cas test, le mot clef `assert` lèvera une exception `AssertionError` si l'expression évaluée ne retourne pas `True`. En plus du mot clef `assert`, le module `unittest` fournit un ensemble de fonctions chargées de valider les tests. La tableau suivants liste les plus courantes:

| Méthode                                           | Vérification                                        |
| ------------------------------------------------- | --------------------------------------------------- |
| `assertEqual(a, b)`                               | `a == b`                                            |
| `assertNotEqual(a, b)`                            | `a != b`                                            |
| `assertTrue(x)`                                   | `x is True`                                         |
| `assertFalse(x)`                                  | `x is False`                                        |
| `assertIs(a, b)`                                  | `a is b`                                            |
| `assertIsNot(a, b)`                               | `a is not b`                                        |
| `assertIsNone(x)`                                 | `x is None`                                         |
| `assertIsNotNone(x)`                              | `x is not None`                                     |
| `assertIn(a, b)`                                  | `a in b`                                            |
| `assertNotIn(a, b)`                               | `a not in b`                                        |
| `assertIsInstance(a, b)`                          | `isinstance(a, b)`                                  |
| `assertNotIsInstance(a, b)`                       | `not isinstance(a, b)`                              |
| `assertRaises(exception, fonction, args, kwargs)` | `Vérifie que la fonction lève l’exception attendue` |

Exemple:

```python
# Methode a tester (normalement dans un autre fichier)
def is_even(nbr):
  """Retourne True sur le nombre est pair, False si impair
  """
  return True if nbr % 2 == 0 else False

# Classe dediee aux tests unitaires
import unittest

class Test_IsEven(unittest.TestCase):
  def test_is_even(self):
    self.assertTrue(is_even(2))
    self.assertFalse(is_even(1))
    self.assertEqual(is_even(0), True)

if __name__ == '__main__':
  unittest.main()

=> .
   ----------------------------------------------------------------------
   Ran 1 test in 0.000s

   OK
```

#### Exécution

La méthode la plus simple pour lancer un test unitaire est de l'exécuter comme tout autre programme Python.

```bash
$ python test_iseven.py

.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
```

Il est également possible de demander à Python de parcourir tous les dossiers (depuis le répertoire courant) à la recherche de tests unitaires afin d'exécuter l'ensemble des tests.

```bash
$ python -m unittest discover [dossier_de_test]
ou
$ python -m unittest discover -s [dossier_de_test] -p 'test_*.py' 
```

### doctest

`doctest` permet d'inclure des tests unitaires sans créer de classes dédiées. Grâce à `doctest`, il est possible d'inclure les tests unitaires directement dans la documentation des fonctions.

> :information_source: **_INFO_**
>
> `doctest` affiche uniquement les erreurs rencontrées, c'est pourquoi dans l'exemple ci-dessous une erreur a été volontairement introduite.

```python
def mult(a, b):
  """Return a*b

  >>> mult(2, 3)
  6
  >>> mult(2, 5)
  10
  """
  # introduction volontaire d'un bug
  if b == 5:
    b += 1

  return a*b

if __name__ == '__main__':
  import doctest
  doctest.testmod()

=> **********************************************************************
   File "/opt/rapazp/teaching/python/math.py", line 19, in __main__.mult
   Failed example:
       mult(2, 5)
   Expected:
       10
   Got:
       12
   **********************************************************************
   1 items had failures:
     1 of   2 in __main__.mult
   ***Test Failed*** 1 failures.
```

## Divers

### Arguments d'exécution

Il exsite plusieurs modules pour récupérer les arguments passés en paramètres d'exécution d'une application Python: `sys`, `getopt`, `argparse`, `docopts`, etc.

Ici, seule le module `argparse` sera abordé car il offre beaucoup de flexibilité mais requiert une certaine structure.

```python
import argparse

description = "Le champ 'description' contient le texte a afficher " \
              "avant l aide detaillee des arguments"

parser = argparse.ArgumentParser(description=description)

# Ajout d'arguments
parser.add_argument("-a",
                    "--annee", 
                    help="défini l'année")

parser.add_argument("-V",
                    "--version", 
                    help="affiche la version du programme", 
                    action="store_true")

args = parser.parse_args()

# Verification des arguments
if args.annee:
  print(f"L'année est {args.annee}")
if args.version:
  print("Application version 1.0")
```

```bash
$ python exemple_arguments.py -h
usage: exemple_arguments.py [-h] [-a ANNEE] [-V]

Le champ 'description' contient le texte a afficher avant l aide detaillee des arguments

optional arguments:
  -h, --help            show this help message and exit
  -a ANNEE, --annee ANNEE
                        défini l année
  -V, --version         affiche la version du programme
```

```bash
$ python exemple_arguments.py -V
Application version 1.0
```

```bash
$ python exemple_arguments.py -a 2020 -V
l annee est 2020
Application version 1.0
```

# Sources

- [Python 3.10 documentation](https://docs.python.org/3)
- [Python Cheatsheet](https://www.pythoncheatsheet.org/)
- [Getting Started with Python in VS Code](https://code.visualstudio.com/docs/python/python-tutorial)
- [W3schools - Python Tutorial](https://www.w3schools.com/python/)
- [Python Tutorial](https://www.tutorialspoint.com/python/index.htm)
- [Cours Python 3 pour la programmation scientifique](https://courspython.com/)
- [Cours de Python](https://python.sdv.univ-paris-diderot.fr/)
- [Apprendre le langage de programmation python](https://python.doctor/)
- [Style Guide for Python Code](https://www.python.org/dev/peps/pep-0008/#function-and-variable-names)
- [Conventions de syntaxe en python](http://all4dev.libre-entreprise.org/index.php/Conventions_de_syntaxe_en_python)
- [Notes sur les modules et packages en Python](https://zestedesavoir.com/billets/1842/notes-sur-les-modules-et-packages-en-python/)
- [Python 3's f-Strings: An Improved String Formatting Syntax (Guide)](https://realpython.com/python-f-strings/)
- [Proper way to declare custom exceptions in modern Python?](https://stackoverflow.com/a/53469898/2260619)
- [Le test uniatire en Python - Les bases](https://ichi.pro/fr/tests-unitaires-en-python-les-bases-37012509038752)

<br /><p style='text-align: right;'><sub>&copy; 2019-2022 - dServ Solutions SàRL - [Pascal Rapaz](mailto:pascal.rapaz@dserv.ch)</sub></p>
