<!--
Copyright (C) 2010-2023 by [Pascal Rapaz](mailto:pascal.rapaz@gmail.com)

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
Free Documentation License".

A copy of the license is agreement is available at [GNU Free Documentation License](https://www.gnu.org/licenses/fdl-1.3.txt)".
-->

# Cheatsheet Google Search <!-- omit in toc -->

## Table des matières <!-- omit in toc -->

- [Opérateurs](#opérateurs)
  - [Opérateurs de base](#opérateurs-de-base)
- [Tips](#tips)
  - [Filtre par date](#filtre-par-date)
  - [Recherche avancée](#recherche-avancée)
- [Autres services](#autres-services)
- [Sources](#sources)

## Opérateurs

### Opérateurs de base

<table>
<tr>
  <th>Opérateur</th>
  <th>Description</th>
</tr>
<tr>
  <td>

  `""`
  </td>
  <td>
  Effectuer une recherche en spécifiant une phrase exacte que vous souhaitez trouver dans les résultats. Cela permet d'exclure les résultats qui ne correspondent pas exactement à la phrase recherchée.

  Vous pouvez spécifier un mot précis dans votre recherche et demander à Google de ne pas inclure de synonymes dans les résultats. Par exemple, si vous recherchez le mot `"ordinateur"`, Google n'affichera que des résultats contenant le mot `"ordinateur"` en excluant les termes similaires comme `"PC"` ou `"machine"`.
   </td>
</tr>
<tr>
  <td>

  `-`
  </td>
  <td>

  Le caractère moins `-` permet d'exclure certains termes ou mots-clés des résultats de recherche. Il est utile lorsque vous souhaitez affiner vos résultats en éliminant des éléments spécifiques qui ne sont pas pertinents pour votre recherche.

  ```md
  # Recherche des recettes de desserts qui ne contiennent pas de chocolat
  Recettes de desserts -chocolat
  ```

  </td>
</tr>
<tr>
  <td>

  `OR/AND`
  </td>
  <td>

  Fonction de recherche booléenne (doivent être en majuscules)
  
  ```md
  # Recherche des voyages en Europe, en Asie ou les deux
  Voyages Europe OR Asie

  # Recherche des smartphone Samsung liés à la technologie 5G
  Smartphones Samsung AND 5G
  ```

  </td>
</tr>
<tr>
  <td>

  `|`
  </td>
  <td>

  Identique à l'opérateur booléen `OR`
  
  ```md
  # Recherche des voyages en Europe, en Asie ou les deux
  Voyages Europe | Asie
  ```

  </td>
</tr>
<tr>
  <td>

  `()`
  </td>
  <td>

  Permet de regrouper des termes de recherche et définir l'ordre d'évaluation des opérations.
  
  ```md
  # Recherche des iPhone 2023 ou 2022 sans le mondèle SE
  (iPhone 2023 OR iPhone 2022) -"iPhone SE"
  ```

  </td>
</tr>
<tr>
  <td>

  `*`
  </td>
  <td>

  Caractère générique qui perment de nremplacer un mot ou une partie d'un mot inconnu.

  ```md
  # Rechercher les paroles d'une chanson de Taylor Swift
  "Paroles de * par Taylor Swift"
  ```

  </td>
</tr>
</table>

## Tips

Utilisez des mots-clés précis et pertinents pour votre recherche (de préférence en anglais). Plus vos mots-clés sont précis, plus vous aurez de chances de trouver des résultats pertinents.

```md
# Désactiver l'interface graphique au démarrage avec la version 22.04 d'ubuntu
"ubuntu 22.04" disable gui on startup
```

### Filtre par date

Vous avez la possibilité de restreindre la recherche pour trouver des résultats plus pertinents en fonction de la période de temps souhaitée.

![Google filter search](assets/images/google_filter_search.png "Google filter search")

### Recherche avancée

Vous pouvez également utiliser la page [Google Advanced Search](https://www.google.com/advanced_search) pour effectuer des recherches spécifiques. Néanmoins vous n'aurez pas accès à toutes les fonctionnalités offertes par Google Search.

  ![Google advanced search](assets/images/google_advanced_search.png "Google advanced search")

## Autres services

- [scholar.google.com](https://scholar.google.com) est un service qui permet de rechercher des articles universitaires, des thèses, des livres, des rapports techniques et d'autres publications scientifiques. Contrairement à la recherche classique, Scholar se concentre principalement sur les travaux universitaires et académiques.
- [bard.google.com](https://bard.google.com) est un robot conversationnel utilisant le `deep learning` pour générer du texte. `Bard` fonctionne de manière similaire à `ChatGPT` à la différence qu'il s'appuie sur des données mises à jour en temps réel, car il est directement connecté au web.

## Sources

- [Google Search Operators Cheat Sheet](https://static.semrush.com/blog/uploads/files/39/12/39121580a18160d3587274faed6323e2.pdf)
- [Google Search cheatsheet](https://quickref.me/google-search.html)
