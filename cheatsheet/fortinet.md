<!--
Copyright (C) 2021 - EPTM (École professionnelle technique et des métiers) - [Pascal Rapaz](mailto:pascal.rapaz@eptm.ch)

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.

A copy of the license is agreement is available at [GNU Free Documentation License](https://www.gnu.org/licenses/fdl-1.3.txt)".
-->

# Cheatsheet Fortinet <!-- omit in toc -->

## Table des matières <!-- omit in toc -->

- [Ligne de commande](#ligne-de-commande)
  - [Configuration des interfaces](#configuration-des-interfaces)
    - [Afficher la configuration des interfaces](#afficher-la-configuration-des-interfaces)
    - [Afficher la table NAT](#afficher-la-table-nat)
  - [Status du système](#status-du-système)
  - [Reset factory](#reset-factory)

## Ligne de commande

### Configuration des interfaces

```less
config system interface
  edit port1
    set mode static
    set ip <management_address>/<cidr>
    set allowaccess ping https ssh fgfm
end
```

#### Afficher la configuration des interfaces

```less
config system interface
  edit ?
```

> :information_source: **_INFO_**
>
> Le `?` sur un clavier `us` s'obtient avec la combinaison de touches `Shift + -`

#### Afficher la table NAT

```less
get system session list
```

### Status du système

```less
# Display system status
get system status

# Display licence details
diag autoupdate versions
```

### Reset factory

```less
execute factoryreset
```

<br /><p style='text-align: right;'><sub>&copy; 2021 - EPTM (École professionnelle technique et des métiers) - [Pascal Rapaz](mailto:pascal.rapaz@eptm.ch)</sub></p>
