<!--
Copyright (C) 2015-2022 by [Pascal Rapaz](mailto:pascal.rapaz@gmail.com)

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
Free Documentation License".

A copy of the license is agreement is available at [GNU Free Documentation License](https://www.gnu.org/licenses/fdl-1.3.txt)".
-->

TODO
 [ ] https://stackoverflow.com/questions/7340893/what-is-the-difference-between-map-every-and-foreach

# Cheatsheet JavaScript <!-- omit in toc -->

## Table des matières <!-- omit in toc -->

- [Mode `strict`](#mode-strict)
- [Types primitifs](#types-primitifs)
  - [Structure de données](#structure-de-données)
- [Déclaration des variables](#déclaration-des-variables)
- [Opérateurs](#opérateurs)
  - [Opérateurs de comparaison](#opérateurs-de-comparaison)
  - [Opérateurs logiques](#opérateurs-logiques)
  - [Opérateur `Null Coalescing` (`??`)](#opérateur-null-coalescing-)
  - [Opérateur `optional chaining` (`?.`)](#opérateur-optional-chaining-)
- [Conditions et boucles](#conditions-et-boucles)
  - [Les closures](#les-closures)
  - [Variables statiques](#variables-statiques)
- [Fonctions](#fonctions)
  - [Prédéfinies](#prédéfinies)
  - [Utilisateurs](#utilisateurs)
- [Objets](#objets)
  - [Héritage](#héritage)
- [Namespace](#namespace)
- [Regex](#regex)
- [ES5 (@Deprecated)](#es5-deprecated)
  - [Objets](#objets-1)
    - [Constructeur](#constructeur)
    - [Méthodes de classes](#méthodes-de-classes)
      - [Variante 1 : dans le constructeur](#variante-1-dans-le-constructeur)
      - [Variante 2 : via un prototype](#variante-2-via-un-prototype)
  - [Héritage](#héritage-1)
- [Liens et ressources](#liens-et-ressources)
  - [Sites](#sites)
  - [Formations](#formations)

## Mode `strict`

L'utilisation du mode strict élimine d'une part les erreurs silencieuses en les changeant en erreurs explicites, d'autre part, corrige certaines syntaxes rendant difficile l'optimisation du code par le moteur JavaScript.

L'activation du mode strict s'effectue soit de manière globale soit de manière locale dans une fonction :

```js
'use strict';
```

## Types primitifs

Les types disponibles en `JavaScript` sont les suivants:

| Type        | Description                                                                                         |
|-------------|-----------------------------------------------------------------------------------------------------|
| `Number`    | Les nombres sont en double précision 64-bit. Il n'y a pas d'entier en `JavaScript` (sauf `BigInt`). |
| `String`    | Chaîne de caractères `UTF-16`                                                                       |
| `Boolean`   | Valeurs booléennes : `true` / `false`                                                               |
| `Symbol`    | Représente une donnée unique et inchangeable                                                        |
| `Object`    | Représente un objet                                                                                 |
| `null`      | Indique q'un objet est inexistant                                                                   |
| `undefined` | Indique que la variable est sans valeur                                                             |

### Structure de données

<table>
<tr>
  <th>Type</th>
  <th>Syntaxe</th>
</tr>
<tr>
  <td>String</td>
  <td>

  ``` js
  var s = 'John Doe',
      t = "John Doe",
      u = 'John-Doe';

  // creation d'une tableau a partir d'une string
  var tab = s.split(' ');
  var tab1 = u.split('-');

  s.substring(0, 4);
  s.slice(0, -4);

  s.trim();

  s.indexOf('Doe');
  s.lastIndexOf('Doe');

  s.toUpperCase();
  s.toLowerCase();

  s.chartAt(5);

  s.chartCodeAt(5);
  String.fromCharCode(74, 97, 118, 97); // --> Java
  ```

  </td>
</tr>
<tr>
  <td>Tableaux</td>
  <td>

  ``` js
  var a = [];
  var b = [12, 'John', 46, 'Doe'];
  // @Deprecated
  //   On utilise plus l'objet 'Array()' pour créer des tableaux
  var c = new Array(12, 'John', 46, 'Doe'); 

  // ajouter un element
  a.push('John', 'Doe'); // ajoute en fin
  a.unshift(21);         // ajoute au debut

  // supprimer un element
  a.pop();   // supprime le dernier
  a.shift(); // supprime le premier

  // extraire des elements d'un tableau
  b.slice(2, 4);

  // retirer des elements d'un tableau
  b.splice(2, 4);

  // iteration avec forEach
  b.forEach(function(value, index, array) {
  });

  // creation d'une string a partir d'un tableau
  var str = b.join('-');

  // concatene 2 tableaux
  var d = b.concat(c);

  // tri
  b.sort();
  b.reverse();

  // custom tri
  b.sort(function(a,b) {
    if (a < b) {
      return -1;
    } else if (a > b) {
      return 1;
    } else {
      return 0;
    }
  });
  ```

  </td>
</tr>
<tr>
  <td>Objets</td>
  <td>

  ```js
  var a = {}; 
  var person = { firstName: "John", 
                 lastName: "Doe", 
                 age: 50
               };

  // recuperer un element
  person.lastName;
  person['lastName'];

  // ajout d'une propriete
  person.eyeColor = "blue";
  person['eyeColor'] = "blue";

  // objet comme retour d'une fonction
  function getCoords() {
    return { x: 15, y = 20, z: 10};
  }
  ```

  </td>
</tr>
</table>

## Déclaration des variables

En javascript, les mots clefs `var`, `let` et `const` permettent de déclarer une variable. Le choix du mot clef influencera l'accessibilité et la portée de la variable.

> :information_source: **_INFO_**
>
> Bien que le mot clef `static` soit réservé, il n'est pas implémenté en JavaScript (c.f. Variables statiques)

Avec `let`, la portée est limitée au bloc dans lequel il est déclaré :

```js
function myFunction() {
  let month = 4;

  if (month == 4) {
    let day = 14;
    console.log(day); // Affiche 14
  }

  console.log(month); // Affiche 4
  console.log(day);   // Affiche ‘Invalid JavaScript' car elle n'existe
                      // plus en dehors du du bloc if
}
```

Le même code avec l'utilisation de `var`, donnera accès à la variable `day` en dehors de la boucle :

```js
function myFunction() {
  var month = 4;

  if (month == 4) {
    var day = 14;
    console.log(day); // Affiche 14
  }

  console.log(month); // Affiche 4
  console.log(day);   // Affiche 14
}
```

Enfin, `const` permet de créer des variables en lecture seule (immuable) :

```js
function myFunction() {
  const NUMBER = 4;
  console.log(NUMBER); // Affiche 4

  NUMBER = 5; // TypeError: Assignment to constant variable
}
```

> :bulb: **_TIP_**
>
> Essayez d'utiliser le plus possible `let` pour éviter que vos variables aient une trop grande portée

## Opérateurs

| Opérateur    | Signification                                                                                                                                                                     |
|--------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `typeof`     | donne le type d'un variable. Ne devrait être utilisé que pour vérifier si une variable est `undefined` ou en corrélation avec l'utilisation de la méthode `valueOf()` d'un objet. |
| `instanceof` | vérifie le type d'objet à l'exécution                                                                                                                                             |
| `undefined`  | une variable sans valeur est `undefined`<br />(ne pas utiliser `null` qui désigne un objet "inexistant")                                                                          |

### Opérateurs de comparaison

| Opérateur | Signification                |
|-----------|------------------------------|
| `==`      | égal à                       |
| `!=`      | différent de                 |
| `===`     | contenu et type égal à       |
| `!==`     | contenu ou type différent de |
| `>`       | supérieur à                  |
| `>=`      | supérieur ou égal à          |
| `<`       | inférieur à                  |
| `<=`      | inférieur ou égal à          |

Différence entre `==` et `===` :

```js
var number = 4, text = '4', result;

result = number == text;
console.log(result); // Affiche true alors que number est un
                     // nombre et text une chaine de caracteres

result = number === text;
console.log(result); // Affiche false car cet operateur compare aussi 
                     // les types des variables en plus de leurs valeurs

if (allowCancel === undefined) { 
}
```

### Opérateurs logiques

| Opérateur | Type | Utilisation            |
|-----------|------|------------------------|
| `&&`      | Et   | `valeur1 && valeur2`   |
| `\|\|`    | Ou   | `valeur1 \|\| valeur2` |
| `!`       | Non  | `!valeur`              |

### Opérateur `Null Coalescing` (`??`)

Si le résultat d'une expression est `undefined` ou `null`, l'opérande située après `??` sera retournée.

``` js
let a;
let b = 'John';

console.log(a ?? 'Default');
console.log(b ?? 'Default');

> Default
> John
```

### Opérateur `optional chaining` (`?.`)

L'`optional chaining` est un moyen sûr d'accéder aux propriétés imbriquées des objets sans avoir à vérifier la validité de chaque référence. Si une référence est `undefined` ou `null`, le retour de l'expression sura `undefined`

```js
const adventurer = {
  name: 'John',
  bike: {
    model: 'Triumph',
  }
};

var advent;

console.log(adventurer.bike?.model);
console.log(adventurer.car?.model);
console.log(advent?.car?.model);

> Triumph
> undefined
> undefined
```

## Conditions et boucles

<table>
<tr>
  <th>Type</th>
  <th>Syntaxe</th>
</tr>
<tr>
  <td>ternaire</td>
  <td>

  ```js
  variable = (condition) ? true : false ;
  ```

  </td>
</tr>
<tr>
  <td>
  
  `if...else`</td>
  <td>

  ```js
  if (condition) {
  } else if (condition) {
  } else {
  }
  ```

</td>
</tr>
<tr>
  <td>
  
  `switch`</td>
  <td>

  ```js
  switch(condition) {
    case 1:
      break;
    case 2:
    default:
  }
  ```

  </td>
</tr>
<tr>
  <td>
  
  `while`</td>
  <td>

  ```js
  while (condition) {
  }
  ```

</td>
</tr>
<tr>
  <td>
  
  `do...while`</td>
  <td>

  ```js
  do {
  } while (condition);
  ```

</td>
</tr>
<tr>
  <td>
  
  `for`</td>
  <td>

  ```js
  for (var stmt; condition; increment) {
  }
  
  // Evitez cette notation car a.length est évalué à chaque tour
  for (var i = 0; i < a.length; i++) {
  }

  // Utilisez de preference l'une des notations suivante
  for (var i = 0, len = a.length; i < len; i++) {
  }

  var len = a.length;
  for (var i=0; i<len; i++) {
  }
  ```

  </td>
</tr>
<tr>
  <td>
  
  `for...in`</td>
  <td>

  Permet d'itérer sur les propriétés énumérables d'un objet
  
  ```js
  const object = { a: 1, b: 2, c: 3 };

  for (const o in object) {
    console.log(`${o}: ${object[o]}`);
  }

  > "a: 1"
  > "b: 2"
  > "c: 3"
  ```

  </td>
</tr>
<tr>
  <td>
  
  `for...of`</td>
  <td>

  Parcours les éléments d'un objet itérable de type : `Array`, `Map`, `Set`, `String`, `TypedArray`, etc.)
  
  ```js
  const array = ['a', 'b', 'c'];

  for (var e of array) {
    console.log(`${e}`);
  }

  > "a"
  > "b"
  > "c"
  ```

  </td>
</tr>
</table>

### Les closures

_« Une closure est l'inclusion dans la définition d'une fonction d'une variable d'un scope supérieur »_<br />([Qu'est-ce qu'une closure en Python et JavaScript ?](http://sametmax.com/closure-en-python-et-javascript/))

```js
function add(initial) {
  return function(value) {
    return initial + value;
  }
}

var initial10 = add(10);
var initial20 = add(20);

console.log(initial10(6));
console.log(initial20(8));
console.log(add(5)(7));

> 16
> 28
> 12
```

### Variables statiques

Il n'y a pas de type `static` en JavaScript bien que le mot clef soit réservé. Pour déclarer une variable statique nous utilisons les closures

```js
var count = (function() {
    let cpt = 0; // Declaration de la variable pseudo-statique
    return function() { return cpt++; };
})();

console.log(count());
console.log(count());
console.log(count());

> 0
> 1
> 2
```

## Fonctions

### Prédéfinies

| Nom            | Description                                              |
|----------------|----------------------------------------------------------|
| `isNaN()`      | Retourne true sur le paramètre n'est pas un nombre       |
| `parseFloat()` | Converti en nombre avec décimales                        |
| `parseInt()`   | Converti en nombre entier                                |
| `Number()`     | Converti en objet nombre                                 |
| `String()`     | Transforme le contenu de l'objet en chaîne de caractères |

### Utilisateurs

Les paramètres d'une fonction sont indicatifs.

En effet, vous pouvez appeler une fonction sans lui donner les paramètres attendus, auquel cas ils vaudront `undefined`, tout comme vous pouvez donner plus de paramètres que demandés.

Les fonctions ont accès à des variables supplémentaires à l'intérieur de leur corps, appelée `arguments`. Cette variable fournit les paramètres de la fonctions dans un tableau.

Pour des questions de lisibilité, il est conseillé d'utiliser la notation `rest parameter` (`...`) lorsque l'on ne connaît pas le nombre de paramètres.

```js
// Declaration
function myFunction(args) {
}

function myFunction(firstArg, ...args) {
  // Dans ce cas, 'firstArg' ne fait pas partie du contenu de `args`
}

// Fonctions anonymes
var a = function (args) {
};
a();

// Fonctions anonymes nommées (pratique pour les appels récursifs)
var a = function a(args) {
};
a();

// Isolation de code
(function() {
    // Code isolé
    // les parentheses finales permettent une execution immediate du code
})();
```

En `JavaScript`, il est possible de créer des fonctions internes ce qui permet d'alléger le code et évite la création d'objet si cela n'est pas nécessaire.

```js
function person(firstName, lastName) {
  return {
    firstName: firstName,
    lastName: lastName,
    fullName: function() {
      return this.firstName + ' ' + this.lastName;
    }
  }
}

var p = person("John", "Doe");
p.fullName();
```

## Objets

```js
/**
 * Person class
 *
 * @constructor
 * @param {String} firstName - firstName of a person
 * @param {String} lastName - lastName of a person
 * @param {Number} age - age of a person
 */
class Person {
  constructor(firstName, lastName, age) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.age = age;
  }

  getFirstName() {
    return this.firstName;
  }
  
  getLastName() {
    return this.lastName;
  }

  getAge() {
    return this.age;
  }
}

var p = new Person("John", "Doe", 37);
```

### Héritage

```js
class Student extends Person {
  constructor(firstName, lastName, age, profession) {
    super(firstName, lastName, age);
    this.profession = profession;
  }

  getProfession() {
    return this.profession;
  }
}

var s = new Student("John", "Doe", 17, "EI");
```

## Namespace

Le `JavaScript` ne possède pas nativement les namespace néanmoins il est possible de structurer son code de sorte à simuler des catégories

```js
if (typeof myNamespace === 'undefined') {
  var myNamespace = {
    version: 1.4,
    lang: 'fr',

    init : function() {},

    users: {
      list: function(){},
      add: function() {},
      delete: function(),
    },

    mails: {
      list: function(){},
      add: function() {},
      delete: function(),
    }
  };
} else {
  console.log('myNamespace already exist !');
}

myNamespace.init();
```

## Regex

```js
var regex = /content/;
var regexOjbect = new RegExp("content", "i");

var myVar = "of string"
var regexOjbect = new RegExp("content " + myVar, "i");

var regexWithEscape = /content with an escape \/ char/;
var regexIgnoreCase = /content/i;

var regexOrOperator = /content|value/;

var regexStartWith = /^content/;
var regexEndWith = /content$/;

var regexCharSet = /b[iu]g/;
var regexCharSetInterval = /b[a-zA-z]g/;
var regexCharSetExclude = /b[^a-f]g/;

var regexAnyChar = /b.g/;

var regexZeroOrOne = /add?ress/; // 'd' doit exister 0 ou 1 fois
var regexOneOrMore = /ad+res+/;  // 'd' et 's' doit exister 1 ou plusieurs fois
var regexZeroOrMore = /ad*res*/; // 'd' et 's' doit exister 0 ou plusieurs fois

var regexRepeatN = /ad{1}res{1}/;        // 'd' et 's' sont repetes 1 fois
var regexRepeatNtoM = /ad{1,2}res{1,2}/; // 'd' et 's' sont repetes 1 ou 2 fois
var regexRepeatNtoInf = /ad{1,}res{1,}/; // 'd' et 's' sont repetes 1 fois ou a l'infini
var regexWithCapture = /(content)/;
var regexWithoutCapture = /(?:content)/;

// recherche une string
/Content/i.test("my string content the expression"); // --> true

// recherche la premiere occurrence de la string ou retourne null
/b.g/.exec("this is not a bug");

// verifie la validite d'une adresse email
/^[a-z0-9._-]+@[a-z0-9._-]+\.[a-z]{2,6}$/.test("John.Doe@gmail.com".toLowerCase());

// verifie la validite d'une adresse email et capture les elements trouves
/^([a-z0-9._-]+)@([a-z0-9._-]+)\.([a-z]{2,6})$/.test("John.Doe@gmail.com".toLowerCase());
console.log(RegExp.$1 + " " + RegExp.$2 + " " + RegExp.$3)

// remplace la premiere occurrence
"big big big".replace(/big/, "bug");

// remplace toutes les occurrences
"big big big".replace(/big/g, "bug");

// remplace les occurrences en fonction de la capture
"12/26/2015".replace(/^(\d{2})\/(\d{2})\/(\d{4})$/, "$2/$1/$3");
```

## ES5 (@Deprecated)

### Objets

#### Constructeur

```js
function Person(firstName, lastName) {
  this.firstName = firstName;
  this.lastName = lastName;
}

var p = new Person("John", "Doe");
```

#### Méthodes de classes

##### Variante 1 : dans le constructeur

```js
function Person(firstName, lastName) {
  this.firstName = firstName;
  this.lastName = lastName;

  this.addFriend = function(firstName, lastName) {
    this.friend.push(new Person(firstName, lastName));
  };
}
```

##### Variante 2 : via un prototype

```js
function Person(firstName, lastName) {
  this.firstName = firstName;
  this.lastName = lastName;
}

Person.prototype.addFriend = function(firstName, lastName) {
  this.friend.push(new Person(firstName, lastName));
}
```

> :information_source: **_INFO_**
>
> L'utilisation de prototype à l'avantage d'être indépendant de l'objet. En cas d'adaptation de la méthode toutes les instances des objets bénéficieront des modifications. Cette solution doit être privilégiée.

### Héritage

```js
// -----------------------------  Vehicule  ------------------------------- //
function Vehicle(numberOfWheels, tankSize) {
  this.engineStarted = false;
  this.numberOfWheels = numberOfWheels;
  this.tankSize = tankSize;
}

Vehicle.prototype.start = function() {
    this.engineStarted = true;
};

Vehicle.prototype.stop = function() {
    this.engineStarted = false;
};

// -------------------------------  Car  ---------------------------------- //
function Car(numberOfWheels, tankSize, numberOfDoors) {
  Vehicle.call(this, numberOfWheels, tankSize);

  this.numberOfDoors = numberOfDoors;
}

// l'objet prototype doit etre copie du parent pour pouvoir acceder aux 
// methodes
Car.prototype = Object.create(Vehicle.prototype);

// ------------------------------  Moto  ---------------------------------- //
function Moto(numberOfWheels, tankSize, numberOfPassengers) {
  Vehicle.call(this, numberOfWheels, tankSize);

  this.numberOfPassengers = numberOfPassengers;
}

Moto.prototype = Object.create(Vehicle.prototype);
```

## Liens et ressources

### Sites

- [Google JavaScript Style Guide](https://google.github.io/styleguide/jsguide.html)
- [Mozilla Developer Network](https://developer.mozilla.org/fr/) (fr)
- [Wikibooks - Programmation JavaScript](http://fr.wikibooks.org/wiki/Programmation_JavaScript) (fr)
- [La magie des closures](http://margaine.com/2012/12/09/la-magie-des-closures.html) (fr)
- [Qu'est-ce qu'une closure ?](http://sametmax.com/closure-en-python-et-javascript/) (fr)
- [TP - JavaScript Tutorial](http://www.tutorialspoint.com/javascript/index.htm) (en)
- [W3C - JavaScript Tutorial](http://www.w3schools.com/js/default.asp) (en)
- [Performance Tips for JavaScript in V8](http://www.html5rocks.com/en/tutorials/speed/v8/) (en)
- [Airbnb JavaScript Style Guide](http://snowdream.github.io/javascript-style-guide/javascript-style-guide/fr/index.html) (en)

### Formations

- [Dynamisez vos sites web avec JavaScript](http://openclassrooms.com/courses/dynamisez-vos-sites-web-avec-javascript) (fr)
- [A re-introduction to JavaScript (JS tutorial)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/A_re-introduction_to_JavaScript) (en)

<br /><p style='text-align: right;'><sub>&copy; 2015-2022 by [Pascal Rapaz](mailto:pascal.rapaz@gmail.ch)</sub></p>
