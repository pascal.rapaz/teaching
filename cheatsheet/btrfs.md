<!--
Copyright (C) 2015-2021 by [Pascal Rapaz](mailto:pascal.rapaz@gmail.com)

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
Free Documentation License".

A copy of the license is agreement is available at [GNU Free Documentation License](https://www.gnu.org/licenses/fdl-1.3.txt)".
-->

# Cheatsheet BTRFS <!-- omit in toc -->

## Table des matières <!-- omit in toc -->

- [Snapshot](#snapshot)
  - [Création d'un snapshot avec Ubuntu](#création-dun-snapshot-avec-ubuntu)
  - [Restauration d’un snapshot avec Ubuntu](#restauration-dun-snapshot-avec-ubuntu)
  - [Suppression d'un snapshot avec Ubuntu](#suppression-dun-snapshot-avec-ubuntu)
- [Partitions](#partitions)
  - [Agrandir une partition](#agrandir-une-partition)
  - [Crypter une partition](#crypter-une-partition)
    - [Procédure de création](#procédure-de-création)
    - [Changer le label](#changer-le-label)
- [Liens et ressources](#liens-et-ressources)
  - [Sites](#sites)

## Snapshot

### Création d'un snapshot avec Ubuntu

Pour utiliser les snapshots de `/` et `/home` créés avec une distribution Ubuntu, vous devez monter le système de fichiers `btrfs` dans un dossier de travail, car les snapshots doivent être gérés à partir du sommet de l'arbre btrfs.

``` $ sudo mount /dev/sdX# /mnt ```

Créez ensuite le snapshot avec la commande suivante :

``` $ sudo btrfs subvolume snapshot /mnt/@ /mnt/@_snapshot ```

Ceci va créer un snapshot du sous-volume `@` nommé `@_snapshot` également situé à la racine de l'arbre `btrfs`. Étant donné que le nouveau snapshot est situé à la racine, il ne sera pas affiché dans la liste des fichiers montés.

### Restauration d’un snapshot avec Ubuntu

Pour restaurer un snapshot, il suffit de renommer les dossiers et de redémarrer le PC :

```bash
sudo mount /dev/sdX# /mnt
sudo mv /mnt/@ /mnt/@_oldroot
sudo mv /mnt/@_snapshot /mnt/@
sudo reboot
```

### Suppression d'un snapshot avec Ubuntu

Pour supprimer un snapshot, utilisez la commande `btrfs` suivante :

```bash
sudo mount /dev/sdX# /mnt
sudo btrfs subvolume delete /mnt/@_oldroot
```

> :warning: **_WARNING_**
>
> La commande `btrfs subvolume set-default` casse l'arborescence définie par défaut d'Ubuntu.
>
> En cas d'utilisation accidentelle de cette commande, vous pouvez rétablir les valeurs par défaut à l'aide des commandes de l'article [The btrfs-tools command "set-default" will break Ubuntu’s layout](https://help.ubuntu.com/community/btrfs#The_btrfs-tools_command_.27.27set-default.27.27_will_break_Ubuntu.27s_layout)

## Partitions

### Agrandir une partition

*(source : [How to resize/extend a btrfs formatted root partition](https://www.suse.com/de-de/support/kb/doc/?id=7018329))*

1. Ajoutez le nouveau disque
2. Rescannez le bus SCSI

   ```bash
   sudo echo 1 > /sys/block/sda/device/rescan
   ```

3. Agrandissez la partition

   ```bash
   sudo cfdisk /dev/sdX
   ```

4. Agrandissez le système de fichier

   ```bash
   sudo btrfs filesystem resize max /
   ```

### Crypter une partition

Il existe plusieurs moyens pour chiffrer une partition btrfs : [Does btrfs support encryption ?](https://btrfs.wiki.kernel.org/index.php/FAQ#Does_btrfs_support_encryption.3F)

Étant donné que j’utilise l’encryption principalement sur des disques externes, j’ai choisi la variante `dm-crypt / LUKS` pour crypter mes données avec une table de partition de type GPT.

> :skull: **_WARNING_**
>
> Ne copiez/collez pas bêtement les commandes qui suivent et faites un backup de vos données avant de débuter la procédure !
>
> Je ne suis en aucun cas responsable d’une quelconque perte de données !

#### Procédure de création

1. Installez les packages nécessaires pour le cryptage

   ```bash
   apt-get install cryptsetup
   ```

2. Créez la partitions GPT à crypter

   ```bash
   gdisk /dev/sdc
   ```

3. Préparez la partition

   ```bash
   cryptsetup --cipher aes-xts-plain64 --hash sha512    --use-random --verify-passphrase luksFormat /dev/sdc1
   ```

4. Ouvrez la partition

   ```bash
   cryptsetup luksOpen /dev/sdc1 <name>
   ```

5. Formatez la partition en btrfs

   ```bash
   mkfs.btrfs /dev/mapper/<name>
   mount -o noatime,discard,defaults /dev/mapper/<name> /mnt
   ```

#### Changer le label

Pour changer le label du disque btrfs, vous devez d’abord le monter puis utiliser l’option label de la commande btrfs :

```bash
btrfs fi label /media/<user>/<uuid> <label>
```


```bash
sudo -i
mount /dev/nmve0n1p2 /mnt -o subvol=@
cd /
for i in proc sys dev run; do mount -o bind $i /folder/$i; done
cd /mnt
chroot .
```

## Liens et ressources

### Sites

- [btrfs - wiki](https://btrfs.wiki.kernel.org/index.php/Main_Page)
- [FAQ - btrfs wiki](https://btrfs.wiki.kernel.org/index.php/FAQ)
- [How-To: Install Arch Linux on an encrypted Btrfs partition](http://www.brunoparmentier.be/blog/how-to-install-arch-linux-on-an-encrypted-btrfs-partition.html)
- [Full disk encryption with Btrfs and multiple drives in Ubuntu](http://nyeggen.com/post/2014-04-05-full-disk-encryption-with-btrfs-and-multiple-drives-in-ubuntu/)
- [How to create snapshots and restore your Linux system using btrfs](http://linuxpitstop.com/snapshots-and-restore-linux-system-using-btrfs/)
- [How to make a btrfs snapshot ?](https://askubuntu.com/questions/124075/how-to-make-a-btrfs-snapshot)
- [Btrfs hands on: My first experiments with a new Linux file system](http://www.zdnet.com/article/btrfs-hands-on-my-first-experiments-with-a-new-linux-file-system/)
- [How to resize/extend a btrfs formatted root partition](https://www.suse.com/de-de/support/kb/doc/?id=7018329)

<br /><p style='text-align: right;'><sub>&copy; 2015-2021 by [Pascal Rapaz](mailto:pascal.rapaz@gmail.ch)</sub></p>
