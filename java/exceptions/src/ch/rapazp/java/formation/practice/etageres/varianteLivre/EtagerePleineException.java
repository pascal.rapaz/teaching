package ch.rapazp.java.formation.practice.etageres.varianteLivre;

/* -----------------------------------------------------------------------------
 * (c) 2002, 2003 by Rodrigue Vaudan
 * -----------------------------------------------------------------------------
 *
 * EtagerePleineException.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 20.02.2003     1.0       Creation
 * -----------------------------------------------------------------------------
 */

/**
 * Exercice 3
 * 
 * Erreur survenant lorsqu'une etagere est pleine.
 * 
 * @author Rodrigue Vaudan
 * @version 1.0
 */
public class EtagerePleineException extends Exception {

  public EtagerePleineException(String str) {
    super(str);
  } // endConst
} // /:~
