package ch.rapazp.java.formation.practice.addition;

/* -----------------------------------------------------------------------------
 * (c) 2002, 2003 by Rodrigue Vaudan
 * -----------------------------------------------------------------------------
 *
 * Calcul.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 20.02.2003     1.0       Creation
 * -----------------------------------------------------------------------------
 */

/**
 * Exercice 2
 * 
 * Calcul la somme des arguments de la ligne de commande independamment du
 * contenu des parametre de lancement.
 * 
 * @author Rodrigue Vaudan
 * @version 1.0
 */
public class Calcul {

  public static void main(String[] args) {
    int resultat = 0;

    for (int i = 0; i < args.length; i++) {
      try {
        resultat += Integer.parseInt(args[i]);
      } catch (NumberFormatException ex) {
        // do nothing
      } // end try
    } // end for

    System.out.println("resultat : " + resultat);
  } // end main
} // /:~
