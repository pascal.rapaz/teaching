package ch.rapazp.java.formation.practice.etageres.varianteLivre;

/* -----------------------------------------------------------------------------
 * (c) 2002, 2003 by Rodrigue Vaudan
 * -----------------------------------------------------------------------------
 *
 * Livre.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 20.02.2003     1.0       Creation
 * 25.04.2003     1.1       Modifications divers
 * -----------------------------------------------------------------------------
 */

/**
 * Exercice 3
 * 
 * Representation d'un livre
 * 
 * @author Rodrigue Vaudan
 * @author Pascal Rapaz
 * @version 1.1
 */
public class Livre {

  private final String title;
  private final String author;
  private final String edition;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Constructeur
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  public Livre(String titre, String auteur, String edition) {

    this.title = titre;
    this.author = auteur;
    this.edition = edition;
  } // endConst

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Methode
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  public String toString() {

    StringBuilder res = new StringBuilder();

    res.append("Titre : " + getTitre() + "\n");
    res.append("Auteur :" + getAuteur() + "\n");
    res.append("Edition :" + getEdition() + "\n");

    return res.toString();
  } // endFct

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Getters
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /**
   * @return String
   */
  public String getAuteur() {
    return author;
  }

  /**
   * @return String
   */
  public String getEdition() {
    return edition;
  }

  /**
   * @return String
   */
  public String getTitre() {
    return title;
  }
} // /:~
