package ch.rapazp.java.formation.practice.trace;

/* -------------------------------------------------------------------------------------------------
 * (c) 2002, 2003 by Rodrigue Vaudan
 * -------------------------------------------------------------------------------------------------
 *
 * MyFirstException.java
 *
 * -------------------------------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 20.02.2003     1.0       Creation
 * -------------------------------------------------------------------------------------------------
 */
public class MyFirstException extends Exception {

  public MyFirstException(String s) {
    super(s);
  }// endConst
}// /:~