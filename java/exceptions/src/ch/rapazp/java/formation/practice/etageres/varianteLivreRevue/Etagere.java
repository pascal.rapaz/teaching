package ch.rapazp.java.formation.practice.etageres.varianteLivreRevue;

/* -----------------------------------------------------------------------------
 * (c) 2002, 2003 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * Etagere.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 25.04.2003     1.0       Creation
 * -----------------------------------------------------------------------------
 */

/**
 * Exercice 4
 * 
 * Representation d'une etagere sous forme de classe avec utilisation d'objet
 * comme element de celle-ci.
 * 
 * @author Pascal Rapaz
 * @version 1.0
 */
public class Etagere {

  private Object[] etagere = null;
  private int positionLivre = -1;
  private static final int NBRE_LIVRE = 5;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Constructeurs
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  public Etagere() {
    etagere = new Object[NBRE_LIVRE];
  } // endConst

  public Etagere(int taille) {
    etagere = new Object[taille];
  } // endConst

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Methodes
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /**
   * Permet de poser un objet sur l'etagere
   * 
   * @param anObject
   *          L'objet a poser sur l'etagere
   * @throws EtagereException
   *           Exception leve si l'etagere est pleine
   */
  public void poser(Object anObject) throws EtagereException {

    try {
      etagere[++positionLivre] = anObject;
    } catch (IndexOutOfBoundsException ex) {
      throw new EtagereException("L'étagère est pleine !!!");
    } // endTry
  } // endFct

  /**
   * Retourne un element de l'etagere
   * 
   * @param i
   *          L'element a prendre
   * @return Un <code>Object</code>
   * @throws EtagereException
   *           Exception levee si une erreur liee a l'etagere survient
   */
  public Object getElement(int i) throws EtagereException {

    try {
      if (etagere[i] == null) {

        throw new EtagereException("Il n'y a rien a cet emplacement!");
      }// endIf

      return etagere[i];

    } catch (IndexOutOfBoundsException ex) {
      throw new EtagereException("Tu touches le mur la!");
    } // endTry
  } // endFct

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  public String toString() {

    StringBuilder res = new StringBuilder();
    boolean hasMoreBooks = true;
    int i = -1;

    while (hasMoreBooks) {
      try {

        /*
         * Java utilise automatiquement la bonne methode d'affichage de chaque
         * objet contenu dans l'etagere. Grace a l'heritage (plus
         * particulierement au polymorphisme) il sait quelle methode toString il
         * doit utiliser.
         */
        if (etagere[++i] != null) {

          res.append(etagere[i]);
          res.append("*****************\n");
        } // endIf

        // could be ArrayIndexOutOfBoundsException or NullPointerException
      } catch (Exception ex) {
        // nous sommmes a la fin de l'etagere ou plus de livre dans
        // l'etagere
        hasMoreBooks = false;
      } // end try
    } // end while

    return res.toString();
  } // endFct
} // /:~