package ch.rapazp.java.formation.practice.trace;

/* -------------------------------------------------------------------------------------------------
 * (c) 2002, 2003 by Rodrigue Vaudan
 * -------------------------------------------------------------------------------------------------
 *
 * TestMyExceptions.java
 *
 * -------------------------------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 20.02.2003     1.0       Creation
 * -------------------------------------------------------------------------------------------------
 */
public class TestMyExceptions {

  public static void throwMyException(int i) throws Exception {
    switch (i) {
    case 1:
      throw new MyFirstException("MyFirstException de throwMyException");
    case 2:
      throw new MySecondException("MySecondException de throwMyException");
    default:
      throw new Exception("Exception de throwMyException");
    }// endSwitch
  }// endFct

  public static void main(String[] args) {
    for (int i = 1; i <= 3; i++) {
      try {
        throwMyException(i);
      } catch (MyFirstException ex) {
        System.out.println("Catch MyFirstException - " + ex.getMessage());
      } catch (MySecondException ex) {
        System.out.println("Catch MySecondException - " + ex.getMessage());
      } catch (Exception ex) {
        System.out.println("Catch Exception - " + ex.getMessage());
      } finally {
        System.out.println("Finally de main");
      }// endTry

      System.out.println("Next for");
    }// endFor
  }// endMain
}// /:~