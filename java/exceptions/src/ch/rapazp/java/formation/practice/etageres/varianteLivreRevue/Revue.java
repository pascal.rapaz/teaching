package ch.rapazp.java.formation.practice.etageres.varianteLivreRevue;

/* -----------------------------------------------------------------------------
 * (c) 2002, 2003 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * Revue.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 25.04.2003     1.0       Creation
 * -----------------------------------------------------------------------------
 */

/**
 * Exercice 4
 * 
 * Representation d'une revue
 * 
 * @author Pascal Rapaz
 * @version 1.0
 */
public class Revue {

  private final String title;
  private final String theme;
  private final double prix;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Constructeur
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  public Revue(String title, String theme, double prix) {
    this.title = title;
    this.theme = theme;
    this.prix = prix;
  } // endConst

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Methodes
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  public String toString() {

    StringBuilder res = new StringBuilder();

    res.append("Titre : " + getTitle() + "\n");
    res.append("Theme :" + getTheme() + "\n");
    res.append("Prix :" + getPrix() + "\n");

    return res.toString();
  } // endFct

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Getters
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  public double getPrix() {
    return prix;
  }

  public String getTheme() {
    return theme;
  }

  public String getTitle() {
    return title;
  }
} // /:~
