package ch.rapazp.java.formation.practice.etageres.varianteLivre;

/* -----------------------------------------------------------------------------
 * (c) 2002, 2003 by Rodrigue Vaudan
 * -----------------------------------------------------------------------------
 *
 * Etagere.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 20.02.2003     1.0       Creation
 * 25.04.2003     1.1       Modifications divers
 * -----------------------------------------------------------------------------
 */

/**
 * Exercice 3
 * 
 * Representation d'une etagere sous forme de classe.
 * 
 * @author Rodrigue Vaudan
 * @author Pascal Rapaz
 * @version 1.1
 */
public class Etagere {

  private Object[] etagere = null;
  private int posLivre = -1;
  private static final int NBRE_LIVRE = 5;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Constructeurs
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  public Etagere() {
    etagere = new Object[NBRE_LIVRE];
  } // endConst

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Methodes
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /**
   * Permet de poser un livre sur l'etagere
   * 
   * @param livreAPoser
   *          Le livre a poser sur l'etagere
   * @throws EtagerePleineException
   *           Lancee si l'etagere est pleine
   */
  public void poserLivre(Livre livreAPoser) throws EtagerePleineException {

    try {
      etagere[++posLivre] = livreAPoser;
    } catch (IndexOutOfBoundsException ex) {
      throw new EtagerePleineException("L'étagère est pleine !!");
    } // end try
  } // endFct

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  public String toString() {
    String res = "";
    boolean hasMoreBooks = true;
    int i = 0;

    while (hasMoreBooks) {
      try {
        res += ((Livre) etagere[i++]).toString();
        res += "*****************\n";

        // could be ArrayIndexOutOfBoundsException or NullPointerException
      } catch (Exception ex) {
        // nous sommmes a la fin de l'etagere ou plus de livre dans
        // l'etagere
        hasMoreBooks = false;
      } // end try
    } // end while

    return res;
  } // endFct
} // /:~
