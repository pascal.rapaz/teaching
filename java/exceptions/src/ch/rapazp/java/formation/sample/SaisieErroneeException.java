package ch.rapazp.java.formation.sample;

/* -----------------------------------------------------------------------------
 * (c) 2002, 2003 by Rodrigue Vaudan
 * -----------------------------------------------------------------------------
 *
 * SaisieErroneeException.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 20.02.2003     1.0       Creation
 * -----------------------------------------------------------------------------
 */
public class SaisieErroneeException extends Exception {

  public SaisieErroneeException() {
    super();
  }// endConst

  public SaisieErroneeException(String str) {
    super(str);
  }// endConst
}// /:~