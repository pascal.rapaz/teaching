package ch.rapazp.java.formation.sample;

/* -----------------------------------------------------------------------------
 * (c) 2014 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * ModuleFiveDisqueTest.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 27.02.2014     1.0       Creation
 * -----------------------------------------------------------------------------
 */

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import ch.rapazp.java.formation.practice.service.Disque;
import ch.rapazp.java.formation.practice.service.DisqueHelper;
import ch.rapazp.java.formation.practice.service.Style;

@RunWith(value = Parameterized.class)
public class ModuleFiveDisqueTest {

  private Disque disque;

  private String artist;
  private String title;
  private Style style;
  private int nbrTrack;
  private long time;
  private double price;
  private String formatedTime;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Constructeurs
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  public ModuleFiveDisqueTest(String artist, String title, Style style, int nbrTrack, long time,
      double price, String formatedTime) {

    this.disque = new Disque(artist, title, style, nbrTrack, time, price);

    this.artist = artist;
    this.title = title;
    this.style = style;
    this.nbrTrack = nbrTrack;
    this.time = time;
    this.price = price;
    this.formatedTime = formatedTime;
  }// endConst

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Test case
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  @Parameters
  public static Collection<Object[]> data() {
    return Arrays.asList(new Object[][] {

        { "Kadebostany", "Pop Collection", Style.POP, 10, DisqueHelper.calculTime(0, 34, 42),
            25.10, "00:34:42" },
        { "Daft Punk", " Random Access Memories", Style.ELECTRO, 13,
            DisqueHelper.calculTime(0, 74, 24), 29.50, "01:14:24" },
        { "Damon Albarn", "Everyday Robots", Style.ROCK, 14, DisqueHelper.calculTime(0, 28, 54),
            19.85, "00:28:54" },

    });
  }

  @Test
  public void testDisque() {

    // En general on evite de tester les getters et les setters a mon qu'ils ne
    // fassent des traitements particuliers
    assertEquals("Artiste", artist, disque.getArtist());
    assertEquals("Titre", title, disque.getTitle());
    assertEquals("Nombre de morceaux", nbrTrack, disque.getNbrTrack());
    assertEquals("Style", style, disque.getStyle());
    assertEquals("Durée", time, disque.getTime());
    assertEquals("Prix", price, disque.getPrix(), 2);
  }// endFct

  @Test
  public void testFormatedTime() {

    assertEquals("Formated time", formatedTime, disque.getFormatedTime());
  }// endFct
}// /:~
