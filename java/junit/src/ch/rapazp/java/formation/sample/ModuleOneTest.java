package ch.rapazp.java.formation.sample;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import ch.rapazp.java.formation.practice.fibonacci.FibonacciRecursif;
import ch.rapazp.java.formation.practice.pi.Pi;

public class ModuleOneTest {

  private static final double DELTA = 1e-15;

  @Test
  public void testPi() {

    assertEquals("leibnizFormula 10 itérations", 3.0418396189294032, Pi.leibnizFormula(10), DELTA);
    assertEquals("leibnizFormula 100 itérations", 3.1315929035585537, Pi.leibnizFormula(100), DELTA);
    assertEquals("leibnizFormula 1000000 itérations", 3.1415916535897743,
        Pi.leibnizFormula(1000000), DELTA);
    assertEquals("leibnizFormula 100000000 itérations", 3.141592643589326,
        Pi.leibnizFormula(100000000), DELTA);
  }// endFct

  @Test
  public void testFibonacciRecursif() {

    FibonacciRecursif fibonacciRecursif = new FibonacciRecursif();

    assertEquals("Fibonacci 20:", 10946, fibonacciRecursif._fibonacci(20));
    assertEquals("Fibonacci 45:", 1836311903, fibonacciRecursif._fibonacci(45));
  }// endFct

}// /:~
