package ch.rapazp.java.formation.sample;

/* -----------------------------------------------------------------------------
 * (c) 2014 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * ModuleFiveTest.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 27.02.2014     1.0       Creation
 * -----------------------------------------------------------------------------
 */

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import ch.rapazp.java.formation.practice.service.DisqueHelper;

@RunWith(value = Parameterized.class)
public class ModuleFiveTest {

  private String ftime;
  private int hour;
  private int minute;
  private int second;
  private long result;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Constructeurs
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  public ModuleFiveTest(String ftime, int hour, int minutes, int seconds, long result) {

    this.ftime = ftime;
    this.hour = hour;
    this.minute = minutes;
    this.second = seconds;
    this.result = result;
  }// endConst

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Test case
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  @Parameters
  public static Collection<Object[]> data() {
    return Arrays.asList(new Object[][] {

      { "01:00:00", 1, 0, 0, 3600000 }, 
      { "00:35:00", 0, 35, 0, 2100000 },
      { "00:00:15", 0, 0, 15, 15000 }, 
      { "02:07:29", 2, 7, 29, 7649000 }, 
      
    });
  }

  @Test
  public void testCalculTime() {

    assertEquals("calculTime " + ftime, result, DisqueHelper.calculTime(hour, minute, second));
  }// endFct

}// /:~
