package ch.rapazp.java.formation.practice.disque.compactDisc.dvd;

/* -----------------------------------------------------------------------------
 * (c) 2014 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * EnumZone.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 18.03.2014     1.0       Creation
 * -----------------------------------------------------------------------------
 */

/**
 * Exercice 2
 * 
 * Classe d'enumeration des differentes zones possibles pour un DVD
 * 
 * @author Pascal Rapaz
 * @version 1.0
 */
public enum EnumZone {

  US(1, "United State"),
  EUROPE(2, "Europe"),
  ASIE(3, "Asie"),
  AUSTRALIE(4, "Australie"),
  AFRIQUE(5, "Afrique"),
  CHINE(6, "Chine"),
  RESERVE(7, "reserve"),
  SPECIAL(8, "special");
  
  private final int number;
  private final String desc;
  
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// Constructeur
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	EnumZone(int number, String desc) {

		this.number = number;
		this.desc = desc;
	} //endConst

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// Methodes 
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	public String getDescription() {
		
		return desc;
	} //endFct

	public int getNumber() {
		
		return number;
	} //endFct
	
	public String toString() {
	  
	  return this.getDescription() + "(" + this.getNumber() + ")";
	} //endFct
} ///:~
