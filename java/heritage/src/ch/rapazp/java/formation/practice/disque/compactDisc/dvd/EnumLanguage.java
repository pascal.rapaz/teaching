package ch.rapazp.java.formation.practice.disque.compactDisc.dvd;

/* -----------------------------------------------------------------------------
 * (c) 2014 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * EnumLanguage.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 18.03.2014     1.0       Creation
 * -----------------------------------------------------------------------------
 */

/**
 * Exercice 2
 * 
 * Enumeration pour la gestion des langues
 * 
 * @author Pascal Rapaz
 * @version 1.0
 */
public enum EnumLanguage {

  FRANCAIS("FR"), ALLEMAND("DE"), ANGLAIS("EN"), ITALIEN("IT");

  private final String lang;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Constructeur
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  EnumLanguage(String lang) {

    this.lang = lang;
  }// endConst

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Methodes
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /**
   * @return String Represente le style musical
   */
  public String lang() {
    return lang;
  }// endFct

  public String toString() {
    return this.lang();
  }// endFt
}// /:~
