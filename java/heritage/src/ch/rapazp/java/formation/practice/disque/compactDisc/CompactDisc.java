package ch.rapazp.java.formation.practice.disque.compactDisc;

/* -----------------------------------------------------------------------------
 * (c) 2002, 2003 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * CompactDisque.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 13.02.2003     1.0       Creation
 * -----------------------------------------------------------------------------
 */

/**
 * Exercice 2
 * 
 * Representation generale d'un compact disc
 * 
 * @author Pascal Rapaz
 * @version 1.0
 */
public class CompactDisc {

  private String title = null;
  private long totalTime = 0;
  private double prix = 0;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Constructeurs
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  public CompactDisc() {
  }// endConst

  /**
   * Construit un nouvel objet de type disque compact avec les champs specifies
   * 
   * @param title
   *          Le titre de disque
   * @param totalTime
   *          La duree totale du disque
   * @param prix
   *          Le prix du disque
   */
  public CompactDisc(String title, long totalTime, double prix) {

    this.setTitle(title);
    this.setTotalTime(totalTime);
    this.setPrix(prix);
  }// endConst

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Methodes
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /**
   * Retourne la duree du disque formatee comme suit: <code>hh:mm:ss</code>
   * 
   * @return String La duree formatee
   */
  public String getFormatedTime() {

    return DiscHelper.formatTime(getTotalTime());
  } // endFct

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Getters and Setters
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /**
   * Retourne le titre de l'album
   * 
   * @return String Le titre du disque
   */
  public String getTitle() {
    return title;
  } // endFct

  /**
   * Retourne la duree totale du disque en milliseconde
   * 
   * @return long La duree totale du disque en milliseconde
   */
  public long getTotalTime() {
    return totalTime;
  } // endFct

  /**
   * Retourne le prix du disque
   * 
   * @return double Le prix du disque
   */
  public double getPrix() {
    return prix;
  } // endFct

  /**
   * Valorise le titre du disque
   * 
   * @param title
   *          Le titre du disque
   */
  public void setTitle(String title) {
    this.title = title;
  } // endFct

  /**
   * Valorise la duree du disque en milliseconde
   * 
   * @param totalTime
   *          La duree du disque
   */
  public void setTotalTime(long totalTime) {
    this.totalTime = totalTime;
  } // endFct

  /**
   * Valorise le prix du disque
   * 
   * @param prix
   *          Le prix du disque
   */
  public void setPrix(double prix) {

    if (prix < 0) {
      System.err.println("Erreur: le prix est negatif!");
    } else {
      this.prix = prix;
    }// endIf
  } // endFct
} // /:~
