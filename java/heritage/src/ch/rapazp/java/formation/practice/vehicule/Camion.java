package ch.rapazp.java.formation.practice.vehicule;

/* -----------------------------------------------------------------------------
 * (c) 2002, 2003 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * Automobile.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 15.02.2003     1.0       Creation
 * -----------------------------------------------------------------------------
 */

/**
 * Exercice 1
 * 
 * Classe camion heritant de la classe de base Vehicule
 * 
 * @author Pascal Rapaz
 * @version 1.0
 */
public class Camion extends Vehicule {

  private int tonnage = 0;
  private String typeChargement = null;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Constructeurs
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /**
   * Construit un nouveau camion
   * 
   * @param genre
   *          Le genre de vehicule
   * @param immatriculation
   *          Le numero d'immatriculation du vehicule
   * @param proprietaire
   *          Le nom du proprio
   * @param nbrPlaces
   *          Le nombre de places
   */
  public Camion(String genre, String immatriculation, String proprietaire, int tonnage) {

    super(genre, immatriculation, proprietaire);
    this.tonnage = tonnage;
  } // endConst

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Methodes
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /**
   * Affecte le type de chargement
   * 
   * @param typeChargement
   *          Le type de chargement
   */
  public void setTypeChargement(String typeChargement) {
    this.typeChargement = typeChargement;
  } // endFct

  /**
   * Retourne le type de chargement
   * 
   * @return String Le type de chargement
   */
  public String getTypeChargement() {
    return (typeChargement == null) ? "vide" : typeChargement;
  } // endFct

  /**
   * Retourne les donnees du vehicule
   * 
   * @return String Les donnees du vehicule (genre, immatriculation,
   *         proprietaire et tonnage)
   */
  public String toString() {
    return super.toString() + "\nTonnage: " + tonnage;
  } // endFct
} // /:~
