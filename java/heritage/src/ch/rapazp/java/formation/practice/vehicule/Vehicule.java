package ch.rapazp.java.formation.practice.vehicule;

/* -----------------------------------------------------------------------------
 * (c) 2002, 2003 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * Vehicule.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 13.02.2003     1.0       Creation
 * -----------------------------------------------------------------------------
 */

/**
 * Exercice 1
 * 
 * Classe de base pour la creation de vehicules
 * 
 * @author Pascal Rapaz
 * @version 1.0
 */
public class Vehicule {

  private String genre = null;
  private String immatriculation = null;
  private String proprietaire = null;
  private boolean isStarted = false;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Constructeurs
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /**
   * Construit un nouveau vehicule
   * 
   * @param genre
   *          Le genre de vehicule
   * @param immatriculation
   *          Le numero d'immatriculation du vehicule
   * @param proprietaire
   *          Le nom du proprio
   */
  public Vehicule(String genre, String immatriculation, String proprietaire) {

    this.genre = genre;
    this.immatriculation = immatriculation;
    this.proprietaire = proprietaire;
  } // endConst

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Methodes
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /**
   * Mise en marche du vehicule
   */
  public void demarrer() {
    isStarted = true;
  } // endFct

  /**
   * Arret du vehicule
   */
  public void arreter() {
    isStarted = false;
  } // endFct

  /**
   * Retourne l'etat du vehicule
   * 
   * @return boolean <code>true</code> si le vehicule est en marche,
   *         <code>false</code> dans les autres cas
   */
  public boolean isStarted() {
    return isStarted;
  } // endFct

  /**
   * Retourne les donnees du vehicule.
   * 
   * @return String Les donnees du vehicule (genre, immatriculation et
   *         proprietaire)
   */
  public String toString() {

    return "\nGenre: " + genre + "\nImmatriculation: " + immatriculation + "\nProprietaire: "
        + proprietaire;
  } // endFct
} // /:~
