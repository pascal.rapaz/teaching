package ch.rapazp.java.formation.sample.circle;

/* -----------------------------------------------------------------------------
 * (c) 2002, 2003 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * Ellipse.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 14.02.2002     1.0       Creation
 * -----------------------------------------------------------------------------
 */

/**
 * Classe permettant de representer une ellipse
 *
 * @author Pascal Rapaz
 * @version 1.0
 */
public class Ellipse extends Circle {

  private double rayon2 = 0;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Constructeur
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~�
  /**
   * Construit un objet ellipse avec les valeurs par defaut
   */
  public Ellipse() {
  } // endConst

  /**
   * Construit une ellipse selon les valeurs des 2 diametres
   * 
   * @param ptDiametre
   *          Le petit diametre
   * @param gdDiametre
   *          Le grand diametre
   */
  public Ellipse(double ptDiametre, double gdDiametre) {

    super(ptDiametre / 2); // appel du constructeur de la classe mere

    rayon2 = gdDiametre;
  } // endConst

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Methodes
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /**
   * @see Circle#area()
   */
  public double area() {

    return (Math.PI * super.getRadius() * rayon2);
  } // endFct

  /**
   * Calcul approche du perimetre de l'ellipse
   * 
   * @see Circle#circumference()
   */
  public double circumference() {

    double res = 0;
    double ptDiam = super.getRadius() * 2;
    double gdDiam = rayon2 * 2;

    double ptDiamCarre = ptDiam * ptDiam;
    double gdDiamCarre = gdDiam * gdDiam;
    double sinus;
    double cosinus;

    for (int i = 0; i < 1000; i++) {
      sinus = (double) Math.sin(i * Math.PI / 2000);
      cosinus = (double) Math.cos(i * Math.PI / 2000);

      res += Math.sqrt(ptDiamCarre * sinus * sinus + gdDiamCarre * cosinus * cosinus);
    } // endFor

    return res *= 4 * Math.PI / 2000;
  } // endFct

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  public String toString() {

    return "Ellipse: R1 = " + super.getRadius() + " R2 = " + rayon2;
  } // endFct

} // /:~
