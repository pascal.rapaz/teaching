package ch.rapazp.java.formation.sample.circle;

/* -----------------------------------------------------------------------------
 * (c) 2002, 2003 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * PlaneCircle.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 14.02.2002     1.0       Creation
 * -----------------------------------------------------------------------------
 */

/**
 * Classe representant un cercle plat avec coordonnees de son centre
 *
 * @author Pascal Rapaz
 * @version 1.0
 */
public class PlaneCircle extends Circle {

  private double centerX = 0;
  private double centerY = 0;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Constructeurs
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /**
   * Construit un cercle avec un rayon defini
   * 
   * Les autres variables sont celle par defaut de cet objet
   * 
   * @param r
   *          Le rayon du cercle
   */
  public PlaneCircle(double r) {

    super(r); // appel le constructeur de la classe de base (Circle)
  } // endConst

  /**
   * Construit un cercle plat avec la coordonnee de son centre
   * 
   * @param r
   *          Le rayon du cercle
   * @param x
   *          Coordonne du centre X
   * @param y
   *          Coordonee du centre Y
   */
  public PlaneCircle(double r, double x, double y) {

    super(r); // appel le constructeur de la classe de base (Circle)

    centerX = x;
    centerY = y;
  } // endConst

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Methodes
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /**
   * Determine si une coordonnee est situee a l'interieur du cercle
   * 
   * @param x
   *          Coordonnee X
   * @param y
   *          Coordonnee Y
   * @return boolean <code>true</code> la coordonnee se situe dans le cercle,
   *         <code>false</code> si ce n'est pas le cas.
   */
  public boolean isInside(double x, double y) {

    double dx = x - centerX; // distance depuis le centre
    double dy = y - centerY; // distance depuis le centre
    double distance = Math.sqrt(dx * dx + dy * dy); // theoreme de pythagore

    return (distance < getRadius()); // retourne true ou false
  } // endFct

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  public String toString() {

    return "Cercle Plat: X = " + centerX + " Y = " + centerY + " R = " + super.getRadius();
  } // endFct

} // /:~
