package ch.rapazp.java.formation.sample.circle;

/* -----------------------------------------------------------------------------
 * (c) 2002, 2003 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * Circle.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 14.02.2002     1.0       Creation
 * -----------------------------------------------------------------------------
 */

/**
 * Classe de base pour la representation de cercles
 *
 * @author Pascal Rapaz
 * @version 1.0
 */
public class Circle {

  private double rayon = 0;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Constructeurs
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /**
   * Construit un objet cercle avec des valeurs par defaut
   */
  public Circle() {
  } // endConst

  /**
   * Construit un objet cercle avec un rayon defini par l'utilisateur
   * 
   * Les autres valeurs sont celles par defaut de la classe mere
   * 
   * @param r
   *          Rayon
   */
  public Circle(double r) {

    rayon = r;
  } // endConst

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Methodes de classe
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /**
   * Converti un angle en radian en degre
   * 
   * @param rads
   *          Angle en radian a convertir en degre
   * @return double Angle converti en degre
   */
  public static double radiusToDegrees(double rads) {
    return rads * 180 / Math.PI;
  } // endFct

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Methodes d'instance
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /**
   * Retourne la surface de l'objet
   * 
   * @return double La surface de l'objet
   */
  public double area() {
    return Math.PI * rayon * rayon;
  } // endFct

  /**
   * Retourne le perimetre de l'objet
   * 
   * @return double Le perimetre de l'objet
   */
  public double circumference() {
    return 2 * Math.PI * rayon;
  } // endFct

  /**
   * Retourne la valeur du rayon
   * 
   * @return double Le rayon
   */
  public double getRadius() {
    return rayon;
  } // endFct

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  public String toString() {

    return "Cercle: R = " + rayon;
  }// endFct
} // /:~
