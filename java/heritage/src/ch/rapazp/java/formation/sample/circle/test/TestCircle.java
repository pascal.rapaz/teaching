package ch.rapazp.java.formation.sample.circle.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import ch.rapazp.java.formation.sample.circle.Circle;
import ch.rapazp.java.formation.sample.circle.Ellipse;
import ch.rapazp.java.formation.sample.circle.PlaneCircle;

public class TestCircle {

  private static final double DELTA = 1e-15;;

  private Circle obj[];

  private double[] surfaces = { 0.0, 314.1592653589793, 1963.4954084936207, 1963.4954084936207,
      0.0, 774.402589109884, 314.1592653589793, 1253.4954687823274, 6361.725123519332,
      535.6415474370597 };

  private double[] circumferences = { 0.0, 62.83185307179586, 157.07963267948966,
      157.07963267948966, 0.0, 253.7083761311296, 62.83185307179586, 329.9380062365267,
      282.7433388230814, 167.6786408525446 };

  public TestCircle() {
    obj = new Circle[10];

    obj[0] = new Circle();
    obj[1] = new Circle(10);
    obj[2] = new PlaneCircle(25);
    obj[3] = new PlaneCircle(25, 16, 23);
    obj[4] = new Ellipse();
    obj[5] = new Ellipse(17, 29);
    obj[6] = new Circle(10);
    obj[7] = new Ellipse(21, 38);
    obj[8] = new PlaneCircle(45, 10, 18);
    obj[9] = new Ellipse(31, 11);
  }

  @Test
  public void testSurface() {

    for (int i = 0; i < obj.length; i++) {
      assertEquals("Surface du cercle", surfaces[i], obj[i].area(), DELTA);
    }// endFor
  }// endFct

  @Test
  public void testCircumferences() {

    for (int i = 0; i < obj.length; i++) {
      assertEquals("Perimètre du cercle", circumferences[i], obj[i].circumference(), DELTA);
    }// endFor
  }// endFct

  @Test
  public void testIsInside() {

    assertFalse("Coordonnée hors du cercle", ((PlaneCircle) obj[2]).isInside(20, 30));
    assertTrue("Coordonnée dans le cercle", ((PlaneCircle) obj[3]).isInside(20, 30));
  }
}// /:~
