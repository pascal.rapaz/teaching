package ch.rapazp.java.formation.sample;

/* -----------------------------------------------------------------------------
 * (c) 2014 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * SampleCirclePOO.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 13.02.2003     1.0       Creation
 * -----------------------------------------------------------------------------
 */

/**
 * Presentation de differentes notions relatives a la POO (static - non static,
 * d'instance - de classe)
 * 
 * @author Pascal Rapaz
 * @version 1.0
 */
public class SampleCirclePOO {
  public static void main(String[] args) {
    CirclePOO c = new CirclePOO(25);

    System.out.println("surface   = " + c.area());
    System.out.println("perimetre = " + c.circumference());
  }// endMain
}// /:~

class CirclePOO {

  // angle demi cercle
  private static final int ANGLE_DEMI_CERCLE = 180;

  // champ d'instance
  private double rayon = 0;

  /**
   * Construit un objet cercle avec des valeurs par defaut
   */
  public CirclePOO() {
    this(1.0);
  }// endConst

  /**
   * Construit un objet cercle avec un rayon defini par l'utilisateur
   * 
   * @param r
   *          Rayon
   */
  public CirclePOO(double r) {
    rayon = r;
  }// endConst

  /**
   * Methode de classe Converti un angle en radian en degre
   */
  public static double radiusToDegrees(double rads) {
    return rads * ANGLE_DEMI_CERCLE / Math.PI;
  }// end radiusToDegrees

  /**
   * Methode d'instance Retourne la surface d'un cercle
   */
  double area() {
    return Math.PI * rayon * rayon;
  }// end area

  /**
   * Methode d'instance Retourne le perimetre d'un cercle
   */
  public double circumference() {
    return 2 * Math.PI * rayon;
  }// end circumference
}// /:~
