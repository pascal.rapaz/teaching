package ch.rapazp.java.formation.practice.interfaces.tests;

/* -----------------------------------------------------------------------------
 * (c) 2014 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * CompareTest.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 13.03.2014     1.0       Creation
 * -----------------------------------------------------------------------------
 */

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import ch.rapazp.java.formation.practice.interfaces.comparator.Constantes;
import ch.rapazp.java.formation.practice.interfaces.comparator.Tache;

public class CompareTest {

  @Test
  public void test() {

    Tache task1 = new Tache("task1");
    Tache task2 = new Tache("task2");
    Tache task3 = new Tache("task3");
    Tache task4 = new Tache("task4");

    task1.setPriorite(Constantes.MED_PRIORITE);
    task2.setPriorite(Constantes.MED_PRIORITE);
    task3.setPriorite(Constantes.MIN_PRIORITE);
    task4.setPriorite(3);

    assertEquals("Compare " + task1 + " / " + task2, 0, task1.compareTo(task2));
    assertEquals("Compare " + task1 + " / " + task3, 1, task1.compareTo(task3));
    assertEquals("Compare " + task1 + " / " + task4, 1, task1.compareTo(task4));
    assertEquals("Compare " + task3 + " / " + task4, -1, task3.compareTo(task4));
  }// endTest
}// /:~
