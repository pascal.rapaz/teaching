package ch.rapazp.java.formation.practice.interfaces.tests;

/* -----------------------------------------------------------------------------
 * (c) 2014 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * TriTest.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 13.03.2014     1.0       Creation
 * -----------------------------------------------------------------------------
 */

import static org.junit.Assert.assertArrayEquals;

import java.util.Arrays;

import org.junit.Test;

import ch.rapazp.java.formation.practice.interfaces.tri.Constantes;
import ch.rapazp.java.formation.practice.interfaces.tri.Tache;
import ch.rapazp.java.formation.practice.interfaces.tri.TacheComparator;

public class TriTest {

  private Tache[] taches;
  private int[] result = { 10, 5, 3, 1 };

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Constructeurs
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  public TriTest() {

    taches = new Tache[4];

    taches[0] = new Tache("Préparer formation Java");
    taches[0].setPriorite(Constantes.MAX_PRIORITE);

    taches[1] = new Tache("Mettre en place tests unitaires du module 'interfaces'");
    taches[1].setPriorite(Constantes.MED_PRIORITE);

    taches[2] = new Tache("Mettre à jour le site web");
    taches[2].setPriorite(Constantes.MIN_PRIORITE);

    taches[3] = new Tache("Envoyer les corrigés");
    taches[3].setPriorite(3);
  }// endConst

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Test case
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  @Test
  public void test() {
    Arrays.sort(taches, new TacheComparator(false));

    int[] orderedPriority = new int[4];

    for (int i = 0; i < 4; i++) {

      orderedPriority[i] = taches[i].getPriorite();
    } // end for

    assertArrayEquals("Compare priority order", result, orderedPriority);
  }// endTest
}// /:~
