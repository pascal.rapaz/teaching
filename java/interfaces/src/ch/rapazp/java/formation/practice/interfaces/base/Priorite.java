package ch.rapazp.java.formation.practice.interfaces.base;

/* -----------------------------------------------------------------------------
 * (c) 2002, 2003 by Rodrigue Vaudan
 * -----------------------------------------------------------------------------
 *
 * Priorite.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 13.10.2003     1.0       Creation
 * -----------------------------------------------------------------------------
 */

/**
 * Exercice 1
 * 
 * Interface permettant de definir une priorite
 * 
 * @author Rodrigue Vaudan
 * @version 1.0
 */
public interface Priorite {
  public static final int MAX_PRIORITE = 10;
  public static final int MED_PRIORITE = 5;
  public static final int MIN_PRIORITE = 1;

  /**
   * Permet de valoriser la priorite
   * 
   * @param value
   *          La valeur de la priorite
   */
  public void setPriorite(int value);

  /**
   * Permet de recuperer la priorite
   * 
   * @return La valeur actuelle de la priorite
   */
  public int getPriorite();
} // /:~
