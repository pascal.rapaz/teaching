package ch.rapazp.java.formation.practice.interfaces.base;

/* -----------------------------------------------------------------------------
 * (c) 2002, 2003 by Rodrigue Vaudan
 * -----------------------------------------------------------------------------
 *
 * Tache.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 13.10.2003     1.0       Creation
 * -----------------------------------------------------------------------------
 */

/**
 * Exercice 1
 * 
 * Classe definissant une tache
 * 
 * @author Rodrigue Vaudan
 * @version 1.0
 */
public class Tache implements Priorite {
  private int priorite = 0;
  private String name = null;

  /**
   * Constructeur
   * 
   * @param name
   *          Le nom de la tache
   */
  public Tache(String name) {
    setPriorite(MED_PRIORITE);
    setName(name);
  } // endConst

  /**
   * @see intf.task1.Priorite#setPriorite(int)
   */
  public void setPriorite(int value) {
    priorite = value;
  } // endFct

  /**
   * @see intf.task1.Priorite#getPriorite()
   */
  public int getPriorite() {
    return priorite;
  } // endFct

  /**
   * Retourne le nom de la tache
   * 
   * @return String
   */
  public String getName() {
    return name;
  } // endFct

  /**
   * Specifie le nom de la tache
   * 
   * @param name
   *          Le nom de la tache
   */
  public void setName(String name) {
    this.name = name;
  } // endFct

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  public String toString() {
    return getName() + " | priorite: " + getPriorite();
  } // endFct
} // /:~
