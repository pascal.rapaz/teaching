package ch.rapazp.java.formation.practice.interfaces.tri;

/* -----------------------------------------------------------------------------
 * (c) 2002, 2003 by Rodrigue Vaudan
 * -----------------------------------------------------------------------------
 *
 * Tache.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 13.10.2003     1.0       Creation
 * 13.03.2014     1.1       Mise à jour implémentations de l'interface Comparable
 * -----------------------------------------------------------------------------
 */

/**
 * Exercice 3
 * 
 * Classe definissant une tache
 * 
 * @author Rodrigue Vaudan
 * @author Pascal Rapaz
 * @version 1.1
 */
public class Tache implements Priorite, Constantes, Comparable<Tache> {

  private int priorite = 0;
  private String name = null;

  /**
   * Constructeur
   * 
   * @param name
   *          Le nom de la tache
   */
  public Tache(String name) {
    setPriorite(MED_PRIORITE);
    setName(name);
  } // endConst

  /**
   * @see intf.task1.Priorite#setPriorite(int)
   */
  public void setPriorite(int value) {
    priorite = value;
  } // endFct

  /**
   * @see intf.task1.Priorite#getPriorite()
   */
  public int getPriorite() {
    return priorite;
  } // endFct

  /**
   * Retourne le nom de la tache
   * 
   * @return String
   */
  public String getName() {
    return name;
  } // endFct

  /**
   * Specifie le nom de la tache
   * 
   * @param name
   *          Le nom de la tache
   */
  public void setName(String name) {
    this.name = name;
  } // endFct

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  public String toString() {
    return getName() + " | priorite: " + getPriorite();
  } // end toString

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Comparable#compareTo(java.lang.Object)
   */
  public int compareTo(Tache autreTache) {

    if (autreTache.getPriorite() == this.getPriorite()) {
      return 0;
    } // endIf

    if (autreTache.getPriorite() > this.getPriorite()) {
      return -1;
    } // endIf

    return 1;
  } // endFct

} // /:~
