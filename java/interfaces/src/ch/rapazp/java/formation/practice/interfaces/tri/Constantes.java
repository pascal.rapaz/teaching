package ch.rapazp.java.formation.practice.interfaces.tri;

/* -----------------------------------------------------------------------------
 * (c) 2002, 2003 by Rodrigue Vaudan
 * -----------------------------------------------------------------------------
 *
 * Constantes.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 13.10.2003     1.0       Creation
 * -----------------------------------------------------------------------------
 */

/**
 * Exercice 3
 * 
 * Constantes
 * 
 * @author Rodrigue Vaudan
 * @version 1.0
 */
public interface Constantes {
  public static final int MAX_PRIORITE = 10;
  public static final int MED_PRIORITE = 5;
  public static final int MIN_PRIORITE = 1;
} // /:~
