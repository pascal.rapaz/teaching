package ch.rapazp.java.formation.sample;

/* -----------------------------------------------------------------------------
 * (c) 2002, 2003 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * Rectangle.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 09.10.2003     1.0       Creation
 * -----------------------------------------------------------------------------
 */

/**
 * Rectangle heritant de la classe abstraite 'Shape'
 *
 * @author Pascal Rapaz
 * @version 1.0
 */
public class Rectangle extends Shape {

  private double hauteur = 0;
  private double base = 0;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Constructeurs
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  public Rectangle(double base, double hauteur) {

    this.base = base;
    this.hauteur = hauteur;
  } // endConst

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Implementation des methodes abstraites
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /*
   * (non-Javadoc)
   * 
   * @see ch.rapazp.java.formation.sample.Shape#area()
   */
  public double area() {

    return base * hauteur;
  } // endFct

  /*
   * (non-Javadoc)
   * 
   * @see ch.rapazp.java.formation.sample.Shape#perimeters()
   */
  public double perimeters() {

    return (2 * base) + (2 * hauteur);
  } // endFct
} // /:~
