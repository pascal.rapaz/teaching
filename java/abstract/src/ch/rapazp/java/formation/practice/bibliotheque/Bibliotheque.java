package ch.rapazp.java.formation.practice.bibliotheque;

/*
 * ----------------------------------------------------------------------------
 * (c) 2002, 2003 by Pascal Rapaz
 * ----------------------------------------------------------------------------
 *
 * Bibliotheque.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 01.09.2003     1.0       Creation
 * -----------------------------------------------------------------------------
 */

/**
 * Gestion d'une mini bibliotheque
 *
 * @author Pascal Rapaz
 * @version 1.0
 */
public class Bibliotheque {

  private Document lstDocs[] = null;
  private int max;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Constructeur
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  public Bibliotheque() {

    max = 20;
    lstDocs = new Document[max];
  } // endConst

  /**
   * @param n
   *          Nombre maximum de livres autorises dans ma bibliotheque.
   */
  public Bibliotheque(int n) {
    max = n;
    lstDocs = new Document[max];
  } // endConst

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Methodes
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /**
   * Ajoute un document dans la bibliotheque
   * 
   * @param doc
   *          Document a ajouter dans la bibliotheque
   */
  public void add(Document doc) {

    if (Document.getNbDocuments() < max) {

      lstDocs[Document.getNbDocuments() - 1] = doc;
    } // endIf
  } // endFct

  /**
   * Affiche le contenu de la bibliotheque
   */
  public void listing() {

    System.out.println("nbre de docs =  " + Document.getNbDocuments());

    for (int i = 0; i < Document.getNbDocuments(); i++) {

      System.out.println(lstDocs[i]); // polymorphisme
    } // endFor
  } // endFct

  /**
   * Affiche tous les titres en utilisant la methode abstraite surdefinie dans
   * chaque objet
   */
  public void displayAllFormated() {

    System.out.println("nbre de docs =  " + Document.getNbDocuments());

    for (int i = 0; i < Document.getNbDocuments(); i++) {

      lstDocs[i].displayFormated(); // polymorphisme
    } // endFor
  } // endFct
} // /:~
