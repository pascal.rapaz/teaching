package ch.rapazp.java.formation.practice.bibliotheque;

/* -----------------------------------------------------------------------------
 * (c) 2002, 2003 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * Test.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 01.09.2003     1.0       Creation
 * -----------------------------------------------------------------------------
 */

/**
 * Test le fonctionnement de ma mini bibliotheque
 *
 * @author Pascal Rapaz
 * @version 1.0
 */
public class Test {

  public static void main(String args[]) {

    Bibliotheque bibli = new Bibliotheque(10); // 10 livres maxi

    Document doc = new Livre("The Java Programming Language", "Ken Arnold, James Gosling",
        "Addison Wesley");
    bibli.add(doc);

    doc = new Internet("Da Linux French Page", "http://linuxfr.org/site.html");
    bibli.add(doc);

    bibli.add(new Internet("JavaSoft", "http://www.javasoft.com"));

    bibli.listing();
    System.out.println();
    bibli.displayAllFormated();
  } // endMain
} // /:~
