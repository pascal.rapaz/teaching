package ch.rapazp.java.formation.practice.bibliotheque;

/*
 * ----------------------------------------------------------------------------
 * (c) 2002, 2003 by Pascal Rapaz
 * ----------------------------------------------------------------------------
 *
 * Livre.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 01.09.2003     1.0       Creation
 * -----------------------------------------------------------------------------
 */

/**
 * Representation d'un livre
 * 
 * @author Pascal Rapaz
 * @version 1.0
 */
public class Livre extends Document {

  private String auteur = null;
  private String editeur = null;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Constructeur
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  public Livre(String titre, String auteur, String editeur) {

    super(titre);
    this.auteur = auteur;
    this.editeur = editeur;
  } // endConst

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Methodes
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  public String toString() {

    return super.toString() + " - Auteur: " + auteur + " - Editeur: " + editeur;
  }// endFct

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Implementation des methode abstraites
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /*
   * (non-Javadoc)
   * 
   * @see
   * ch.rapazp.java.formation.practice.bibliotheque.Document#displayFormated()
   */
  public void displayFormated() {

    System.out.println("Livre: " + this.toString());
  } // endFct
} // /:~
