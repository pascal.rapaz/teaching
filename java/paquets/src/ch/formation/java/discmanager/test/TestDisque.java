package ch.formation.java.discmanager.test;

import ch.formation.java.discmanager.disc.Disque;
import ch.formation.java.discmanager.disc.enums.Style;
import ch.formation.java.discmanager.disc.helper.DisqueHelper;

/* -----------------------------------------------------------------------------
 * (c) 2002 - 2014 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * TestDisque.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 18.12.2002     1.0       Creation
 * 17.01.2014     1.1       Utilisation des type enum pour le style musical
 * -----------------------------------------------------------------------------
 */

/**
 * Classe de test pour les objets de type Disque.
 * 
 * Modifications effectuees par rapport a l'exercice 7:
 * 
 * - Suppression des appels aux methodes d'affichage autre que toString()
 * 
 * - Utilisation de l'enumeration pour affecter le style musical
 * 
 * @author Pascal Rapaz
 * @version 1.1
 */
public class TestDisque {

  public static void main(String[] args) {

    // Creation des disques
    Disque d1 = new Disque();
    d1.setArtist("Midnight Oil");
    d1.setTitle("Capricornia");
    d1.setStyle(Style.ROCK);
    d1.setNbrTrack(12);
    d1.setTime(DisqueHelper.calculTime(0, 56, 56));

    Disque d2 = new Disque("David Gray", "White ladder");
    d2.setStyle(Style.FOLK);
    d2.setNbrTrack(11);
    d2.setTime(DisqueHelper.calculTime(0, 69, 30));

    Disque d3 = new Disque("Alanis Morissette", "Under Rug Swept", Style.ROCK, 13,
        DisqueHelper.calculTime(0, 50, 52), 35.10);

    // Affichage des differents disques
    System.out.println(d1);
    System.out.println(d2);
    System.out.println(d3);

    // Compare le nombre de morceaux
    System.out.print("L'auteur du plus grand disque est: ");

    String plusGrand;
    int compare = d1.compare(d2);

    if (compare == 0) {
      plusGrand = "Ils ont le meme nombre de morceaux";
    } else if (compare == 1) {
      plusGrand = d1.getArtist();
    } else {
      plusGrand = d2.getArtist();
    } // endIf

    System.out.print(plusGrand);

    // Calcul du nombre total de morceaux
    System.out.println("\nLe nombre total de morceaux est: "
        + (d1.getNbrTrack() + d2.getNbrTrack() + d3.getNbrTrack()));

    // Calcul de la duree totale
    System.out.println("La duree total est: "
        + DisqueHelper.formatTime(d1.getTime() + d2.getTime() + d3.getTime()));
  } // endMain
} // /:~
