package ch.rapazp.java.formation.pratice.dynamique;

/* -----------------------------------------------------------------------------
 * (c) 2002 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * Dynamique.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 14.11.2002     1.0       Creation
 * -----------------------------------------------------------------------------
 */

/**
 * Exercice 2
 * 
 * Creation dynamique d'un tableau.
 * 
 * @author Pascal Rapaz
 * @version 1.1
 */
public class Dynamique {

  public static void main(String[] args) {

    char[] tableau;

    tableau = new char[2];
    tableau[0] = 'v';
    tableau[1] = 'i';

    System.out.println("le contenu du tableau est: " + String.valueOf(tableau));
  } // endMain
} // /:~
