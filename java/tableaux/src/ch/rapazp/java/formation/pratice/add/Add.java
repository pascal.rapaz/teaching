package ch.rapazp.java.formation.pratice.add;

/* -----------------------------------------------------------------------------
 * (c) 2002 - 2014 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * ShellSort.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 14.11.2002     1.0       Creation
 * -----------------------------------------------------------------------------
 */

/**
 * Exercice 5
 * 
 * Additionne les elements passes en arguments au programme.
 * 
 * @author Pascal Rapaz
 * @version 1.0
 */
public class Add {

  public static void main(String[] args) {
    int res = 0;
    String somme = new String();

    if (args.length == 0) {
      System.out.println("Utilisation: java AddInteger nbr1 <nbr2> <nbr3> ...");
      System.exit(1);
    }// endIf

    for (int i = 0; i < args.length; i++) {
      somme += args[i];
      somme += (i < args.length - 1) ? " + " : " = ";

      res += Integer.parseInt(args[i]);
    }// endFor

    System.out.println("La somme vaut: " + somme + res);
  }// endMain
}// /:~
