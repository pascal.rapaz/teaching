package ch.rapazp.java.formation.pratice.addinteger;

/* -----------------------------------------------------------------------------
 * (c) 2002 - 2014 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * AddInteger.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 14.11.2002     1.0       Creation
 * 10.01.2014	    1.1       Migration Java 1.7
 * -----------------------------------------------------------------------------
 */

/**
 * Exercice 6
 * 
 * Additionne les elements passes en arguments au programme.
 * 
 * @author Pascal Rapaz
 * @version 1.0
 */
public class AddInteger {

  public static void main(String[] args) {
    Integer res = new Integer(0);

    if (args.length == 0) {
      System.out.println("Utilisation: java AddInteger nbr1 <nbr2> <nbr3> ...");
      System.exit(1);
    }// endIf

    for (int i = 0; i < args.length; i++) {
      res = new Integer(Integer.parseInt(args[i]) + res);
    }// endFor

    System.out.println("La somme vaut: " + res);
  }// endMain
}// /:~
