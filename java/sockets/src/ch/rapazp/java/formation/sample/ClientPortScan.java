package ch.rapazp.java.formation.sample;

/* -----------------------------------------------------------------------------
 * (c) 2004 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * ClientPortScan.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 17.12.2004     1.0       Creation
 * -----------------------------------------------------------------------------
 */

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Application permettant de scanner les ports les plus communs afin de savoir
 * s'ils sont ouverts ou non.
 * 
 * Pour ameliorer les performances lors du scannage, il faudrait <i>threader</i>
 * les scans.
 * 
 * @author Pascal Rapaz
 * @version 1.0
 */
public class ClientPortScan {

  /* Liste des ports les plus communs a tester */
  private static final int[] COMMON_PORT_LIST = { 0, 21, 22, 23, 25, 53, 63, 70, 79, 80, 110, 113,
      119, 135, 139, 143, 389, 443, 445, 1002, 1024, 1025, 1026, 1027, 1028, 1029, 1030, 1720, 5000 };

  private static final String[] COMMON_PORT_SERVICE = { "<nil>", "FTP", "SSH", "Telnet", "SMTP",
      "DNS", "Whois", "Gopher", "Finger", "HTTP", "POP3", "IDENT", "NNTP", "RPC", "Net BIOS",
      "IMAP", "LDAP", "HTTPS", "MSFT DS", "ms-ils", "DCOM", "Host", "Host", "Host", "Host", "Host",
      "Host", "H.323", "UPnP" };

  public static void main(String args[]) {

    new ClientPortScan(args);
  } // endMain

  /**
   * Scan les ports des machines passees en parametre. <br />
   * Si la liste de machine est vide, le scan se fait localement.
   * 
   * @param hostList
   *          Liste des machines a scanner ou <code>null</code> pour scanner le
   *          localhost.
   */
  public ClientPortScan(String[] hostList) {

    InetAddress ia = null;

    try {

      if (0 == hostList.length) {

        ia = InetAddress.getLocalHost();
        scan(ia);
      } else {

        for (int i = 0; i < hostList.length; i++) {

          ia = InetAddress.getByName(hostList[i]);
          scan(ia);
        } // endFor
      } // endIf
    } catch (UnknownHostException uhe) {

      if (null == ia) {

        uhe.printStackTrace();
      } else {

        System.err.println(ia.getHostName() + " n'est pas une nom d'hote valide!");
      } // endIf

      System.exit(1);
    } // endTry
  } // endConst

  /**
   * Effectue le scannage des ports communs sur l'adresse IP passee en
   * parametre.
   * 
   * @param ia
   *          L'adresse IP a tester.
   * @throws UnknownHostException
   *           L'adresse IP de l'hote ne peut pas etre determinee.
   */
  private void scan(InetAddress ia) throws UnknownHostException {

    Socket socket = null;
    String hostname = ia.getHostName();

    System.out.println("start on host: " + hostname);

    for (int i = 0; i < COMMON_PORT_LIST.length; i++) {

      try {

        socket = new Socket(ia, COMMON_PORT_LIST[i]);

        System.err.println(printPortState(i, true));

        socket.close();
      } catch (IOException e) {

        System.out.println(printPortState(i, false));
      }// endTry
    } // endWhile

    System.out.println("end scan on host: " + hostname);
  } // endFct

  /**
   * Retourne une <code>String</code> contenant les informations (port, service
   * et etat) sur le port scanne.
   * 
   * @param pos
   *          Position dans le tableau des port scannes
   * @param isOpen
   *          <code>true</code> si le port est ouvert et <code>false</code> s'il
   *          est ferme.
   * @return Une <code>String</code> d'information sur l'etat du port.
   */
  private String printPortState(int pos, boolean isOpen) {

    StringBuffer sb = new StringBuffer();

    sb.append(COMMON_PORT_LIST[pos]).setLength(5);
    sb.append(COMMON_PORT_SERVICE[pos]).setLength(15);

    if (isOpen) {
      sb.append("ouvert");
    } else {
      sb.append("ferme");
    } // endIf

    return sb.toString().replace('\u0000', ' ');
  } // endFct
} // /:~
