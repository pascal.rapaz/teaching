package ch.rapazp.java.formation.practice;

/* -----------------------------------------------------------------------------
 * (c) 2004 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * SimpleFTPClient.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 22.12.2004     1.0       Creation
 * -----------------------------------------------------------------------------
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.StringTokenizer;

/**
 * Client ftp simplifie d'exemple pour la connexon par socket.
 * 
 * La norme regissant le protocol ftp peut etre consultee a l'adresse suivante:
 * {@link http://www.w3.org/Protocols/rfc959/}
 * 
 * @author Pascal Rapaz
 * @version 1.0
 */
public class SimpleFTPClient {

  /**
   * Socket utilise pour la connexion ftp.
   */
  private Socket ftpCnSocket = null;

  /**
   * Le flux d'entree utilise par le socket ftp.
   */
  private BufferedReader inputStream = null;

  /**
   * Le flux de sortie utilise par le socket ftp.
   */
  private PrintStream outputStream = null;

  /**
   * Si <code>true</code> affiche toutes les reponses du serveur.
   */
  private boolean logAll = true;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Methode main
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  public static void main(String[] args) {

    try {

      SimpleFTPClient ftp = new SimpleFTPClient("<NOM_SERVEUR_FTP");

      ftp.login("java", "java");
      ftp.getCurrentDirectory();
      ftp.listCurrentDirectoryFiles();
      ftp.logout();
    } catch (UnknownHostException e) {

      e.printStackTrace();
    } catch (IOException e) {

      e.printStackTrace();
    } // endtry
  } // endMain

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Constructeurs
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /**
   * Effectue la connexion au serveur ftp sur le port 21.
   * 
   * @param host
   *          Nom ou adresse IP du serveur ftp-
   * @throws IOException
   *           Une erreur de type E/S est survenue
   * @throws UnknownHostException
   *           L'adresse IP de l'hote ne peut pas etre determinee.
   */
  public SimpleFTPClient(String host) throws UnknownHostException, IOException {

    this(host, 21);
  } // endConst

  /**
   * Effectue la connexion sur le serveur ftp
   * 
   * @param host
   *          Nom ou adresse IP du serveur ftp-
   * @param port
   *          Port de connexion (en principe le port 21 est utilise)
   * @throws IOException
   *           Une erreur de type E/S est survenue
   * @throws UnknownHostException
   *           L'adresse IP de l'hote ne peut pas etre determinee.
   */
  public SimpleFTPClient(String host, int port) throws UnknownHostException, IOException {

    ftpCnSocket = new Socket(host, port);

    /*
     * Recuperation des flux
     */
    if (ftpCnSocket != null) {

      outputStream = new PrintStream(ftpCnSocket.getOutputStream());
      inputStream = new BufferedReader(new InputStreamReader(ftpCnSocket.getInputStream()));

      getServerReply();
    } // endIf
  } // endConst

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Methodes
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /**
   * Effectue le login de l'utilisateur.
   * 
   * @param user
   *          Nom de l'utilisateur de connexion
   * @param pwd
   *          Mot de passe pour etablir la connexion
   * @throws IOException
   *           Une erreur de type E/S est survenue
   */
  public void login(String user, String pwd) throws IOException {

    String userCmd = "USER " + user;
    String pwdCmd = "PASS " + pwd;

    /*
     * Envoi des commandes au serveur: 1 - Envoi d'une ligne avec le nom de
     * l'utilisateur 2 - Envoi d'une ligne avec le mot de passe de l'utilisateur
     */
    sendCommand(userCmd);
    sendCommand(pwdCmd);
  } // endFct

  /**
   * Effectue le logout et ferme la connexion sur le serveur ftp.
   * 
   * @throws IOException
   *           Une erreur de type E/S est survenue
   */
  public void logout() throws IOException {

    String logoutCmd = "QUIT";

    /* logout */
    sendCommand(logoutCmd);

    /* fermeture des flux */
    if (outputStream != null) {
      outputStream.close();
      inputStream.close();

      outputStream = null;
      inputStream = null;
    } // endIf

    /* fermeture de la connexion */
    if (ftpCnSocket != null) {

      ftpCnSocket.close();
      ftpCnSocket = null;
    } // endIf
  } // endFct

  /**
   * Affiche le chemin complet du repertoire courant.
   * 
   * @throws IOException
   *           Une erreur de type E/S est survenue.
   */
  public void getCurrentDirectory() throws IOException {

    String printWorkingDirCmd = "PWD";

    /* execution de la commande */
    String reply = sendCommand(printWorkingDirCmd);

    /* Recupere et affiche la reponse sans le code de retour */
    StringTokenizer token = new StringTokenizer(reply);

    if (token.countTokens() > 1) {

      token.nextToken();
      System.out.println(token.nextToken());
    } // endIf
  } // endFct

  /**
   * Affiche la liste des fichiers contenus dans le repertoire courant.
   * 
   * @throws IOException
   *           Une erreur de type E/S est survenue.
   */
  public void listCurrentDirectoryFiles() throws IOException {

    String listCmd = "LIST";

    sendDataCommand(listCmd);
  } // endFct

  /**
   * Envoi une commande au serveur FTP et retourne le resultat de celle-ci.
   * 
   * @param aCommand
   *          La commande FTP a executer
   * @throws IOException
   *           Une erreur de type E/S est survenue.
   */
  public String sendCommand(String aCommand) throws IOException {

    outputStream.println(aCommand);
    return getServerReply();
  } // endFct

  /**
   * Envoi une commande necessitant au serveur FTP et retourne le resultat de
   * celle-ci.
   * 
   * @param aCommand
   *          La commande FTP a executer
   * @throws IOException
   *           Une erreur de type E/S est survenue.
   */
  public void sendDataCommand(String command) throws IOException {

    /* Ouvre un socket faisant office de serveur sur le client (port 0) */
    ServerSocket serverSocket = new ServerSocket(0);

    /* Ouvre le port sur le serveur */
    openPort(serverSocket);

    /* Envoi la commande */
    sendCommand(command);

    /* Accepte la connexion */
    Socket local = serverSocket.accept();

    /* Affiche le contenu des informations retournee par le serveur ftp */
    BufferedReader in = new BufferedReader(new InputStreamReader(local.getInputStream()));

    String res;
    while ((res = in.readLine()) != null) {

      System.out.println(res);
    } // endWhile

    /* Fermeture de tous les elements ouverts */
    in.close();
    local.close();
    serverSocket.close();

    /* Recupere le retour du server ftp */
    getServerReply();
  } // endFct

  /**
   * Retourne la derniere ligne du resultat de la commande FTP.<br />
   * Il est recommande d'appeler cette methode apres chaque appel de commande
   * pour ce positionner correctement dans le flux (meme si l'on utilise pas le
   * retour!)
   * 
   * La reponse du serveur est constituee d'une ou plusieurs lignes. Chaque
   * ligne est terminee par le caractere '\012'. <br />
   * Le client peut identifier la derniere ligne de la reponse comme suit:
   * 
   * - elle commence par trois nombres ASCII et un espace; Ce n'est pas le cas
   * pour les lignes precedentes
   * 
   * les 3 nombres forment un code:
   * 
   * - entre 100 et 199 indique une marque
   * 
   * - entre 200 et 399 indique une acceptation
   * 
   * - entre 400 et 599 indique un rejet
   * 
   * Exemple de reponse:
   * 
   * <pre>
   *   150-This is the first line of a mark
   *   123-This line does not end the mark; note the hyphen
   *   150 This line ends the mark
   *   226-This is the first line of the second response
   *   226 This line does not end the response; note the leading space
   *   226 This is the last line of the response, using code 226
   * </pre>
   * 
   * @throws IOException
   *           Une erreur de type E/S est survenue.
   */
  private String getServerReply() throws IOException {

    String reply;

    do {

      reply = inputStream.readLine();
      if (logAll)
        System.err.println(reply);
    } while (!(Character.isDigit(reply.charAt(0)) && Character.isDigit(reply.charAt(1))
        && Character.isDigit(reply.charAt(2)) && reply.charAt(3) == ' '));

    return reply;
  } // endFct

  /**
   * Recupere l'adresse et le numero de port du socket et l'envoi via la
   * commande <code>port</code> au serveur ftp.
   * 
   * @param serverSocket
   *          Socket sur lequel le serveur doit ce connecter.
   * @throws IOException
   *           Une erreur de type E/S est survenue.
   */
  private void openPort(ServerSocket serverSocket) throws IOException {

    String portCmd = "PORT ";
    int localPort = serverSocket.getLocalPort();

    // recupere l'adresse IP locale
    InetAddress localIP;

    try {

      localIP = InetAddress.getLocalHost();
    } catch (UnknownHostException e) {

      System.err.println("Impossible de recuperer l'hete locale");
      return;
    } // end try

    // Recupere l'adresse IP
    byte[] addrBytes = localIP.getAddress();

    // Converti l'adresse IP sous forme de short
    short addrShorts[] = new short[4];

    // Probleme: Un byte plus grand que 127 est affiche comme un nombre negatif
    for (int i = 0; i <= 3; i++) {

      addrShorts[i] = addrBytes[i];
      if (addrShorts[i] < 0)
        addrShorts[i] += 256;
    } // end for

    String address = addrShorts[0] + "," + addrShorts[1] + "," + addrShorts[2] + ","
        + addrShorts[3] + "," + ((localPort & 0xff00) >> 8) + "," + (localPort & 0x00ff);

    sendCommand(portCmd + address);
  } // endFct
} // /:~
