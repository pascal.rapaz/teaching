package ch.rapazp.java.formation.sample.service;

/* -----------------------------------------------------------------------------
 * (c) 2014 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * CalculatorService.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 19.06.2014     1.0       Creation
 * -----------------------------------------------------------------------------
 */

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Service pour l'appel distant du calculateur.
 * 
 * @author Pascal Rapaz
 * @version 1.0
 */
public interface CalculatorService extends Remote {

  /**
   * Effectue une addition des deux nombres passes en parametre.
   * 
   * @param a
   *          Nombre a additioner
   * @param b
   *          Nombre a additioner
   * @return La somme des deux nombres
   * @throws RemoteException
   *           Une erreur de communication s'est produite
   */
  public double add(double a, double b) throws RemoteException;

  /**
   * Effectue une soustraction des deux nombres passes en parametre.
   * 
   * @param a
   *          Nombre de base
   * @param b
   *          Nombre a soustraire
   * @return Le resultat de la soustraction
   * @throws RemoteException
   *           Une erreur de communication s'est produite
   */
  public double sub(double a, double b) throws RemoteException;

  /**
   * Effectue la mutliplication des deux nombres passes en parametre.
   * 
   * @param a
   *          Nombre a multiplier
   * @param b
   *          Nombre a multiplier
   * @return Le resultat de la multiplication
   * @throws RemoteException
   *           Une erreur de communication s'est produite
   */
  public double mul(double a, double b) throws RemoteException;

  /**
   * Effectue la division de deux nombres passes en parametre.
   * 
   * @param a
   *          Nombre a diviser
   * @param b
   *          Dividente
   * @return Le resultat de la division
   * @throws RemoteException
   *           Une erreur de communication s'est produite
   */
  public double div(double a, double b) throws RemoteException;
} // /:~
