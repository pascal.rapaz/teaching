package ch.rapazp.java.formation.sample.client;

/* -----------------------------------------------------------------------------
 * (c) 2014 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * CalculatorClient.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 19.06.2014     1.0       Creation
 * -----------------------------------------------------------------------------
 */

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import ch.rapazp.java.formation.sample.service.CalculatorService;

/**
 * Client charge d'appeler le methodes distantes
 * 
 * @author Pascal Rapaz
 * @version 1.0
 */
public class CalculatorClient {

  public static void main(String args[]) {

    new CalculatorClient();
  } // endMain

  public CalculatorClient() {

    CalculatorService calculatorService;

    try {

      Registry registry = LocateRegistry.getRegistry();

      calculatorService = (CalculatorService) registry.lookup("calculatorService");

      System.out.println(calculatorService.sub(4, 3));
      System.out.println(calculatorService.add(4, 5));
      System.out.println(calculatorService.mul(3, 6));
      System.out.println(calculatorService.div(9, 3));

    } catch (RemoteException re) {

      System.out.println();
      System.out.println("RemoteException");
      System.out.println(re);
    } catch (NotBoundException nbe) {

      System.out.println();
      System.out.println("NotBoundException");
      System.out.println(nbe);
    } catch (java.lang.ArithmeticException ae) {

      System.out.println();
      System.out.println("ArithmeticException");
      System.out.println(ae);
    } // endTry
  } // endConst
} // /:~
