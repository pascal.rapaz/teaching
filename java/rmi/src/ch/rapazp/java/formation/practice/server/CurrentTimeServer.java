package ch.rapazp.java.formation.practice.server;

/* -----------------------------------------------------------------------------
 * (c) 2014 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * CurrentTimeServer.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 24.06.2014     1.0       Creation
 * -----------------------------------------------------------------------------
 */

import java.rmi.RMISecurityManager;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import ch.rapazp.java.formation.practice.service.CurrentTimeService;
import ch.rapazp.java.formation.practice.service.CurrentTimeServiceBean;

/**
 * Implementation du serveur RMI.
 * 
 * @author Pascal Rapaz
 * @version 1.0
 */
public class CurrentTimeServer {

  public static void main(String args[]) {

    if (System.getSecurityManager() == null) {
      System.setSecurityManager(new RMISecurityManager());
    } // end if

    new CurrentTimeServer();
  } // end main

  public CurrentTimeServer() {

    try {

      CurrentTimeService currentTimeServiceBean = new CurrentTimeServiceBean();

      CurrentTimeService currentTimeService = (CurrentTimeService) UnicastRemoteObject
          .exportObject(currentTimeServiceBean, 0);

      Registry registry = LocateRegistry.getRegistry();
      registry.rebind("currentTimeService", currentTimeService);

    } catch (Exception e) {

      e.printStackTrace();
      System.exit(1);
    } // end try
  } // end CurrentTimeServer
} // /:~