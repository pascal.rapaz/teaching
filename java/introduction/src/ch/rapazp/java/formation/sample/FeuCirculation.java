package ch.rapazp.java.formation.sample;

/* -----------------------------------------------------------------------------
 * (c) 2014 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * FeuCirculation.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 16.01.2014     1.0       Creation
 * -----------------------------------------------------------------------------
 */

/**
 * Enumeration pour la gestion des feu de circulation routiere
 * 
 * @author pra
 */
public enum FeuCirculation {
  ROUGE(30), ORANGE(10), VERT(30); // Nom des constantes

  private final int seconds; // Duree en secondes

  /**
   * Construit un objet avec la duree du feu en parametre
   * 
   * @param seconds
   *          Duree du feu en seconde
   */
  FeuCirculation(int seconds) {
    this.seconds = seconds;
  }// endConst

  /**
   * @return int La duree en seconde du feu
   */
  int getSeconds() {
    return seconds;
  }// endFct
}// /:~
