package ch.rapazp.java.formation.sample;

/* -----------------------------------------------------------------------------
 * (c) 2014 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * HelloWorld.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 08.01.2014     1.0       Creation
 * -----------------------------------------------------------------------------
 */
public class HelloWorld {

  public static void main(String[] args) {
    System.out.println("Le classique HelloWorld !");
  }// endMain
}// /:~