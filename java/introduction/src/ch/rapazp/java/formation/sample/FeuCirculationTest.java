package ch.rapazp.java.formation.sample;

/* -----------------------------------------------------------------------------
 * (c) 2014 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * FeuCirculationTest.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 16.01.2014     1.0       Creation
 * -----------------------------------------------------------------------------
 */

/**
 * Classe de test pour l'objet FeuCirculation
 * 
 * @author pra
 */
public class FeuCirculationTest {
  public static void main(String[] args) {

    for (FeuCirculation couleur : FeuCirculation.values()) {
      System.out.printf("%s: %d secondes\n", couleur, couleur.getSeconds());
    }// endFor
  }// endMain
}// /:~
