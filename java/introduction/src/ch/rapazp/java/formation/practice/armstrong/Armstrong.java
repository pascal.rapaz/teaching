package ch.rapazp.java.formation.practice.armstrong;

/* -------------------------------------------------------------------------------------------------
 * (c) 2014 by Pascal Rapaz
 * -------------------------------------------------------------------------------------------------
 *
 * Armstrong.java
 *
 * -------------------------------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 06.10.2014     1.0       Creation
 * -------------------------------------------------------------------------------------------------
 */

/**
 * Exercice 10
 * 
 * On appelle nombre de Armstrong (ou nombre narcissique) un entier naturel n non nul qui est 
 * egal a la somme des puissances p-iemes de ses chiffres en base dix, ou p designe le nombre 
 * de chiffres de n
 * (Wikipedia : http://fr.wikipedia.org/wiki/Nombre_narcissique)
 * 
 * Exemples:
 *   153 = 1^3 + 5^3 + 3^3
 *   153 = 1 + 125 + 27
 *   
 *   1634 = 1^4 + 6^4 + 3^4 + 4^4
 *   1634 = 1 + 1296 + 81 + 256
 *   
 * Ecrivez un programme qui affiche les 4 nombres de Armstrong compris entre 100 et 500. 
 * 
 * @author Pascal Rapaz
 * @version 1.0
 */
public class Armstrong {

  public static void main(String[] args) {

    for (int i = 0; i < 10000; i++) {

      if (isArmstrong(i)) {
        System.out.println(i + " is an Armstrong number !");
      }// endIf
    }// endFor
  }// endMain

  /**
   * Returns <code>true</code> if the given number is an Armstrong number.
   */
  public static boolean isArmstrong(int number) {

    int wrk = number;
    int sum = 0;
    int pow = (int) Math.log10(number) + 1; // ou String.valueOf(number).length();
    
    while (wrk != 0) {

      int crt = wrk % 10;
      sum += (int) Math.pow(crt, pow);
      wrk /= 10;
    }// endWhile

    return (sum == number) ? true : false;
  }// endFct
}// /:~
