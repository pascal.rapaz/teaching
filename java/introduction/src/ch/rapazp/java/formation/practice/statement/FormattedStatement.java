package ch.rapazp.java.formation.practice.statement;

/* -----------------------------------------------------------------------------
 * (c) 2014 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * FormattedStatement.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 09.01.2014     1.0       Creation
 * -----------------------------------------------------------------------------
 */

/**
 * Exercice 3
 * 
 * Reformatage de code source.
 * 
 * @author Pascal Rapaz
 * @version 1.0
 */
public class FormattedStatement {

  public static void main(String[] args) {
    int aNumber = 3;

    if (aNumber >= 0) {

      if (aNumber == 0) {
        System.out.println("first string");
      } else {
        System.out.println("second string");
      }// endIf

      System.out.println("third string");
    }// endIf
  }// endMain
}// /:~
