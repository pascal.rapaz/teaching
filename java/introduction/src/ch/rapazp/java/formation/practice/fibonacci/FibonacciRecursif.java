package ch.rapazp.java.formation.practice.fibonacci;

/* -----------------------------------------------------------------------------
 * (c) 2002 - 2014 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * FibonacciRecursif.java
 *
 * Explication:
 *   - Arbre des appels (http://goo.gl/u5KKyF)
 *   - Pile d'appel (http://goo.gl/8oJXIW)
 *   
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 13.11.2002     1.0       Creation
 * 15.11.2002     1.1       Modifie en fct des remarques de JMM
 * 27.02.2014     1.2       Appel de la methode fibonacci() hors du constructeur
 * -----------------------------------------------------------------------------
 */

/**
 * Exercice 4
 * 
 * Calcul le nombre de Fibonacci pour une variable de 20.
 * 
 * @author Pascal Rapaz
 * @version 1.1
 */
public class FibonacciRecursif {

  public static void main(String[] args) {
    FibonacciRecursif fibonacciRecursif = new FibonacciRecursif();

    fibonacciRecursif.fibonacci();
  }// endMain

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Constructeurs
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  public FibonacciRecursif() {
  }// endConst

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Methodes
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  public void fibonacci() {

    System.out.println(_fibonacci(20));
    return;
  }// endFct

  public int _fibonacci(int val) {
    // clause de finitude
    if (val <= 1) {
      return 1;
    }// endIf

    // pas recursif
    return _fibonacci(val - 1) + _fibonacci(val - 2);
  }// endFct
}// /:~
