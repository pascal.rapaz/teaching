package ch.rapazp.java.formation.practice.fleche;

/* -----------------------------------------------------------------------------
 * (c) 2014 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * Fleche.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 09.01.2014     1.0       Creation
 * -----------------------------------------------------------------------------
 */

/**
 * Exercice 6
 * 
 * Dessine une fleche dans la console.
 * 
 * @author Pascal Rapaz
 * @version 1.0
 */
public class Fleche {

  public static void main(String[] args) {

    System.out.println("      |\n      ||\n------|||\n      ||\n      |");
  }// endMain
}// /:~
