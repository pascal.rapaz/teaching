package ch.rapazp.java.formation.practice.bazinga;

/* -------------------------------------------------------------------------------------------------
 * (c) 2002 - 2014 by Pascal Rapaz
 * -------------------------------------------------------------------------------------------------
 *
 * Bazinga.java
 *
 * -------------------------------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 18.01.2002     1.0       Creation
 * -------------------------------------------------------------------------------------------------
 */

/**
 * Exercice 7
 * 
 * Effectue une boucle jusqu'a 100 et affiche baz pour les multiples de 3, inga pour les multiple de
 * 5 et bazinga pour les multiples de 3 et 5
 * 
 * ex: 1 2 baz 4 inga baz 7 8 baz inga 11 baz 13 14 bazinga 16 17 baz 19 inga baz 22 23 ...
 * 
 * @author Pascal Rapaz
 * @version 1.0
 */
public class Bazinga {

  public static void main(String[] args) {

    for (int i = 1; i < 101; i++) {

      if (i % 3 == 0 && i % 5 == 0) {
        System.out.print("bazinga");
      } else if (i % 3 == 0) {
        System.out.print("baz");
      } else if (i % 5 == 0) {
        System.out.print("inga");
      } else {
        System.out.print(i);
      }// endIf

      System.out.print(" ");
    }// endFor
  }// endMain
}// /:~
