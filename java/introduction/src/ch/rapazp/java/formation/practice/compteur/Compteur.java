package ch.rapazp.java.formation.practice.compteur;

/* -----------------------------------------------------------------------------
 * (c) 2014 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * Compteur.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 09.01.2014     1.0       Creation
 * -----------------------------------------------------------------------------
 */

/**
 * Exercice 5
 * 
 * Compte de 0 a 12 par pas de 3.
 * 
 * Utilise successivement les instructions while, do...while et for.
 * 
 * @author Pascal Rapaz
 * @version 1.0
 */
public class Compteur {

  public static void main(String[] args) {
    int cpt;

    // Compte avec une boucle while
    cpt = 0;
    System.out.println("Compteur avec -while-:");

    while (cpt < 13) {
      System.out.println(cpt);
      cpt += 3;
    }// endWhile

    System.out.println();

    // Compte avec une boucle do...while
    cpt = 0;
    System.out.println("Compteur avec -do...while-:");

    do {
      System.out.println(cpt);
      cpt += 3;
    } while (13 > cpt);

    System.out.println();

    // Compte avec une boucle for
    System.out.println("Compteur avec -for-:");

    for (int i = 0; i < 13; i += 3) {
      System.out.println(i);
    }// endFor
  }// endMain
}// /:~
