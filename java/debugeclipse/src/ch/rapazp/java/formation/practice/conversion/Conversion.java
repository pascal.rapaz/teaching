package ch.rapazp.java.formation.practice.conversion;

/* -----------------------------------------------------------------------------
 * (c) 2003 - 2014 by Rodrigue Vaudan, 
 * -----------------------------------------------------------------------------
 *
 * Conversion.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 13.02.2003     1.0       Creation
 * 03.02.2014     1.1       Remplacement des Buffer par un Scanner
 * -----------------------------------------------------------------------------
 */

import java.io.IOException;
import java.util.Scanner;

/**
 * Exemple d'utilisation du debugger d'Eclipse.
 *
 * @author Rodrigue Vaudan
 * @author Pascal Rapaz
 * @version 1.0
 */
public class Conversion {

  public static void main(String[] args) {

    Scanner scanner = new Scanner(System.in);

    try {

      System.out.println("Entrer la valeur de n :");

      // Recuperation de la saisie de l'utilisateur
      double inputDouble = scanner.nextDouble();
      long resTrans = DoubleToLong(inputDouble);

      // affichage du resultat
      System.out.println("DoubleToLong(" + inputDouble + ") -> " + resTrans);

    } catch (IOException ex) {

      System.out.println("Exception: ");
      ex.printStackTrace();

    } finally {

      scanner.close();
    }// endTry
  }// endMain

  /**
   * Transforme un double en long
   * 
   * Exemple : DoubleToLong(15.3) -> 1530
   * 
   * @param n
   *          nombre a convertir
   * @return long
   * @version 1.0
   */
  public static long DoubleToLong(double n) throws IOException {

    long res = 0;

    // transformation du double
    res = (long) (n * 100);

    return res;
  }// endFct
}// /:~
