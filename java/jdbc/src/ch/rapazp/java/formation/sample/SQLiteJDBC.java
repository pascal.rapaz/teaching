package ch.rapazp.java.formation.sample;

/* -----------------------------------------------------------------------------
 * (c) 2014 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * SQLiteJDBC.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 25.03.2014     1.0       Creation
 * -----------------------------------------------------------------------------
 */

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

/**
 * Exemple de connexion a une base de donnees SQLite et illustration des
 * operations CRUD
 * 
 * @author Pascal Rapaz
 * @version 1.0
 * @source http://www.tutorialspoint.com/sqlite/sqlite_java.htm
 */
public class SQLiteJDBC {

  private Connection connection = null;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Constructeurs
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  public SQLiteJDBC() {
  }// endMain

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Methodes
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /**
   * Etabli une connexion a la base de donnees
   */
  public void connect(boolean withPropertiesFile) {

    if (null == connection) {
      // Variante 1
      // Connexion sans fichier de proprietes
      if (!withPropertiesFile) {
  
        try {
          Class.forName("org.sqlite.JDBC");
          connection = DriverManager.getConnection("jdbc:sqlite:test.db");
        } catch (Exception e) {
          System.err.println(e.getClass().getName() + ": " + e.getMessage());
          System.exit(1);
        } // endTry
  
        
      // Variante 2
      // Utilisation d'un fichier properties pour etablir la connexion
      } else {

        Properties props = null;
        FileInputStream input = null;
        
        try {
          props = new Properties();
          input = new FileInputStream("database.properties");
          props.load(input);

          String JDBCDrivers = props.getProperty("jdbc.drivers");

          if (JDBCDrivers != null) {
            System.setProperty("jdbc.drivers", JDBCDrivers);
          } // endIf

          String url = props.getProperty("jdbc.url");

          connection = DriverManager.getConnection(url);
          
        } catch (Exception e) {
        
          System.err.println(e.getClass().getName() + ": " + e.getMessage());
          System.exit(1);
        } finally {
          
          try {
            input.close();
            
          } catch (IOException e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(1);
          }// endTry
        }// endTry
      }// endIf
    }// endIf
  }// endFct

  /**
   * Ferme la connexion à la base de donnees
   */
  public void close() {

    try {
      connection.close();
    } catch (SQLException e) {
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
    } finally {
      connection = null;
    }// endTry
  }// endFct

  /**
   * Crée une nouvelle table COMPANY
   */
  public void createTable() {

    if (null == connection) {
      connect(false);
    }// endIf

    Statement stmt = null;

    try {
      connection.setAutoCommit(true);

      stmt = connection.createStatement();

      String sql = "CREATE TABLE COMPANY " + 
          "(ID INT PRIMARY KEY     NOT NULL," +
          " NAME           TEXT    NOT NULL, " +
          " AGE            INT     NOT NULL, " +
          " ADDRESS        CHAR(50), " +
          " SALARY         REAL)";

      stmt.executeUpdate(sql);
      stmt.close();

    } catch (Exception e) {
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
      System.exit(1);
    }// endTry
  }// endFct

  /**
   * Supprime la table COMPANY
   */
  public void dropTable() {

    if (null == connection) {
      connect(false);
    }// endIf

    Statement stmt = null;

    try {

      connection.setAutoCommit(true);

      stmt = connection.createStatement();

      String sql = "DROP TABLE COMPANY;";

      stmt.executeUpdate(sql);
      stmt.close();

    } catch (Exception e) {
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
      System.exit(1);
    }// endTry
  }// endFct

  /**
   * Insere quelques enregistrement dans la table COMPANY
   * 
   * @return le nombre d'enregistrements inseres
   */
  public int insertOperation() {

    int count = 0;

    if (null == connection) {
      connect(false);
    }// endIf

    Statement stmt = null;

    try {

      connection.setAutoCommit(false);

      stmt = connection.createStatement();

      String sql = "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) " +
          "VALUES (1, 'Paul', 32, 'California', 20000.00 );";
      count += stmt.executeUpdate(sql);
      
      sql = "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) " +
         "VALUES (2, 'Allen', 25, 'Texas', 15000.00 );";
      count += stmt.executeUpdate(sql);
      
      sql = "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) " +
         "VALUES (3, 'Teddy', 23, 'Norway', 20000.00 );";
      count += stmt.executeUpdate(sql);
      
      sql = "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) " +
         "VALUES (4, 'Mark', 25, 'Rich-Mond ', 65000.00 );";
      count += stmt.executeUpdate(sql);

      stmt.close();
      connection.commit();

    } catch (Exception e) {
      
      try {
        if (connection != null) {
          connection.rollback();
        } // endIf
        
      } catch (SQLException ex) {
        System.err.println("Rollback impossible !");
        System.exit(1);
      } // endTry
      
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
      System.exit(1);
    }// endTry

    return count;
  }// endFct

  /**
   * Insere quelques enregistrement dans la table COMPANY
   * 
   * @return le nombre d'enregistrements inseres
   */
  public int insertBatchOperation() {

    int count = 0;

    if (null == connection) {
      connect(false);
    }// endIf

    Statement stmt = null;

    try {

      connection.setAutoCommit(false);

      stmt = connection.createStatement();
      
      String insert;

      for (int i = 5; i < 100; i++) {
        insert = "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) " + "VALUES (" + i
            + ", 'Mark', " + 1 * 25 + ", 'Rich-Mond ', " + 12000.00 * i + " );";

        stmt.addBatch(insert);
      } // end for

      int[] res = stmt.executeBatch();
      count = res.length;

      // commit de la transaction
      connection.commit();

    } catch (SQLException e) {
      if (connection != null) {
        try {
          connection.rollback();
          System.out.println("Error ocurred. Transaction aborted");
        } catch (SQLException rb) {
          System.out.println("Couldn't rollback " + rb.getMessage());
        } // end try
      } // end if

      e.printStackTrace();

    } catch (Exception e) {
      e.printStackTrace();
    } // end try

    return count;
  }// endFct
  
  
  /**
   * Utilise un statement pour recuperer tous les elements de la table COMPANY
   * 
   * @return liste formatée des enregistrements de la table COMPANY
   */
  public ArrayList<String> selectWithStatementOperation() {

    ArrayList<String> ret = new ArrayList<>();

    if (null == connection) {
      connect(false);
    }// endIf

    Statement stmt = null;

    try {

      stmt = connection.createStatement();

      ResultSet rs = stmt.executeQuery("SELECT * FROM COMPANY;");

      while (rs.next()) {

        int id = rs.getInt("id");
        String name = rs.getString("name");
        int age = rs.getInt("age");
        String address = rs.getString("address");
        float salary = rs.getFloat("salary");

        StringBuilder value = new StringBuilder();

        value.append("\"ID = " + id + " | ");
        value.append("NAME = " + name + " | ");
        value.append("AGE = " + age + " | ");
        value.append("ADDRESS = " + address + " | ");
        value.append("SALARY = " + salary + "\"");

        ret.add(value.toString());
      }// endWhile

      rs.close();
      stmt.close();

    } catch (Exception e) {
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
      System.exit(1);
    }// endTry

    return ret;
  }// endFct

  /**
   * Utilise un preparedStatement pour recuperer un enregistrement de la table
   * COMPANY
   * 
   * @param aName
   *          Nom de la personne recherchee dans la base
   * @return liste formatée des enregistrements de la table COMPANY
   */
  public ArrayList<String> selectWithPreparedStatementOperation(String aName) {

    ArrayList<String> ret = new ArrayList<>();

    if (null == connection) {
      connect(false);
    }// endIf

    PreparedStatement pstmt = null;

    try {

      pstmt = connection.prepareStatement("SELECT * FROM COMPANY WHERE NAME like ?;");

      pstmt.setString(1, aName);
      
      ResultSet rs = pstmt.executeQuery();

      while (rs.next()) {

        int id = rs.getInt("id");
        String name = rs.getString("name");
        int age = rs.getInt("age");
        String address = rs.getString("address");
        float salary = rs.getFloat("salary");

        StringBuilder value = new StringBuilder();

        value.append("\"ID = " + id + " | ");
        value.append("NAME = " + name + " | ");
        value.append("AGE = " + age + " | ");
        value.append("ADDRESS = " + address + " | ");
        value.append("SALARY = " + salary + "\"");

        ret.add(value.toString());
      }// endWhile

      rs.close();
      pstmt.close();

    } catch (Exception e) {
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
      System.exit(1);
    }// endTry

    return ret;
  }// endFct

  
  /**
   * Mets a jour des enregistrements de la table COMPANY
   * 
   * @return Le nombre d'enregsitrements modifiés
   */
  public int updateOperation() {

    int count = 0;

    if (null == connection) {
      connect(false);
    }// endIf

    Statement stmt = null;

    try {
      connection.setAutoCommit(false);

      stmt = connection.createStatement();
      String sql = "UPDATE COMPANY set SALARY = 25000.00 where ID=1;";
      count = stmt.executeUpdate(sql);
      connection.commit();

      stmt.close();

    } catch (Exception e) {

      try {
        if (connection != null) {
          connection.rollback();
        } // endIf
        
      } catch (SQLException ex) {
        System.err.println("Rollback impossible !");
        System.exit(1);
      } // endTry

      System.err.println(e.getClass().getName() + ": " + e.getMessage());
      System.exit(1);
    }// endTry

    return count;
  }// endFct

  /**
   * Supprime des enregistrements de la table COMPANY
   * 
   * @return Le nombre d'enregistrements supprimes
   */
  public int deleteOperation() {

    int count = 0;

    if (null == connection) {
      connect(false);
    }// endIf

    Statement stmt = null;

    try {
      connection.setAutoCommit(false);

      stmt = connection.createStatement();
      String sql = "DELETE from COMPANY where ID=2;";
      count = stmt.executeUpdate(sql);
      connection.commit();

      stmt.close();

    } catch (Exception e) {
      
      try {
        if (connection != null) {
          connection.rollback();
        } // endIf
        
      } catch (SQLException ex) {
        System.err.println("Rollback impossible !");
        System.exit(1);
      } // endTry
      
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
      System.exit(1);
    }// endTry

    return count;
  }// endFct

}// /:~
