package ch.rapazp.java.formation.sample.test;

/* -----------------------------------------------------------------------------
 * (c) 2014 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * SQLiteJDBCTest.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 24.03.2014     1.0       Creation
 * -----------------------------------------------------------------------------
 */

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.util.ArrayList;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import ch.rapazp.java.formation.sample.SQLiteJDBC;

/**
 * Classe de test pour la classe SQLiteJDBC
 * 
 * @author Pascal Rapaz
 * @version 1.0
 * @source http://www.tutorialspoint.com/sqlite/sqlite_java.htm
 */
public class SQLiteJDBCTest {

  static SQLiteJDBC jdbc;

  @BeforeClass
  public static void prepare() {
    jdbc = new SQLiteJDBC();
    jdbc.createTable();
    jdbc.connect(true);
  }// endFct

  @AfterClass
  public static void cleanup() {
    jdbc.dropTable();
    jdbc.close();
  }// endFct

  @Test
  public void testJDBC() {

    assertEquals("Insert operation", 4, jdbc.insertOperation());
    ArrayList<String> insert = jdbc.selectWithStatementOperation();
    assertEquals("Insert array size", 4, insert.size());

    assertEquals("Insert batch operation", 95, jdbc.insertBatchOperation());
    ArrayList<String> insertBatch = jdbc.selectWithStatementOperation();
    assertEquals("Insert batch array size", 99, insertBatch.size());

    assertEquals("Update operation", 1, jdbc.updateOperation());
    ArrayList<String> update = jdbc.selectWithStatementOperation();
    assertNotEquals("Content must be different after update", insert.get(0), update.get(0));

    ArrayList<String> select = jdbc.selectWithPreparedStatementOperation("Teddy");
    assertEquals("Data retrieved are not the same",
        "\"ID = 3 | NAME = Teddy | AGE = 23 | ADDRESS = Norway | SALARY = 20000.0\"", select.get(0));

    assertEquals("Delete operation", 1, jdbc.deleteOperation());
    ArrayList<String> delete = jdbc.selectWithStatementOperation();
    assertNotEquals("Content must be different after delete", insert.get(1), delete.get(1));
  }// endFct
}// /:~
