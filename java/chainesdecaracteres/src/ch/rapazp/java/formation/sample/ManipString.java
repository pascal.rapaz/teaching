package ch.rapazp.java.formation.sample;

/* -----------------------------------------------------------------------------
 * (c) 2014 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * ManipString.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 10.01.2014     1.0       Creation
 * -----------------------------------------------------------------------------
 */

/**
 * Exemple de manipulation de String.
 * 
 * @author Pascal Rapaz
 * @version 1.0
 */
public class ManipString {

  public static void main(String[] args) {
    String texte = "Manipulation";
    String s = "manipulation";
    String res;
    String sCasse;
    boolean ok;

    int n = texte.length(); // donne 12
    char c = texte.charAt(5); // donne u
    res = texte.equals(s) ? "ok" : "ko";
    // res = "ko" car la casse est differente
    ok = "manipulation".equals(s); // ok = true
    sCasse = texte.equalsIgnoreCase(s) ? "ok" : "ko"; // sCasse = "ok"

    // Affichage des resultats
    System.out.println(n);
    System.out.println(c);
    System.out.println(res);
    System.out.println(ok);
    System.out.println(sCasse);
  } // endMain
} // /:~
