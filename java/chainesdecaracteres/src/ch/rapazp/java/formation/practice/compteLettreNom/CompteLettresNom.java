package ch.rapazp.java.formation.practice.compteLettreNom;

/* -----------------------------------------------------------------------------
 * (c) 2014 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * CompteLettresNom.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 08.01.2014     1.0       Creation
 * 16.01.2014     1.1       Remplacement de println par printf
 * 01.02.2014     1.2       Utilisation de indexOf pour retrouver l'espace
 * -----------------------------------------------------------------------------
 */

/**
 * Exercice 1
 * 
 * Compte le nombre de lettres.
 * 
 * @author Pascal Rapaz
 * @version 1.2
 */
public class CompteLettresNom {

  public static void main(String[] args) {

    if (args.length != 1) {
      System.err.println("Execution: java CompteLettresNom \"<nom prenom>\"");
      System.exit(0);
    }// endIf

    String nomPrenom = args[0];

    System.out.printf("Votre nom est compose de %d caracteres\n", (nomPrenom.length() - 1));
    System.out.printf("Votre nom est   : %s\n", args[0].substring(0, nomPrenom.indexOf(' ')));
    System.out.printf("Votre prenom est: %s\n",
        args[0].substring(nomPrenom.indexOf(' ') + 1, nomPrenom.length()));
  } // endMain
} // /:~