package ch.rapazp.java.formation.practice.service;

/* -----------------------------------------------------------------------------
 * (c) 2014 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * Style.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 17.01.2014     1.0       Creation
 * -----------------------------------------------------------------------------
 */

/**
 * Exercice 8
 * 
 * Enumeration pour la gestion des styles musicaux
 * 
 * @author Pascal Rapaz
 * @version 1.0
 */
public enum Style {

    BLUES("blues"),
    CLASSICAL("classical"),
    COUNTRY("country"),
    FOLK("folk"),
    JAZZ("jazz"),
    NEWAGE("newage"),
    REGGEA("reggea"),
    ROCK("rock"),
    MISC("misc"),
    POP("pop"),
    ELECTRO("electro"),
    AUTRE("autre");

  private final String style;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Constructeur
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  Style(String style) {
    this.style = style;
  }// endConst

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Methodes
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /**
   * @return String Represente le style musical
   */
  public String style() {
    return style;
  }// endFct
}// /:~
