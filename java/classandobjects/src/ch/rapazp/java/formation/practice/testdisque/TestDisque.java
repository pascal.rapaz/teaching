package ch.rapazp.java.formation.practice.testdisque;

/* -----------------------------------------------------------------------------
 * (c) 2002 - 2014 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * TestDisque.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 17.12.2002     1.0       Creation
 * -----------------------------------------------------------------------------
 */

/**
 * Exercice 3
 * 
 * Classe de test pour les objets de type Disque.
 * 
 * Modifications effectuees dans le main() par rapport a l'exercice 2:
 * 
 * - L'appel des methodes statique est fait comme suit: [classe].[methode]
 * 
 * @author Pascal Rapaz
 * @version 1.0
 */
public class TestDisque {

  public static void main(String[] args) {

    // Creation des disques
    Disque d1 = new Disque("Midnight Oil", "Capricornia", "rock alternatif",
        12, Disque.calculTime(0, 56, 56));

    Disque d2 = new Disque("Tom McRae", "Tom McRae", "rock", 13,
        Disque.calculTime(0, 45, 17));

    Disque d3 = new Disque("Alanis Morissette", "Under Rug Swept", "rock", 13,
        Disque.calculTime(0, 50, 52));

    // Affichage des differents disques
    System.out.println("Disque 1");
    System.out.println("~~~~~~~~");
    System.out.println(d1.getArtist());
    System.out.println(d1.getTitle());
    System.out.println(d1.getStyle());
    System.out.println(d1.getNbrTrack());
    System.out.println(d1.getFormatedTime());

    System.out.println("\nDisque 2");
    System.out.println("~~~~~~~~");
    System.out.println(d2.getArtist());
    System.out.println(d2.getTitle());
    System.out.println(d2.getStyle());
    System.out.println(d2.getNbrTrack());
    System.out.println(d2.getFormatedTime());

    System.out.println("\nDisque 3");
    System.out.println("~~~~~~~~");
    System.out.println(d3.getArtist());
    System.out.println(d3.getTitle());
    System.out.println(d3.getStyle());
    System.out.println(d3.getNbrTrack());
    System.out.println(d3.getFormatedTime());

    // Calcul du nombre total de morceaux
    System.out.println("\nLe nombre total de morceaux est: "
        + (d1.getNbrTrack() + d2.getNbrTrack() + d3.getNbrTrack()));

    // Calcul de la duree totale
    System.out.println("La duree total est: "
        + Disque.formatTime(d1.getTime() + d2.getTime() + d3.getTime()));
  } // endMain
} // /:~
