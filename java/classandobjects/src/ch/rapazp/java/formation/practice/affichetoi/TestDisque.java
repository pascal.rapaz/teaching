package ch.rapazp.java.formation.practice.affichetoi;

/* -----------------------------------------------------------------------------
 * (c) 2002 - 2014 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * TestDisque.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 17.12.2002     1.0       Creation
 * 20.03.2003     1.1       Modification du test d'affichage
 * -----------------------------------------------------------------------------
 */

/**
 * Exercice 4
 * 
 * Classe de test pour les objets de type Disque
 * 
 * Modifications effectuees dans le main() par rapport a l'exercice 3:
 * 
 * - Remplacement du traitement de l'affichage par l'appel de la methode
 * afficheToi()
 * 
 * @author Pascal Rapaz
 * @version 1.1
 */
public class TestDisque {

  public static void main(String[] args) {

    // Creation des disques
    Disque d1 = new Disque("Midnight Oil", "Capricornia", "rock alternatif",
        12, Disque.calculTime(0, 56, 56));

    Disque d2 = new Disque("Tom McRae", "Tom McRae", "rock", 13,
        Disque.calculTime(0, 45, 17));

    Disque d3 = new Disque("Alanis Morissette", "Under Rug Swept", "rock", 13,
        Disque.calculTime(0, 50, 52));

    // Affichage des differents disques
    d1.afficheToi();
    d2.afficheToi();
    System.out.println(d3);

    // Calcul du nombre total de morceaux
    System.out.println("\nLe nombre total de morceaux est: "
        + (d1.getNbrTrack() + d2.getNbrTrack() + d3.getNbrTrack()));

    // Calcul de la duree totale
    System.out.println("La duree total est: "
        + Disque.formatTime(d1.getTime() + d2.getTime() + d3.getTime()));
  } // endMain
} // /:~
