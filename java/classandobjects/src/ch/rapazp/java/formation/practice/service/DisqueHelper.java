package ch.rapazp.java.formation.practice.service;

/* -----------------------------------------------------------------------------
 * (c) 2002 - 2014 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * DisqueHelper.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 18.12.2002     1.0       Creation
 * -----------------------------------------------------------------------------
 */

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Exercice 8
 * 
 * Cette classe contient toutes les methodes de service utilisee par la classe
 * Disque.
 * 
 * @author Pascal Rapaz
 * @version 1.0
 */
public abstract class DisqueHelper {

  /**
   * Retourne une heure en format milliseconde
   * 
   * @param hour
   *          Heure
   * @param min
   *          Minutes
   * @param sec
   *          Secondes
   * @return long Represente l'heure en milliseconde
   */
  public static long calculTime(int hour, int min, int sec) {

    /*
     * Rappel: 1 heure = 1 * 60 * 60 * 1000 millisecondes
     */
    return (hour * 3600 + min * 60 + sec) * 1000;
  } // endFct

  /**
   * Retourne l'heure formatee.
   * 
   * @param time
   *          L'heure a formater
   * @return String L'heure formatee
   */
  public static String formatTime(long time) {

    // Definition du timezone afin de ne pas avoir de decalage horaire dans
    // le formatage de l'heure
    TimeZone utc = TimeZone.getTimeZone("GMT");
    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
    sdf.setTimeZone(utc);

    return sdf.format(new Date(time));
  } // endFct
}// /:~
