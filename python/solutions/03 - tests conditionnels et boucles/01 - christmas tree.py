#!/usr/bin/env python3
# -*- coding: utf-8 -*-

##
# Copyright (C) 2019-2020 - dServ Solutions SàRL - [Pascal Rapaz](mailto:pascal.rapaz@dserv.ch)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
##

##
# Dessinez un sapin de noel de 5 sur 10
#
# Tips : 
#   '*' * 5 ==> "*****"
#
# Exemple:
#     *
#    ***
#   *****
#  *******
# *********
#     *
#     *
##
import os, sys
os.system('cls' if sys.platform == 'win32' else 'clear')

size = 10

for i in range(size):
  if i%2 == 0:
    continue
  #endIf

  print(' '*(int((size - i)/2)) + '*'*i)
#endFor

for i in range(2):
  print(' '*(int(size/2) - 1) + '*')
#endFor
