#!/usr/bin/env python3
# -*- coding: utf-8 -*-

##
# Copyright (C) 2019-2020 - dServ Solutions SàRL - [Pascal Rapaz](mailto:pascal.rapaz@dserv.ch)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
##

class Circle:
  def __init__(self, rayon):
    """Cree un objet Cercle

    Arguments:
    rayon -- rayon du cercle
    """
    self.__rayon = rayon
  #endDef

  @property
  def rayon(self):
    """
    >>> c = Circle(5)
    >>> c.rayon
    5
    >>> c.diametre = 30
    >>> c.rayon
    15.0
    """
    return self.__rayon
  #endDef

  @rayon.setter
  def rayon(self, r):
    self.__rayon = r
  #endDef
  
  @property
  def diametre(self):
    """
    >>> Circle(5).diametre
    10
    """
    return self.__rayon * 2
  #endDef

  @diametre.setter
  def diametre(self, d):
    self.__rayon = d / 2
  #endDef
#endClass