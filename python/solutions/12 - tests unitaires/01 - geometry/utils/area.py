#!/usr/bin/env python3
# -*- coding: utf-8 -*-

##
# Copyright (C) 2019-2020 - dServ Solutions SàRL - [Pascal Rapaz](mailto:pascal.rapaz@dserv.ch)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
##

from math import pi

from exceptions.unknownshape import UnknownShapeError

from shapes.rectangle import Rectangle
from shapes.circle import Circle
from shapes.square import Square

def area(forme):
  """Retourne la surface d'une forme geometrique.

  Arguments:
  forme -- objet utilise pour calculer la surface

  >>> area(Rectangle(30, 20))
  600
  >>> area(Circle(8))
  201.06192982974676
  >>> area(Square())
  Traceback (most recent call last):
    ...
  exceptions.unknownshape.UnknownShapeError: Type de forme inconnu : Square
  """
  if isinstance(forme, Rectangle):
    return forme.longueur * forme.largeur
  elif isinstance(forme, Circle):
    return pi * forme.rayon**2
  else:
    raise UnknownShapeError(f"Type de forme inconnu : {forme.__class__.__name__}")
  #endIf
#endDef