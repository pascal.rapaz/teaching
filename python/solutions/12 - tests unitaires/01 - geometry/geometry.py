#!/usr/bin/env python3
# -*- coding: utf-8 -*-

##
# Copyright (C) 2019-2020 - dServ Solutions SàRL - [Pascal Rapaz](mailto:pascal.rapaz@dserv.ch)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
##

##
# En vous basant sur l'exercice précédent "01 - Géométrie" :
# 
#   * Créez une nouvelle classe `Cercle` contenant les arguments `rayon` et `diametre`
#   * Complétez la fonction `area` afin qu'elle puisse retourner la surface d'un cercle
#   * Créez une nouvelle classe `Carre` sans implémentation
#   * Créez une exception `UnknownShapeError` qui hérite de la clase `Exception`
#   * Adaptez la fonction `area` afin qu'elle lève une exception `UnknownShapeError` si elle reçoit une forme inconnue
#   * Testez l'exception avec un objet de type `Carre`
#   * "Refactorez" votre projet afin d'avoir la structure suivante :
# 
#     project
#     ├── geometry.py
#     ├── shapes
#     │   ├── square.py
#     │   ├── circle.py
#     │   └── rectangle.py
#     ├── exceptions
#     │   └── unknownshape.py
#     └── utils
#         └── area.py
#
# Exemple de résultat:
#   Surface du rectangle (20, 30) = 600m2
#   Surface du cercle de rayon 8 = 201.06192982974676m2
#   Traceback (most recent call last):
#     File "/geometry.py", line 55, in <module>
#       print(f"Surface du carre = {area(s)}m2")
#     File "/utils/area.py", line 40, in area
#       raise UnknownShapeError(f"Type de forme inconnu : {forme.__class__.__name__}")
#   exceptions.unknownshape.UnknownShapeError: Type de forme inconnu : Square
##

import doctest

import shapes.circle
import utils.area

from shapes.rectangle import Rectangle
from shapes.square import Square

if __name__ == "__main__":
  doctest.testmod(utils.area)
  doctest.testmod(shapes.circle)

  r = Rectangle(30, 20)
  c = shapes.circle.Circle(8)
  s = Square()

  print(f"Surface du rectangle {r.largeur, r.longueur} = {utils.area.area(r)}m2")
  print(f"Surface du cercle de rayon {c.rayon} = {utils.area.area(c)}m2")
