#!/usr/bin/env python3
# -*- coding: utf-8 -*-

##
# Copyright (C) 2019-2020 - dServ Solutions SàRL - [Pascal Rapaz](mailto:pascal.rapaz@dserv.ch)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
##

import unittest

from exceptions.unknownshape import UnknownShapeError

from shapes.rectangle import Rectangle
from shapes.circle import Circle
from shapes.square import Square
from utils.area import area

class Test_Geometry(unittest.TestCase):
  def setUp(self):
    self.rectangle = Rectangle(30, 20)
    self.circle = Circle(8)
    self.square = Square()
  #endDef

  def test_area_rectangle(self):
    self.assertEqual(area(self.rectangle), 600)
  #endDef

  def test_area_circle(self):
    self.assertEqual(area(self.circle), 201.06192982974676)
  #endDef

  def test_circle_diameter(self):
    self.assertEqual(self.circle.diametre, 16)
    self.circle.rayon = 15
    self.assertEqual(self.circle.diametre, 30)
    self.circle.rayon = 8
  #endDef

  def test_circle_radius(self):
    self.circle.diametre = 40
    self.assertEqual(self.circle.rayon, 20)
    self.circle.diametre = 16
    self.assertEqual(self.circle.rayon, 8)
  #endDef

  def test_area_exception(self):
    with self.assertRaises(UnknownShapeError) as context:
      area(self.square)

    self.assertTrue("Type de forme inconnu" in str(context.exception))
  #endDef
#emdClass

if __name__ == '__main__':
  unittest.main()
#endIf