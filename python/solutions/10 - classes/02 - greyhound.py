#!/usr/bin/env python3
# -*- coding: utf-8 -*-

##
# Copyright (C) 2019-2020 - dServ Solutions SàRL - [Pascal Rapaz](mailto:pascal.rapaz@dserv.ch)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
##

##
# En vous basant sur l'exercice 17 - Animaux, créér une classe `Levrier()` 
# qui hérite de la classe `Chien()`.
#
# Cette nouvelle classe aura la méthode :
#   * `cours()`
#     retourne : "[nom] cours à la vitesse de [x] km/h !"
#
# Ensuite :
#   - Ajoutez un lévrier à votre liste de chiens
#   - Affichez le status des chiens avec la méthode `etat()` et, s'il s'agit d'un lévrier 
#     et qu'il ne dors pas, appelez également la méthode `cours()`
#
# Résultat attendu:
#   Beethoven ne dors pas !
#   Leika est en train de dormir !
#   McQueen est en train de dormir !
#   Iko ne dors pas !
#   Iko cours à la vitesse de 53 km/h !
##

class Animaux:
  chiens = []

  def __init__(self, chiens):
    """Gère une liste d'animaux

    Arguments:
    chiens -- liste de chiens
    """
    self.chiens = chiens
  #endDef

  def etat(self):
    for c in self.chiens:
      print(c.dors())

      if isinstance(c, Levrier) and not c.is_sleeping:
        print(c.cours())
      #endIf
    #endFor
  #endDef
#endClass

class Chien():

  def __init__(self, nom, age, race, is_sleeping=False):
    """Cree un objet Chien

    Arguments:
    nom -- nom du chien
    age -- age du chien
    race -- race du chien
    is-sleeping -- True si le chien dors, sinon False
    """
    self.__nom = nom
    self.__age = age
    self.__race = race
    self.__is_sleeping = is_sleeping
  #endDef

  def dors(self):
    if (self.__is_sleeping):
      return f"{self.nom} est en train de dormir !"
    
    return f"{self.nom} ne dors pas !"
  #endDef

  @property
  def nom(self):
    return self.__nom
  #endDef

  @nom.setter
  def nom(self, n):
    self.__nom = n
  #endDef

  @property
  def age(self):
    return self.__age
  #endDef

  @age.setter
  def age(self, a):
    self.__age = a
  #endDef

  @property
  def race(self):
    return self.__race
  #endDef

  @race.setter
  def race(self, r):
    self.__race = r
  #endDef

  @property
  def is_sleeping(self):
    return self.__is_sleeping
  #endDef

  @is_sleeping.setter
  def is_sleeping(self, s):
    self.__is_sleeping = s
  #endDef

  def __repr__(self):
    return f"Chien(nom='{self.__nom}', age={self.__age:d}, race='{self.__race}', is_sleeping={self.__is_sleeping})"
  #endDef
#endClass

class Levrier(Chien):
  def __init__(self, nom, age, vitesse, race="Lévrier", is_sleeping=False):
    super().__init__(nom, age, race, is_sleeping)
    self.__vitesse = vitesse
  #endDef

  def cours(self):
    return f"{self.nom} cours à la vitesse de {self.vitesse:d} km/h !"
  #endDef

  @property 
  def vitesse(self):
    return self.__vitesse
  #endDef

  @vitesse.setter
  def vitesse(self, v):
    self.__vitesse = v
  #endDef

  def __repr__(self):
    return f"Levrier(nom='{self.nom}', age={self.age}, race='{self.race}', is_sleeping={self.is_sleeping}, vitesse={self.__vitesse:d})"
  #endDef
#endClass

# Creation de la liste de chiens
mes_chiens = [Chien("Beethoven", 8, "Saint-Bernard", False),
              Chien("Leika", 1, "Yorkshire", True), 
              Chien("McQueen", 3, "Bouledogue Français", True),
              Levrier("Iko", 5, 53),
             ]

# Creation d'une instance d'animaux
mes_animaux = Animaux(mes_chiens)

# Affiche le status des chiens
mes_animaux.etat()