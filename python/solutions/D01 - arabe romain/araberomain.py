#!/usr/bin/env python3
# -*- coding: utf-8 -*-

##
# -----------------------------------------------------------------------------
# Copyright (C) 2019-2021 - dServ Solutions SàRL - [Pascal Rapaz](mailto:pascal.rapaz@dserv.ch)
# -----------------------------------------------------------------------------
#
# Conversion chiffres arabes -> chiffres romains et inversément
#
# Préconditions:
#   - Les nombres passés en paramètre sont valides
#   - Les nombres sont compris en 1 et 3999
#
# Versions : 1.0 - 11.02.2021
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
##


# -----------------------------------------------------------------------------
# Roman to Arabic
# -----------------------------------------------------------------------------


trans = {"M": 1000,
         "D": 500,
         "C": 100,
         "L": 50,
         "X": 10,
         "V": 5,
         "I": 1}


def roman_to_arabic(roman):
  """Retourne la valeur 'arabe' d'un chiffre romain

  Parameters:
    roman (str): Nombre en notation romaine

  Returns:
    arabic (int): Nombre arabe correspondant

  Doctests:
  >>> roman_to_arabic('MCMLXXXV')
  1985
  >>> roman_to_arabic('VII')
  7
  >>> roman_to_arabic('XVII')
  17
  >>> roman_to_arabic('DCXCIII')
  693
  >>> roman_to_arabic('MMXXI')
  2021
  """
  arabic = 0
  for i, letter in enumerate(roman):
    if (i+1) == len(roman) or trans[letter] >= trans[roman[i+1]]:
      arabic += trans[letter]
    else:
      arabic -= trans[letter]
    # endIf
  # endFor

  return arabic
# endDef


def roman_to_arabic_short(roman):
  """Retourne la valeur 'arabe' d'un chiffre romain

  Version compacte proposée par [@200_success](https://codereview.stackexchange.com/a/141413/238954)

  Parameters:
    roman (str): Nombre en notation romaine

  Returns:
    arabic (int): Nombre arabe correspondant

  Doctests:
  >>> roman_to_arabic_short('MCMLXXXV')
  1985
  >>> roman_to_arabic_short('VII')
  7
  >>> roman_to_arabic_short('XVII')
  17
  >>> roman_to_arabic_short('DCXCIII')
  693
  >>> roman_to_arabic_short('MMXXI')
  2021
  """
  values = [trans[letter] for letter in roman]

  return sum(
      val if val >= next_val else -val
      for val, next_val in zip(values[:-1], values[1:])
  ) + values[-1]
# endDef


# -----------------------------------------------------------------------------
# Arabic to Roman
# -----------------------------------------------------------------------------


def arabic_to_roman(arabic):
  """Retourne la valeur en chiffre 'romain' d'un nombre arabe

  Parameters:
    arabic (int): Nombre en notation arabe

  Returns:
    roman (str): Nombre en notation romaine

  Doctests:
  >>> arabic_to_roman(1985)
  'MCMLXXXV'
  >>> arabic_to_roman(7)
  'VII'
  >>> arabic_to_roman(17)
  'XVII'
  >>> arabic_to_roman(693)
  'DCXCIII'
  >>> arabic_to_roman(2021)
  'MMXXI'
  """
  roman = ''

  # traitement des milliers
  roman += 'M' * (arabic // 1000)

  # traitement des centaines
  roman += __digit_to_roman((arabic // 100) % 10, 'C', 'D', 'CM')

  # traitement des dizaines
  roman += __digit_to_roman((arabic // 10) % 10, 'X', 'L', 'XC')

  # traitement des unités
  roman += __digit_to_roman(arabic % 10, 'I', 'V', 'IX')

  return roman
# endDef


def __digit_to_roman(digit, one, five, nine):
  """Retourne la chaine correspondante au {digit}

  Parameters:
    digit (int): Nombre arabe à convertir
    one (str): Valeur romaine pour le 1
    five (str): Valeur romaine pour le 5
    nine (str): Valeur romaine pour le 9

  Returns:
    roman (str): Nombre en notation romaine

  Doctests:
  >>> __digit_to_roman(9, 'A', 'B', 'CD')
  'CD'
  >>> __digit_to_roman(7, 'A', 'B', 'CD')
  'BAA'
  >>> __digit_to_roman(5, 'A', 'B', 'CD')
  'B'
  >>> __digit_to_roman(4, 'A', 'B', 'CD')
  'AB'
  >>> __digit_to_roman(2, 'A', 'B', 'CD')
  'AA'
  """
  if digit == 9:
    return nine
  elif digit >= 5:
    return five + one * (digit - 5)
  elif digit == 4:
    return one + five
  else:
    return one * digit
  # endIf
# endDef


def arabic_to_roman_short(arabic):
  """Retourne la valeur en chiffre 'romain' d'un nombre arabe

  Version compacte proposée par [@200_success](https://codereview.stackexchange.com/a/141413/238954)

  Parameters:
    arabic (int): Nombre en notation arabe

  Returns:
    roman (str): Nombre en notation romaine

  Doctests:
  >>> arabic_to_roman_short(1985)
  'MCMLXXXV'
  >>> arabic_to_roman_short(7)
  'VII'
  >>> arabic_to_roman_short(17)
  'XVII'
  >>> arabic_to_roman_short(693)
  'DCXCIII'
  >>> arabic_to_roman_short(2021)
  'MMXXI'
  """
  return (
      'M' * (arabic // 1000) +
      __digit_to_roman_short((arabic // 100) % 10, 'C', 'D', 'CM') +
      __digit_to_roman_short((arabic // 10) % 10, 'X', 'L', 'XC') +
      __digit_to_roman_short(arabic % 10, 'I', 'V', 'IX')
  )
# endDef


def __digit_to_roman_short(digit, one, five, nine):
  """Retourne la chaine correspondante au {digit}

  Version compacte proposée par [@200_success](https://codereview.stackexchange.com/a/141413/238954)

  Parameters:
    digit (int): Nombre arabe à convertir
    one (str): Valeur romaine pour le 1
    five (str): Valeur romaine pour le 5
    nine (str): Valeur romaine pour le 9

  Returns:
    roman (str): Nombre en notation romaine

  Doctests:
  >>> __digit_to_roman_short(9, 'A', 'B', 'CD')
  'CD'
  >>> __digit_to_roman_short(7, 'A', 'B', 'CD')
  'BAA'
  >>> __digit_to_roman_short(5, 'A', 'B', 'CD')
  'B'
  >>> __digit_to_roman_short(4, 'A', 'B', 'CD')
  'AB'
  >>> __digit_to_roman_short(2, 'A', 'B', 'CD')
  'AA'
  """
  return (
      nine if digit == 9 else
      five + one * (digit - 5) if digit >= 5 else
      one + five if digit == 4 else
      one * digit
  )
# endDef


if __name__ == '__main__':
  # Exécution des tests unitaires
  import doctest
  doctest.testmod()

  # Conversion des nombres romains
  # Recuperation et conversion en majuscule
  roman = input("Saisissez un nombre écrit en chiffres romains : ").upper()
  print(f"{roman} égale {roman_to_arabic(roman)}")
  print(f"{roman} égale {roman_to_arabic_short(roman)}")

  # Conversion des chiffres arabes
  # Recuperation et conversion en int
  arabic = int(input("Saisissez un nombre arabe (<4000) : "))
  print(f"{arabic} égale {arabic_to_roman(arabic)}")
  print(f"{arabic} égale {arabic_to_roman_short(arabic)}")
# endMain
