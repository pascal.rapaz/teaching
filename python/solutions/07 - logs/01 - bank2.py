#!/usr/bin/env python3
# -*- coding: utf-8 -*-

##
# Copyright (C) 2019-2020 - dServ Solutions SàRL - [Pascal Rapaz](mailto:pascal.rapaz@dserv.ch)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
##

##
# Créez le fichier `execution.log` qui, en fonction des données `data` et à chaque exécution, complète les logs:
#    info  : ajoute le détenteur du compte
#    debug : indique le solde du comtpe
#
#  data = ['John Doe', 1350]
#
#  Exemple:
#    INFO:root:Détenteur John Doe
#    DEBUG:root:Solde actuel du compte 1350
#    INFO:root:Détenteur John Doe
#    DEBUG:root:Solde actuel du compte 1350
##

import logging

data = ['John Doe', 1350]

logging.basicConfig(filename='execution.log', level=logging.DEBUG)

### 
# Affiche également dans la console les messages WARNING, ERROR et CRITICAL
logger = logging.getLogger()

# cree un format personnalise pour la sortie console
# (correspond au format par defaut)
fmt = logging.Formatter("%(levelname)s:%(name)s:%(message)s")

logConsole = logging.StreamHandler()
logConsole.setFormatter(fmt)
logConsole.setLevel(logging.WARNING)
logger.addHandler(logConsole)
###

logging.info(f'Détenteur {data[0]}')
logging.debug(f'Solde actuel du compte {data[1]:d}')