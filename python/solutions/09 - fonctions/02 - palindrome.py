#!/usr/bin/env python3
# -*- coding: utf-8 -*-

##
# Copyright (C) 2019-2020 - dServ Solutions SàRL - [Pascal Rapaz](mailto:pascal.rapaz@dserv.ch)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
##

##
# Écrire une fonction qui vérifie si la string passée en paramètre est un palindrome, puis affiche le résultat:
# 
#  - "Sugus" est un palindrome
#  - "Emile nu a une lime" est un palindrome
#  - "Completement debile" n'est pas un palindrome
##

def is_palindrome(string):
  s = string.lower().replace(" ", "")  # passe tout en minuscule et supprime les espaces

  # compare la chaine d'origine avec la chaîne renversée
  return True if s == s[::-1] else False
#endDef

def print_resultat(string):
  if(is_palindrome(string)):
    print(f'"{string}" est un palindrome')
  else:
    print(f'"{string}" n\'est pas un palindrome')
  #endIf
#endDef

print_resultat("Sugus")
print_resultat("Emile nu a une lime")
print_resultat("Completement debile")