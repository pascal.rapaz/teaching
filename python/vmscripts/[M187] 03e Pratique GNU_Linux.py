#!/usr/bin/env python3
# -*- coding: utf-8 -*-

##
# Prepare GNU/Linux for evaluation (D)
#
# License: GPL-2+
#   This package is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This package is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   If you need the GNU General Public License write to:
#     Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston,
#     MA 02110-1301, USA.
#
# (c) 2018-2022, Pascal Rapaz (RapazP - pascal.rapaz@rapazp.ch)
# =============================================================================
##

import argparse
import os
import logging
import sys
import shutil
import subprocess
import textwrap

VERSION = "1.2.0"
VERSION_MSG = "Application     : %s" % __file__ +\
              "\nRelease version : %s" % VERSION +\
              "\n(C) 2018-2019 by Pascal Rapaz (pascal.rapaz@rapazp.ch)"

euid = os.geteuid()
if euid != 0:
  print("Script not started as root. Running sudo...")
  args = ['sudo', sys.executable] + sys.argv + [os.environ]
  # the next line replaces the currently-running process with the sudo
  os.execlpe('sudo', *args)
#endIf

class bcolors:
  RED="\033[31m%s\033[0m"
  GREEN="\033[32m%s\033[0m"
  BLUE="\033[34m%s\033[0m"
#endClass

PATHS = [
         'nirvana',
         'arcade_fire',
         'radiohead',
         'doors',
         'blondie',
         'beatles/help',
         'beatles/hey_jude'
        ]

USERS = [
        'emilie'
        ]

USER_HOME = os.path.expanduser('~')

def clear_history():
  cmd = "cat /dev/null > ~/.bash_history && history -c"
  execute(cmd)
#endDef

def backup():
  if not os.path.exists('/etc/passwd.bak'):
    shutil.copy('/etc/passwd', '/etc/passwd.bak')
    shutil.copy('/etc/shadow', '/etc/shadow.bak')
    shutil.copy('/etc/group', '/etc/group.bak')
    shutil.copy('/etc/gshadow', '/etc/gshadow.bak')
  #endIf

  if (os.path.exists('/home/administrator') and 
      not os.path.exists('/home/administrator/.bashrc.bak')):

    shutil.copy('/home/administrator/.bashrc', '/home/administrator/.bashrc.bak')
  #endIf
#endDef

def restore():
  for u in USERS:
    cmd = "userdel -rf " + u
    execute(cmd)
  #endFor

  shutil.move('/etc/passwd.bak', '/etc/passwd')
  shutil.move('/etc/shadow.bak', '/etc/shadow')
  shutil.move('/etc/group.bak', '/etc/group')
  shutil.move('/etc/gshadow.bak', '/etc/gshadow')

  if os.path.exists('/home/administrator'):
    shutil.move('/home/administrator/.bashrc.bak', '/home/administrator/.bashrc')
  #endIf

  cmd = 'rm /home/emilie -rf;'
  cmd += 'rm /mnt/stranger -rf;'
  cmd += 'rm /home/robin -rf;'
  cmd += 'rm /home/nancy -rf;'

  execute(cmd)
#endDef

def create_user():
  for u in USERS:
    cmd = 'useradd -m %s -d /home/%s' % (u, u)
    execute(cmd)

    create_folders(u)
    create_empty_file(u)
  #endIf
#endDef

def create_folders(user):
  for f in PATHS:
    ff = os.path.join('/home', user, f)

    if not os.path.exists(ff):
      os.makedirs(ff)
    #endIf
  #endIf
#endDef

def create_empty_file(user):
  f = os.path.join('/home', user, PATHS[6], 'empty.sh')

  cmd = 'touch ' + f
  execute(cmd)
#endDef

def check_install():
  if os.path.exists('/home/emilie'):
    print(bcolors.GREEN % ("Installation terminee!"))
    print(bcolors.GREEN % ("Bon examen !!!"))
  else:
    print("L'installation a echoue, appelez votre enseignant!")
  #endIf
#endDef

def verify_responses():
  res = []

  res.append(bcolors.BLUE % ("Exercice 1 : Gestion des fichiers\n"))
  if os.path.exists('/home/administrator/config'):
    res.append("> contenu du dossier config:\n")
    cmd = 'ls /home/administrator/config'
    stdout, stderr = execute(cmd)
    res.append(stdout)
    res.append('\n')
  else:
    res.append(bcolors.RED % ("[KO] le dossier ~/config n'existe pas\n"))
  #endIf

  if os.path.exists('/home/emilie'):
    res.append(bcolors.RED % ("[KO] /home/emilie existe\n"))
  else:
    res.append(bcolors.GREEN % ("[OK] le dossier /home/emilie est supprime\n"))
  #endIf

  res.append(bcolors.BLUE % ("\nExercice 2 : Creation utilisateurs\n"))
  cmd = "cat /etc/passwd | grep 'robin\|nancy'"
  res.append("> filtre robin et nancy avec grep\n")
  stdout, stderr = execute(cmd)
  res.append(stdout)
  res.append('\n')
  cmd = "cat /etc/group | grep 'characters'"
  res.append("> filtre characters avec grep\n")
  stdout, stderr = execute(cmd)
  res.append(stdout)
  res.append('\n')

  res.append(bcolors.BLUE % ("Exercice 3 : Repertoires et droits\n"))
  if not os.path.exists('/mnt/stranger'):
    res.append(bcolors.RED % ("[KO] /mnt/stranger n'existe pas\n"))
  #endIf
  res.append("> Propriétaire /mnt/stranger\n")
  res.append("--> administrator:root\n")
  cmd = "ls -lsa /mnt/ | grep stranger"
  stdout, stderr = execute(cmd)
  res.append(stdout)
  res.append("\n> droits du dossier /mnt/stranger/things\n")
  res.append("--> défaut: drwxrwxr-x, attendus: dr-xrw---- (560)\n")
  res.append("--> le groupe propriétaire est changé > administrator:characters\n")
  cmd = "ls -lsa /mnt/stranger/ | grep things"
  stdout, stderr = execute(cmd)
  res.append(stdout)
  res.append('\n')

  print(''.join(res))
#endDef

def execute(command, exit=False):
  """
  Execute synchronus command.

  @param command: Command bash to launch
  @param exit: Exit if there is an execution error (Default: True)
  """

  logging.debug(command)

  p = subprocess.run(command, 
                     stdout=subprocess.PIPE,
                     stderr=subprocess.PIPE,
                     shell=True, 
                     encoding='utf-8',
                     executable='/bin/bash')

  if (0  != p.returncode and
      exit):

    sys.exit(p.returncode)
  #endIf

  logging.debug(p.stdout)
  logging.debug(p.stderr)

  return p.stdout, p.stderr
#endDef

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Startup methods
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_arguments():
  """
  Retrieve command line arguments
  """

  parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                   description=textwrap.dedent("""
    Prepare GNU/Linux for evaluation
    """))

  parser.add_argument("--verify",
                      action='store_true')

  parser.add_argument("--restore",
                       action='store_true')

  parser.add_argument("-v", "--verbose",
                      action='store_true',
                      help="increase execution verbosity")

  parser.add_argument("-V", "--version",
                      action='version',
                      version=VERSION_MSG)

  return parser.parse_args()
#endDef

if __name__ == "__main__":
  args = get_arguments()

  if args.verbose:
    logging.basicConfig(level=logging.DEBUG)
  else:
    logging.basicConfig(level=logging.INFO)
  #endIf

  if (args.verify):
    verify_responses()
  elif (args.restore):
    restore()
  else:
    clear_history()
    backup()
    create_user()
    check_install()
  #endIf
#endIf
