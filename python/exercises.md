<!--
Copyright (C) 2019-2021 - dServ Solutions SàRL - [Pascal Rapaz](mailto:pascal.rapaz@dserv.ch)

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
Free Documentation License".

A copy of the license is agreement is available at [GNU
Free Documentation License](https://www.gnu.org/licenses/fdl-1.3.txt)".
-->

# Énoncés des exercices <!-- omit in toc -->

## Table des matières <!-- omit in toc -->

- [01. Introduction](#01-introduction)
  - [01. Hello World](#01-hello-world)
- [02. Variables](#02-variables)
  - [01. Hello World](#01-hello-world-1)
- [03. Tests conditionnels et boucles](#03-tests-conditionnels-et-boucles)
  - [01. Christmas tree](#01-christmas-tree)
  - [02. Bazinga](#02-bazinga)
  - [03. Factorial](#03-factorial)
- [04. Structures de données](#04-structures-de-données)
  - [01. Calendar](#01-calendar)
  - [02. Network](#02-network)
  - [03. MinMax](#03-minmax)
- [05. Slicing](#05-slicing)
  - [01. Pizzas](#01-pizzas)
  - [02. World](#02-world)
- [06. Formater une string](#06-formater-une-string)
  - [01. Bank](#01-bank)
- [07. Logs](#07-logs)
  - [01. Bank2](#01-bank2)
- [08. Exceptions et fichiers](#08-exceptions-et-fichiers)
  - [01. DisplayLog](#01-displaylog)
  - [02. FilterLog](#02-filterlog)
- [09. Fonctions](#09-fonctions)
  - [01. Unique](#01-unique)
  - [02. Palindrome](#02-palindrome)
- [10. Classes](#10-classes)
  - [01. Animaux](#01-animaux)
  - [02. Lévrier](#02-lévrier)
- [11. Modules et packages](#11-modules-et-packages)
  - [01. Géométrie](#01-géométrie)
  - [02. Géométrie (suite)](#02-géométrie-suite)
- [12. Tests unitaires](#12-tests-unitaires)
  - [01. Géométrie (fin)](#01-géométrie-fin)
    - [Bonus](#bonus)
- [Divers](#divers)
  - [D01. Arabe Romain](#d01-arabe-romain)
    - [Bonus](#bonus-1)

# 01. Introduction

## 01. Hello World

Écrire une application qui affiche `Hello World` dans le terminal

# 02. Variables

## 01. Hello World

Écrire une application qui utilise une variable contenant `Hello World`, puis afficher son contenu dans le terminal

# 03. Tests conditionnels et boucles

## 01. Christmas tree

Dessinez un sapin de noël de 5 sur 10

> :pencil2: **_TIP_**
>
> `'*' * 5 ==> "*****"`

Exemple:

```less
    *
   ***
  *****
 *******
*********
    *
    *
```

## 02. Bazinga

Effectuez une boucle jusqu'a 100 et affichez :

- `baz` pour les multiples de 3
- `inga` pour les multiples de 5
- `bazinga` pour les multiples de 3 et 5

ex: 1 2 baz 4 inga baz 7 8 baz inga 11 baz 13 14 bazinga 16 17 baz 19 inga baz 22 23 ...

## 03. Factorial

Calculez la valeur factorielle d'un nombre (ex: 4! = 4*3*2*1 = 24)

# 04. Structures de données

## 01. Calendar

Soit les listes suivantes :

```py
jours = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
mois = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin',
        'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre']
```

Écrivez un programme qui crée une nouvelle liste de sorte d'avoir pour chaque mois le nombre de jours correspondant :

```py
  ['Janvier', 31, 'Février', 28,'Mars', 31, etc.]
```

## 02. Network

Créez une base de données au format suivant:

|     | Interface | IP                | Status |
| --- | --------- | ----------------- | ------ |
| 1   | lo        | 127.0.0.1/8       | up     |
| 2   | eth0      | 192.168.43.205/24 | down   |
| 3   | eth1      | 192.168.110.1/24  | up     |
| 4   | wlp2s0    | 172.16.0.38       | up     |

- Affichez la liste de toutes les interfaces `up`
- Affichez la liste de toutes les interfaces `down`

## 03. MinMax

Créez un programme qui trouve le minimum et le maximum dans un `set` ou un `frozenset`

Exemple:

```text
[5, 10, 3, 15, 2, 20]
min = 2
max = 20
```

# 05. Slicing

## 01. Pizzas

Récupérez le dernier élément de la listede pizzas

```py
pizzas = ['Hawai', 'Pepperoni', 'Fromaggi', 'Napolitana', 'Diavoli']
```

## 02. World

Récupérer le mot "World" de la chaîne de caractères "Hello World !"

# 06. Formater une string

## 01. Bank

Formatez les données du tableau `data` afin d'obtenir le rendu suivant :

```text
Bonjour M. Doe, 
Le solde actuel de votre compte est de 1350.-
```

```py
data = ['John Doe', 1350]
```

# 07. Logs

## 01. Bank2

Créez le fichier `execution.log` qui, en fonction des données `data` et à chaque exécution, complète les logs:

```text
info  : ajoute le détenteur du compte
debug : indique le solde du comtpe
```

Exemple:

```py
data = ['John Doe', 1350]
```

```text
INFO:root:Détenteur John Doe
DEBUG:root:Solde actuel du compte 1350
INFO:root:Détenteur John Doe
DEBUG:root:Solde actuel du compte 1350
```

# 08. Exceptions et fichiers

## 01. DisplayLog

Affichez le contenu du fichier de log créé dans l'exercice [Bank2](#12---Bank2) et gérez les exceptions dans le cas ou le fichier n'existe pas ou n'est pas trouvé

## 02. FilterLog

Adaptez l'application [DisplayLog](#13---DisplayLog) afin d'afficher uniquement les informations de debug et levez une exception si le fichier n'en contient pas.

***Bonus :***

- *Faites en sorte qu'il n'y ai pas de lignes vides lors de l'affichage*

# 09. Fonctions

## 01. Unique

Écrire une fonction qui reçoit 2 listes en paramètre et retourne une liste contenant tous les éléments uniques issus de la fusion des 2 listes.

Exemple:

```text
l1 = [1, 2, 3, 3, 4, 5]
l2 = [1, 4, 5, 7, 8]
res = [1, 2, 3, 4, 5, 7, 8]
```

## 02. Palindrome

Écrire une fonction qui vérifie si la string passée en paramètre est un palindrome, puis affiche le résultat :

```text
"Sugus" est un palindrome
"Emile nu a une lime" est un palindrome
"Completement debile" n'est pas un palindrome
 ```

# 10. Classes

## 01. Animaux

Créez les classes `Animaux()` et `Chien()`

La classe `Animaux()` contiendra la liste des chiens et aura les méthodes et attributs suivants :

- attributs
  - `chiens` -> liste vide
- méthodes
  - `etat()` -> parcours les éléments de la liste et, pour chaque chien, appel la méthode `dors()`

La classe `Chien()` représentera un animal et aura les méthodes et attributs suivants :

- attributs
  - `nom`
  - `age`
  - `race`
  - `is_sleeping`
- méthodes
  - `dors()`

    retourne : "`[nom] est en train de dormir !`" ou "`[nom] ne dors pas !`" en fonction de l'état de l'attribut `is_sleeping`

Une fois les 2 classes créées :

- Créez une liste de chiens (minimum 3):
  
  `mes_chiens = [Chien("Beethoven", 8, "Saint-Bernard", False), ...]`

- Instanciez la classe animaux avec la liste de chiens
- Affichez le status des chiens avec la méthode `etat()`

Résultat attendu:

```text
Beethoven ne dors pas !
Leika est en train de dormir !
McQueen est en train de dormir !
```

## 02. Lévrier

En vous basant sur l'exercice [17 - Animaux](#17---Animaux), créér une classe `Levrier()` qui hérite de la classe `Chien()`.

Cette nouvelle classe aura la méthode :

- `cours()`

  retourne : "[nom] cours à la vitesse de [x] km/h !"

Ensuite :

- Ajoutez un lévrier à votre liste de chiens
- Affichez le status des chiens avec la méthode `etat()` et, s'il s'agit d'un lévrier et qu'il ne dors pas, appelez également la méthode `cours()`

Résultat attendu:

```text
Beethoven ne dors pas !
Leika est en train de dormir !
McQueen est en train de dormir !
Iko ne dors pas !
Iko cours à la vitesse de 53 km/h !
```

# 11. Modules et packages

## 01. Géométrie

Créez un nouveau projet qui contiendra les éléments suivants :
  
- Une classe `Rectangle` avec les arguments `longueur` et `largeur`
- Un `package` "utils" qui contiendra le module utilitaire `area`.
- Le module utilitaire `area` aura une fonction permettant de calculer la surface d'une forme géométrique en fonction de l'objet passé en paramètre.
  
  Par exemple pour le rectangle : `return longueur * largeur`
- Le module `geometry` chargé d'instancier l'objet `Rectangle` et de calculer la surface de ce dernier

  Par exemple pour un rectangle de 30x20 le résultat sera: `Surface = 600m2`

Structure du projet:

```bash
project
├── geometry.py
├── rectangle.py
└── utils
    └── area.py
```

## 02. Géométrie (suite)

En vous basant sur l'exercice précédent "[01 - Géométrie](#01---g%c3%a9om%c3%a9trie)" :

- Créez une nouvelle classe `Cercle` contenant les arguments `rayon` et `diametre`
- Complétez la fonction `area` afin qu'elle puisse retourner la surface d'un cercle
- Créez une nouvelle classe `Carre` sans implémentation
- Créez une exception `UnknownShapeError` qui hérite de la classe `Exception`
- Adaptez la fonction `area` afin qu'elle lève une exception `UnknownShapeError` si elle reçoit une forme inconnue
- Testez l'exception avec un objet de type `Carre`
- "Refactorez" votre projet afin d'avoir la structure suivante :

  ```bash
  project
  ├── geometry.py
  ├── shapes
  │   ├── square.py
  │   ├── circle.py
  │   └── rectangle.py
  ├── exceptions
  │   └── unknownshape.py
  └── utils
      └── area.py
  ```

Exemple de résultat:

```text
Surface du rectangle (20, 30) = 600m2
Surface du cercle de rayon 8 = 201.06192982974676m2
Traceback (most recent call last):
  File "/geometry.py", line 55, in <module>
    print(f"Surface du carre = {area(s)}m2")
  File "/utils/area.py", line 40, in area
    raise UnknownShapeError(f"Type de forme inconnu : {forme.__class__.__name__}")
exceptions.unknownshape.UnknownShapeError: Type de forme inconnu : Square
```

# 12. Tests unitaires

## 01. Géométrie (fin)

En vous basant sur l'exercice [02. Géométrie (suite)](#02-g%c3%a9om%c3%a9trie-suite), écrivez les tests unitaires avec `unittest` et/ou `doctest` pour les éléments suivants:

- Calcul de la surface d'un rectangle
- Calcul de la surface d'un cercle
- Vérifiez que les proportions entre le diamètre et le rayon d'un cercle sont correctes
- Levée d'exception si la forme n'est pas supportée par la méthode `area`

Exemple de résultat avec unittest :

```bash
$ python -m unittest discover -s unittest/
.....
----------------------------------------------------------------------
Ran 5 tests in 0.000s

OK
```

### Bonus

- Gérer le cas ou l'utilisateur saisi des valeures négatives.

# Divers

## D01. Arabe Romain

Écrivez la méthode `arabic_to_roman()` permettant de convertir un nombre arabe en nombre romain.

**Rappel**

Les nombres romains et arabes obéissent à la table de correspondance suivante:

|      |     |     |     |     |     |     |
| ---- | --- | --- | --- | --- | --- | --- |
| M    | D   | C   | L   | X   | V   | I   |
| 1000 | 500 | 100 | 50  | 10  | 5   | 1   |

On converti les nombres de la façon suivante:

|     |      |     |     |     |     |     |
| --- | ---- | --- | --- | --- | --- | --- |
| 900 | 800  | 700 | 600 | 400 | 300 | 200 |
| CM  | DCCC | DCC | DC  | CD  | CCC | CC  |

Ainsi, par exemple, 1985 devient : MCMLXXXV

### Bonus

- Écrivez la fonction `roman_to_arabic()` qui convertit un nombre romain en nombre arabe.
- Écrivez des tests unitaires pour valider le fonctionnement des 2 méthodes

<br/><p style='text-align: right;'><sub>&copy; 2019-2021 - dServ Solutions SàRL - [Pascal Rapaz](mailto:pascal.rapaz@dserv.ch)</sub></p>
